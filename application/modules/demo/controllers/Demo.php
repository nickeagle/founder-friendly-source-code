<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo extends MY_Controller {

    /**
     * Class constructor
     *
     * Loads required models, check if user has right to access this class, load the hook class and add a hook point
     *
     * @return  void
     */
    public function __construct()
    {
    	parent::__construct();
    	$this->load->model("demo/Demo_model","Mmodel");
    }


    public function index()
    {
        
    	echo date_default_timezone_get();
    	 echo date("Y-m-d H:i:s");
    }

    public function servertime()
    {
        echo date("Y-m-d H:i:s",strtotime());
    }


}