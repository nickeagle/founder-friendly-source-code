<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo_model extends CI_Model {

    /**
     * Class constructor
     *
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }


        public function get()
	    {
	        //write your query and get result

            $query=$this->db->query("select * from table_name where 1");

            $result=$query->result_array();

            return $result;

	    }


}
