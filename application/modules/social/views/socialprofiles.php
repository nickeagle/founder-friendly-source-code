<?php $this->load->view("shared/header.php");?>

    <body>
      <?php $this->load->view("shared/social_nav.php");

?>

  <div class="container-fluid">


    <div class="row" style="padding-bottom:30px;">

            <div class="col-md-9 col-sm-8">

                <h1>   Connect your accounts</h1>

            </div><!-- /.col -->

    </div>

<!-- Welcome alert -->

<div class="row">
<div style="margin-top:30px; margin-bottom:60px;" class="alert alert-danger" role="alert"><p>Due to Facebook making immediete changes to it's developer platform, we are unable to integrate with your Facebook or Instagram profile. This functionality will be live as soon as Facebook has made it's changes. We will keep you updated.</p></div>
</div>



<!-- connect buttons -->

<!-- external events drag and drop -->
    <div id='wrap'>

       <div id='external-events'>
         <h4>Connect You Accounts</h4>

         <?php

         $disable_facebook=($facebook_profile==false)?"":"disabled";
         $disable_twitter=($twitter_profile==false)?"":"disabled";
         $disable_instragram=($instragram_profile==false)?"":"disabled";

         //print_r($twitter_profile);


         ?>

         <?php
         /*
         if($social_profile!=false)
         {
           foreach ($social_profile as $key => $value) {
         ?>
           <div> <button class="btn btn-block btn-social <?php echo $value->btn_type; ?>" onClick="openpopup('<?php echo base_url(); ?>social/<?php echo $value->authname; ?>');">
           <span class="<?php echo $value->profile_icon; ?>"></span> Connect <?php echo $value->profile_name; ?> Profile</button>
         </div>
         <br>
         <?php
           }
         }
         */
         ?>

         <div> <button class="btn btn-block btn-social btn-twitter" onClick="openpopup('<?php echo base_url(); ?>social/twitterAuth');">
           <span class="fa fa-twitter"></span> Connect twitter Profile</button>
         </div>
         <br>

       </div>

    <!-- profile widgets -->
<div class="col-md-8">
    <div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading">Your connected accounts</div>
  <div style="padding:20px;">
  <!-- Table -->

                      <?php

  /*
                      if($facebook_profile!=false)
                      {
                        foreach ($facebook_profile as $key => $value) {
                          echo "<div class='connectaccount'><img src='https://graph.facebook.com/".$value['id']."/picture?type=square' alt='".$value['name']."'><span class='fa fa-facebook' style='background-color:#3b5998;'></span><i class='fa fa-trash del' style='color:#3b5998;'></i></div>";
                        }
                      }

                      if($twitter_profile!=false)
                      {
                        foreach ($twitter_profile as $key => $value) {
                           echo "<div class='connectaccount'><img src='".$value->profile_image_url."' alt='".$value->name."'><span class='fa fa-twitter' style='background-color:#55acee;'></span><i class='fa fa-trash del' style='color:#55acee;'></i></div>";
                        }
                      }

                      if($instragram_profile!=false)
                      {
                        foreach ($instragram_profile as $key => $value) {
                         echo "<div class='connectaccount'><img src='https://graph.facebook.com/".$value['id']."/picture?type=square' alt='".$value['name']."'><span class='fa fa-instagram' style='background-color:#3f729b;'></span><i class='fa fa-trash del' style='color:#3f729b;' ></i></div>";
                        }
                      }

  */

                      if($all_social_profile!=false)
                      {
                          foreach ($all_social_profile as  $profile) {

                              foreach ($profile['user_profile'] as $key => $user_profile) {

                                switch ($profile['profile_id']) {
                                  case '1':
                                    $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                    break;

                                    case '2':
                                    $imgUrl=$user_profile['profile']->profile_image_url;
                                    break;
                                    case '3':
                                    $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                    break;

                                }

                                  echo "<div class='connectaccount'><img src='".$imgUrl."' alt=''><span class='".$profile['profile_icon']."' style='background-color:".$profile['profile_default_color'].";'></span><i class='fa fa-trash del' style='color:rgb(244, 67, 54);' onclick='delSocial(".$user_profile['id'].");'></i></div>";

                              }

                          }
                      }

                      /*
                        if($facebook_profile!=false)
                        {
                          echo "<div class='connectaccount'><img src='https://graph.facebook.com/".$facebook_profile['id']."/picture?type=square' alt='".$facebook_profile['name']."'><span class='fa fa-facebook' style='background-color:#3b5998;'></span><i class='fa fa-trash del' style='color:#3b5998;'></i></div>";
                        }
                        if($twitter_profile!=false)
                        {
                          echo "<div class='connectaccount'><img src='".$twitter_profile->profile_image_url."' alt='".$twitter_profile->name."'><span class='fa fa-twitter' style='background-color:#55acee;'></span><i class='fa fa-trash del' style='color:#55acee;'></i></div>";
                        }
                        if($instragram_profile!=false)
                        {
                          echo "<div class='connectaccount'><img src='https://graph.facebook.com/".$instragram_profile['id']."/picture?type=square' alt='".$instragram_profile['name']."'><span class='fa fa-instagram' style='background-color:#3f729b;'></span><i class='fa fa-trash del' style='color:#3f729b;'></i></div>";
                        }*/
                      ?>
                      <div class="row">
                      <span id="message" style="color: #ff0000;font-size: 10px;padding-left: 10px;"></span>
                    </div>
                    </div>

</div>
</div>
</div>


<div>

<!-- new buttons -->



<!--
             <div class="col-md-4"> <button class="btn btn-block btn-social btn-facebook" <?php echo $disable_facebook; ?> onClick="openpopup('<?php echo base_url(); ?>social/facebookAuth');">
                <span class="fa fa-facebook"></span> Connect Facebook Page/Profile</button>
              </div>
             <div class="col-md-4"><button class="btn btn-block btn-social btn-twitter" <?php echo $disable_twitter; ?> onClick="openpopup('<?php echo base_url(); ?>social/twitterAuth');">
                <span class="fa fa-twitter"></span> Connect Twitter Profile</button>
              </div>
             <div class="col-md-4"> <button class="btn btn-block btn-social btn-instagram" <?php echo $disable_instragram; ?> onClick="openpopup('<?php echo base_url(); ?>social/instragramAuth');">
                <span class="fa fa-instagram"></span> Connect Instragram Profile</button>
              </div>
-->






<script type="text/javascript">
  var popup;
  function openpopup(url)
  {
        popup = window.open(url, "Popup", "width=600,height=500");
        popup.focus();
  }

  function delSocial(id)
  {

    $.ajax({
          method: 'post',
          url: '<?php echo base_url(); ?>social/deleteProfile',
          data: {
            id:id
          },
          dataType: 'json',
          success: function(data){
            console.log(data);

            $("#message").html(data.message);

            setTimeout(function(){location.reload();},4000);
          }
        });
  }
</script>

</html>
