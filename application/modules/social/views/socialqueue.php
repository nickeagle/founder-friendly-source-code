<?php $this->load->view("shared/header.php");?>
    <body>
      <?php $this->load->view("shared/social_nav.php");

?>
  <?php
          include( 'assets/social/social-stream.php' ); // Path to PHP Social Stream main file
          ?>

  <div class="container-fluid">




    <div class="row" style="padding-bottom:30px;">

            <div class="col-md-9 col-sm-8">

                <h1>   Social Queue</h1>

            </div><!-- /.col -->

    </div>



    <!-- social media search and display -->
    <?php

    if(isset($_GET['search']))
    {
         $search = htmlspecialchars($_GET["search"]);
    }
    else
    {
        $search='';
    }

   


    echo social_stream(
        array(
            'id' => '1',
            'type' => 'timeline',
            'network' => array(
                'twitter' => array(
                    'twitter_id_1' => array(
                        'axentmedia' // Twitter username 1
                    ),
                    'twitter_id_3' => array(
                        '#cats', // Twitter hashtag 1
                        '#media' // Twitter hashtag 2
                    ),
                    'twitter_images' => 'small',
                    'twitter_feeds' => 'retweets,replies'
                ),
                'instagram' => array(
                    'instagram_id_1' => array(
                        '500px', // Instagram username 1
                        'envato' // Instagram username 2
                    ),
                    'instagram_id_2' => array(
                        $search // Instagram hashtag 1
                         // Instagram hashtag 2
                    )
                )
            ),
            'theme' => 'sb-modern-light',
            'itemwidth' => 250,
            'results' => 30,
            'debuglog' => 0,
            'add_files' => true
        )
    );
?>

    </div>



</html>
