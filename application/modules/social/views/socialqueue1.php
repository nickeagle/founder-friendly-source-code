<?php $this->load->view("shared/header.php");?>
    <body>
      <?php $this->load->view("shared/social_nav.php");

?>
 <link href="assets/social/public/css/colorbox.css" rel="stylesheet" type="text/css" /><link href="assets/social/public/css/styles.min.css" rel="stylesheet" type="text/css" /><link href="assets/social/public/css/timeline-styles.css" rel="stylesheet" type="text/css" /></script><script type="text/javascript" src="assets/social/public/js/sb-utils.js"></script><script type="text/javascript" src="assets/social/public/js/sb-timeline.js"></script><style type="text/css">#timeline_timeline1.sboard .origin-flag.sb-facebook {
    background-color: rgba(48, 87, 144, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-facebook:after {
    border-left: 8px solid rgba(48, 87, 144, 1) !important
}

#timeline_timeline1.sboard .bg-facebook {
    background-color: rgba(48, 87, 144, 0.8) !important
}

#timeline_timeline1.sboard .sb-facebook.sb-hover:hover,
#timeline_timeline1.sboard .sb-facebook.active {
    background-color: #305790 !important;
    border-color: #305790 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-twitter {
    background-color: rgba(6, 208, 254, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-twitter:after {
    border-left: 8px solid rgba(6, 208, 254, 1) !important
}

#timeline_timeline1.sboard .bg-twitter {
    background-color: rgba(6, 208, 254, 0.8) !important
}

#timeline_timeline1.sboard .sb-twitter.sb-hover:hover,
#timeline_timeline1.sboard .sb-twitter.active {
    background-color: #06d0fe !important;
    border-color: #06d0fe !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-google {
    background-color: rgba(192, 77, 46, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-google:after {
    border-left: 8px solid rgba(192, 77, 46, 1) !important
}

#timeline_timeline1.sboard .bg-google {
    background-color: rgba(192, 77, 46, 0.8) !important
}

#timeline_timeline1.sboard .sb-google.sb-hover:hover,
#timeline_timeline1.sboard .sb-google.active {
    background-color: #c04d2e !important;
    border-color: #c04d2e !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-tumblr {
    background-color: rgba(46, 78, 101, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-tumblr:after {
    border-left: 8px solid rgba(46, 78, 101, 1) !important
}

#timeline_timeline1.sboard .bg-tumblr {
    background-color: rgba(46, 78, 101, 0.8) !important
}

#timeline_timeline1.sboard .sb-tumblr.sb-hover:hover,
#timeline_timeline1.sboard .sb-tumblr.active {
    background-color: #2E4E65 !important;
    border-color: #2E4E65 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-delicious {
    background-color: rgba(45, 110, 174, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-delicious:after {
    border-left: 8px solid rgba(45, 110, 174, 1) !important
}

#timeline_timeline1.sboard .bg-delicious {
    background-color: rgba(45, 110, 174, 0.8) !important
}

#timeline_timeline1.sboard .sb-delicious.sb-hover:hover,
#timeline_timeline1.sboard .sb-delicious.active {
    background-color: #2d6eae !important;
    border-color: #2d6eae !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-pinterest {
    background-color: rgba(203, 18, 24, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-pinterest:after {
    border-left: 8px solid rgba(203, 18, 24, 1) !important
}

#timeline_timeline1.sboard .bg-pinterest {
    background-color: rgba(203, 18, 24, 0.8) !important
}

#timeline_timeline1.sboard .sb-pinterest.sb-hover:hover,
#timeline_timeline1.sboard .sb-pinterest.active {
    background-color: #cb1218 !important;
    border-color: #cb1218 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-flickr {
    background-color: rgba(255, 1, 133, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-flickr:after {
    border-left: 8px solid rgba(255, 1, 133, 1) !important
}

#timeline_timeline1.sboard .bg-flickr {
    background-color: rgba(255, 1, 133, 0.8) !important
}

#timeline_timeline1.sboard .sb-flickr.sb-hover:hover,
#timeline_timeline1.sboard .sb-flickr.active {
    background-color: #ff0185 !important;
    border-color: #ff0185 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-instragram {
    background-color: rgba(41, 84, 119, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-instragram:after {
    border-left: 8px solid rgba(41, 84, 119, 1) !important
}

#timeline_timeline1.sboard .bg-instragram {
    background-color: rgba(41, 84, 119, 0.8) !important
}

#timeline_timeline1.sboard .sb-instragram.sb-hover:hover,
#timeline_timeline1.sboard .sb-instragram.active {
    background-color: #295477 !important;
    border-color: #295477 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-youtube {
    background-color: rgba(184, 0, 0, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-youtube:after {
    border-left: 8px solid rgba(184, 0, 0, 1) !important
}

#timeline_timeline1.sboard .bg-youtube {
    background-color: rgba(184, 0, 0, 0.8) !important
}

#timeline_timeline1.sboard .sb-youtube.sb-hover:hover,
#timeline_timeline1.sboard .sb-youtube.active {
    background-color: #b80000 !important;
    border-color: #b80000 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-vimeo {
    background-color: rgba(0, 160, 220, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-vimeo:after {
    border-left: 8px solid rgba(0, 160, 220, 1) !important
}

#timeline_timeline1.sboard .bg-vimeo {
    background-color: rgba(0, 160, 220, 0.8) !important
}

#timeline_timeline1.sboard .sb-vimeo.sb-hover:hover,
#timeline_timeline1.sboard .sb-vimeo.active {
    background-color: #00a0dc !important;
    border-color: #00a0dc !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-stumbleupon {
    background-color: rgba(236, 68, 21, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-stumbleupon:after {
    border-left: 8px solid rgba(236, 68, 21, 1) !important
}

#timeline_timeline1.sboard .bg-stumbleupon {
    background-color: rgba(236, 68, 21, 0.8) !important
}

#timeline_timeline1.sboard .sb-stumbleupon.sb-hover:hover,
#timeline_timeline1.sboard .sb-stumbleupon.active {
    background-color: #ec4415 !important;
    border-color: #ec4415 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-deviantart {
    background-color: rgba(73, 93, 81, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-deviantart:after {
    border-left: 8px solid rgba(73, 93, 81, 1) !important
}

#timeline_timeline1.sboard .bg-deviantart {
    background-color: rgba(73, 93, 81, 0.8) !important
}

#timeline_timeline1.sboard .sb-deviantart.sb-hover:hover,
#timeline_timeline1.sboard .sb-deviantart.active {
    background-color: #495d51 !important;
    border-color: #495d51 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-rss {
    background-color: rgba(215, 139, 45, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-rss:after {
    border-left: 8px solid rgba(215, 139, 45, 1) !important
}

#timeline_timeline1.sboard .bg-rss {
    background-color: rgba(215, 139, 45, 0.8) !important
}

#timeline_timeline1.sboard .sb-rss.sb-hover:hover,
#timeline_timeline1.sboard .sb-rss.active {
    background-color: #d78b2d !important;
    border-color: #d78b2d !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-soundcloud {
    background-color: rgba(255, 51, 0, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-soundcloud:after {
    border-left: 8px solid rgba(255, 51, 0, 1) !important
}

#timeline_timeline1.sboard .bg-soundcloud {
    background-color: rgba(255, 51, 0, 0.8) !important
}

#timeline_timeline1.sboard .sb-soundcloud.sb-hover:hover,
#timeline_timeline1.sboard .sb-soundcloud.active {
    background-color: #ff3300 !important;
    border-color: #ff3300 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-vk {
    background-color: rgba(76, 117, 163, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-vk:after {
    border-left: 8px solid rgba(76, 117, 163, 1) !important
}

#timeline_timeline1.sboard .bg-vk {
    background-color: rgba(76, 117, 163, 0.8) !important
}

#timeline_timeline1.sboard .sb-vk.sb-hover:hover,
#timeline_timeline1.sboard .sb-vk.active {
    background-color: #4c75a3 !important;
    border-color: #4c75a3 !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-linkedin {
    background-color: rgba(24, 132, 188, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-linkedin:after {
    border-left: 8px solid rgba(24, 132, 188, 1) !important
}

#timeline_timeline1.sboard .bg-linkedin {
    background-color: rgba(24, 132, 188, 0.8) !important
}

#timeline_timeline1.sboard .sb-linkedin.sb-hover:hover,
#timeline_timeline1.sboard .sb-linkedin.active {
    background-color: #1884BC !important;
    border-color: #1884BC !important;
    color: #fff !important
}

#timeline_timeline1.sboard .origin-flag.sb-vine {
    background-color: rgba(57, 169, 123, 0.8) !important
}

#timeline_timeline1.sboard .origin-flag.sb-vine:after {
    border-left: 8px solid rgba(57, 169, 123, 1) !important
}

#timeline_timeline1.sboard .bg-vine {
    background-color: rgba(57, 169, 123, 0.8) !important
}

#timeline_timeline1.sboard .sb-vine.sb-hover:hover,
#timeline_timeline1.sboard .sb-vine.active {
    background-color: #39a97b !important;
    border-color: #39a97b !important;
    color: #fff !important
}

#timeline_timeline1.sboard .sb-item .sb-container {
    background-color: #ffffff;
    border: 1px solid #e5e5e5;
    display: inline-block;
    width: 100%;
    box-shadow: 2px 2px 5px #e5e5e5;
}

#timeline_timeline1.sboard.sb-modern .sb-item .sb-footer a {
    color: rgba(0, 0, 0, 0.8) !important
}

#timeline_timeline1.sboard,
#timeline_timeline1.sboard a {
    font-size: 11px
}

#timeline_timeline1.sboard .sb-heading {
    font-size: 12px !important
}

#timeline_timeline1.sboard .timeline-row small {
    color: #000000
}

#timeline_timeline1.sboard .timeline-row .sb-title a {
    color: #000000
}

#timeline_timeline1.sboard .timeline-row {
    color: rgba(0, 0, 0, 0.8)
}

#timeline_timeline1.sboard .timeline-row a {
    color: #305790
}

#timeline_timeline1.sboard .timeline-row a:visited {
    color: rgba(48, 87, 144, 0.8)
}

#timeline_timeline1.sboard .sb-content {
    border-bottom-left-radius: 0 !important;
    border-bottom-right-radius: 0 !important
}
.sboard .sb-item .sb-thumb img {

    border-radius: 0px 4px 4px 0 !important;

}

.loader2 {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}


   .modalDialog {
    position: fixed;
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: #cccccc;
    z-index: 99999;
    opacity: 0;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
    opacity: 1;
    pointer-events: auto;
    display: none;
}
.modalDialog > div {
    width: 100%;
    position: relative;
    margin: 20% auto;
}

</style>

<div id="openLoading" class="modalDialog"><div align='center' ><div class='loader2'></div></div></div>
 <!-- edit modal pop up -->

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Post</h4>
      </div>
      <div class="modal-body">




            <div class="row">

                <div class="col-lg-12 col-sm-12 col-xs-12">

                        <form id="socialPostUpdate" class="socialPost" method="post" enctype="multipart/form-data" role="form">
                          <ul style=" padding-left:2px;"  class="list-inline list_PostActions" id="list_PostActionsUpdate">

                            <?php
                               if($all_social_profile!=false)
                                  {
                                      foreach ($all_social_profile as  $profile) {

                                          foreach ($profile['user_profile'] as $key => $user_profile) {

                                            switch ($profile['profile_id']) {
                                              case '1':
                                              $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                              break;

                                              case '2':
                                              $imgUrl=$user_profile['profile']->profile_image_url_https;
                                              break;
                                              case '3':
                                              $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                              break;
                                            }



                                              echo "<li>
                                                  <div class='connectaccount' data-id='".$user_profile['id']."'><img src='".$imgUrl."' ><span class='".$profile['profile_icon']."' style='background-color:".$profile['profile_default_color'].";'></span></div>
                                              </li>";

                                          }

                                      }
                                  }
                            ?>



                        </ul>
                       <input type='hidden' name='updatepostid' id="updatepostid" style="display: none;" value="">
                        <input type='file' name='uploadimageUpdate' id="uploadimageUpdate" style="display: none;">
                        <input type="checkbox" name="timeschedulecheck" id="timeschedulecheckUpdate" style="display: none;">
                        <div class="form-group">
                                <textarea style="margin-bottom:5px; " class="form-control" name="postmessageUpdate" id="postmessageUpdate" placeholder="What's on your mind?"></textarea>
                        </div>
                        <div class="form-group">
                                <input type="input" class="form-control datepicker" name="timescheduleUpdate" id="timescheduleUpdate" placeholder="Post Time" readonly="" >
                        </div>
                        <ul style="margin-bottom:5px; padding-left:2px;" class="list-inline post-actions" id="list-post">
                          <li><span id="addimageUpdate" class="pointer"><span class="glyphicon glyphicon-camera"></span></span></li>


                          <li class="pull-right">


                            <span class="btn btn-primary btn-xs pointer btn-color" id="addpostUpdate" >Update Post</span>
                          </li>
                        </ul>

                     <div id="posterrUpdate" style="color: #ff0000;"></div>
                     <div class="col-lg-4 col-sm-8 imgPos" style="display: none;"><img src="" id="imageFileUpdate" class="img-responsive img-rounded" ><i class="fa fa-times-circle imgDel"></i></div>
                     <div class="col-lg-4 col-sm-8 imgPos" id="postimage" style="display: none;"></div>
                      <!--
                        <div class="col-md-4 center-block">

                          <button type="button" class="btn btn-default btn-sm">
                                    <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                        </div> -->
                      </form>
                  </div>



            </div>


      </div>
      <div class="modal-footer">

        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>

  <div class="container-fluid">




    <div class="row" style="padding-bottom:30px;">

            <div class="col-md-9 col-sm-8">

                <h1>   Social Queue</h1>

            </div><!-- /.col -->

    </div>




    <!-- social media search and display -->





<div id="sb_timeline1">
    <div id="timeline_timeline1" class="sboard sb-modern sb-modern-light timeline animated" data-columns>


        <?php



            if($all_post!=FALSE)
            {
                $this->load->module('social');

                foreach ($all_post as $key => $post) {

                    if($post->status==1 && $post->postdate>date("Y-m-d H:i:s",time()))
                    {


                                    switch ($post->profile_id) {
                                              case '1':
                                              $imgUrl="https://graph.facebook.com/".$post->auth_user_id."/picture?type=square";
                                              //$name=$user_profile['profile']['name'];
                                              $fb_name=$this->social->getfacebookProfile($post->auth_token);
                                              $name=$fb_name['name'];
                                              $uname='@'.$post->auth_user_id;
                                              $ulink='https://facebook.com'.$post->auth_user_id;

                                              break;

                                              case '2':

                                              $twitter_name=$this->social->getTwitterProfile($post->auth_token,$post->auth_token_secrect);
                                              $imgUrl=$twitter_name->profile_image_url_https;
                                              $name=$twitter_name->name;
                                              $uname='@'.$post->auth_user_id;
                                              $ulink='https://twitter.com'.$post->auth_user_id;
                                              break;
                                              case '3':
                                              $imgUrl="https://graph.facebook.com/".$post->auth_user_id."/picture?type=square";
                                              $fb_name=$this->social->getInstragramProfile($post->auth_token);
                                              $name=$fb_name['name'];
                                              $uname='@'.$post->auth_user_id;
                                              $ulink='https://facebook.com'.$post->auth_user_id;
                                              break;
                                            }


        ?>

                 <div class="timeline-row" id="post<?php echo $post->post_id; ?>">
            <div class="timeline-time"> <small></small><?php echo date("F d , Y",strtotime($post->postdate)); ?></small> &nbsp;<?php echo date("H:i",strtotime($post->postdate)); ?> </div>
            <div class="timeline-icon post_edit" data-id="<?php echo $post->post_id; ?>"  data-social="<?php echo $post->user_social_id; ?>" data-attr="<?php echo $post->profile_name; ?>" data-img="<?php echo $post->imagetype; ?>">
                <div class="bg-<?php echo $post->profile_name; ?>" title="edit post" style='cursor:pointer;z-index:99999999999;' > <i class="sb-bico sb-wico sb-pencil"></i> </div>
            </div>
            <div class="timeline-content">
                <div class="panel-body sb-item sb-<?php echo $post->profile_name; ?> <?php echo $post->profile_name; ?>-3-1">
                    <div class="sb-container sb-imgexpand sb-nothumb"><span class="origin-flag sb-<?php echo $post->profile_name; ?>"><i class="sb-icon sb-<?php echo $post->profile_name; ?>"></i></span>
                        <?php
                            if($post->imagetype!="")
                            {
                        ?>
                        <div class="sb-thumb" style="float: right; width: 30%;">
                              <a href="<?php echo base_url('social_image/'.$post->post_id.'.'.$post->imagetype); ?>" class="icbox cboxElement" data-size="1536,1920" rel="nofollow" target="_blank"><img data-original="<?php echo base_url('social_image/'.$post->post_id.'.'.$post->imagetype); ?>" src="<?php echo base_url('social_image/'.$post->post_id.'.'.$post->imagetype); ?>" alt="" style="display: block;"></a>
                        </div>
                        <?php
                        }
                        ?>

                        <div class="sb-inner" style="float: left;width: 67%;">
                            <div class="sb-user">
                                <div class="sb-uthumb">
                                    <a href="#" rel="nofollow" target="_blank"><img alt="#" src="<?php echo $imgUrl; ?>"></a>
                                </div>
                                <div class="sb-uinfo">
                                    <div class="sb-utitle"><a href="#" rel="nofollow" target="_blank"><?php echo $name; ?></a></div>
                                    <div class="sb-uname"><a href="<?php echo $ulink; ?>" rel="nofollow" target="_blank"><?php echo $uname; ?></a></div>
                                </div>
                            </div><span class="sb-text sb-expand sb-notitle"><?php echo $post->message; ?></span>

                            <div class="sb-info"> <span class="sb-share sb-tweet" style="float: left;">
                                <input type="hidden" id="postmessage<?php echo $post->post_id; ?>" value="<?php echo $post->message; ?>">
                                <input type="hidden" id="posttime<?php echo $post->post_id; ?>" value="<?php echo $post->postdate; ?>">
                                                      <span title="delete post" style='float:left; font-size:16px;cursor:pointer;z-index:99999999999;' onclick='deleteevent("<?php echo $post->post_id; ?>");'><i class='fa fa-trash del' style='color:rgb(244, 67, 54);'></i></span>
                                </span> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php
                    }
                }

            }

            else
            {
                echo "<center><p>There is no schedules posts in the queue</p></center>";
            }
        ?>



    </div>

</div>


<script type="text/javascript">
    $(document).ready(function(){

        var base_url='<?php echo base_url(); ?>';

        $(".post_edit").click(function(){

            var social_id=$(this).attr("data-social");
            var id=$(this).attr("data-id");
             var message=$("#postmessage"+id).val();
             var timeschedule=$("#posttime"+id).val();
             var imag=$(this).attr("data-img");
            $('#list_PostActionsUpdate li').each(function(){
                if($(this).find("div").attr("data-id")==social_id)
                {
                     $(this).css('display','inline');
                  $(this).addClass('active');
                }
                else
                {
                  $(this).css('display','none');
                }
          });

          $("#socialPostUpdate").append("<input type='hidden' name='postinto[]' data-id='postinto"+social_id+"' value='"+social_id+"'>");
          $("#postmessageUpdate").val(message);
          $("#timescheduleUpdate").val(timeschedule);
          $("#postimage").html("<img src='"+base_url+"social_image/"+id+"."+imag+"' class='img-responsive img-rounded'>");
          if(imag!=''){$("#postimage").css('display','inline');}
          $("#updatepostid").val(id);


            $('#editModal').modal();
        });



 $("#addimageUpdate").click(function(e){

      $('#uploadimageUpdate').click();


    });


 $(":file").on("change", function(e) {

      $('#imageFileUpdate').attr('src','');
      var input=this;

      var file = this.files[0];
      var fileType = file["type"];
      var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
      if ($.inArray(fileType, ValidImageTypes) < 0) {
          alert("Upload file is not a image");
          this.value=="";
      }
      else
      {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {

                $('#imageFileUpdate').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
            $('.imgPos').css("display","inline");
        }
      }

  });

    $(".imgDel").click(function(){

        $('#uploadimage').val('');

        $('#uploadimageUpdate').val('');
        $('.imgPos').css("display","none");
    });


$("#addpostUpdate").click(function(){

      var postmessage=$("#postmessageUpdate").val();
      var postime=$("#timescheduleUpdate").val();
      var postinto=new Array();

      $("input[name='postinto[]']").each(function(){

        postinto.push($(this).val());

      });

$("#postmessageUpdate").css('border-color','#d5dbdb');
$("#timescheduleUpdate").css('border-color','#d5dbdb');

if(postmessage.trim()=="")
   {
    $("#posterrUpdate").html("Enter the message !");
    $("#postmessageUpdate").css('border-color','#ff0000');
   }
   else if(postinto.length===0)
   {
    $("#posterrUpdate").html("Select atleast one social profile !");

   }
   else if(postime.trim()=="")
   {
    $("#posterrUpdate").html("Select atleast one social profile !");
    $("#timescheduleUpdate").css('border-color','#ff0000');
   }
   else
   {

    $("#openLoading").css("display","block");
        $.ajax({
          method: 'post',
          url: '<?php echo base_url(); ?>social/updatepost',
          data: new FormData($('#socialPostUpdate')[0]),
          dataType:'json',
          contentType: false,
            processData:false,
          success: function(data){

           if(data.response==false)
            {

              notify(data.message,"danger","body");
            }
            else
            {
              notify(data.message,"success","body");
            }

            //$("#posterr").html(data.message);

            $('#editModal').modal('hide');

            $("#openLoading").css("display","none");

            setTimeout(function(){location.reload();},1000);
          }
        });

    }

    });



    });







function deleteevent(id) {


            $.ajax({
                  method: 'post',
                   url: '<?php echo base_url() ?>social/deletePost',
                   dataType: 'json',
                   data: {
                   eventid :id

                   },

                   success: function(data) {


                       if(data.response==false)
                        {
                          notify(data.message,"danger");
                        }
                        else
                        {
                            $("#post"+id).remove();
                          notify(data.message,"success",'body');

                        }

                   }
                   });


}




function notify(message,type,element='.modal')
{

$.notify({
  // options
  icon: 'glyphicon',

  message: message,

},{
  // settings
  element: element,
  position: null,
  type: type,
  allow_dismiss: false,
  newest_on_top: false,
  showProgressbar: false,
  placement: {
    from: "top",
    align: "right"
  },
  offset: 20,
  spacing: 10,
  z_index: 1031,
  delay: 1000,
  timer: 1000,

  mouse_over: null,
  animate: {
    enter: 'animated fadeInDown',
    exit: 'animated fadeOutUp'
  }

});
}





</script>
