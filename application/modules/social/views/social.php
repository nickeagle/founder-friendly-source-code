<?php $this->load->view("shared/header.php");?>

    <body>
      <?php $this->load->view("shared/social_nav.php");

?>




    <!-- edit modal pop up -->

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Update Post</h4>
      </div>
      <div class="modal-body">




            <div class="row">

                <div class="col-lg-12 col-sm-12 col-xs-12">

                        <form id="socialPostUpdate" class="socialPost" method="post" enctype="multipart/form-data" role="form">
                          <ul style=" padding-left:2px;"  class="list-inline list_PostActions" id="list_PostActionsUpdate">

                            <?php
                               if($all_social_profile!=false)
                                  {
                                      foreach ($all_social_profile as  $profile) {

                                          foreach ($profile['user_profile'] as $key => $user_profile) {

                                            switch ($profile['profile_id']) {
                                              case '1':
                                              $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                              break;

                                              case '2':
                                              $imgUrl=$user_profile['profile']->profile_image_url_https;
                                              break;
                                              case '3':
                                              $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                              break;
                                            }



                                              echo "<li>
                                                  <div class='connectaccount' data-id='".$user_profile['id']."'><img src='".$imgUrl."' ><span class='".$profile['profile_icon']."' style='background-color:".$profile['profile_default_color'].";'></span></div>
                                              </li>";

                                          }

                                      }
                                  }
                            ?>



                        </ul>
                       <input type='hidden' name='updatepostid' id="updatepostid" style="display: none;" value="">
                        <input type='file' name='uploadimageUpdate' id="uploadimageUpdate" style="display: none;">
                        <input type="checkbox" name="timeschedulecheck" id="timeschedulecheckUpdate" style="display: none;">
                        <div class="form-group">
                                <textarea style="margin-bottom:5px; " class="form-control" name="postmessageUpdate" id="postmessageUpdate" placeholder="What's on your mind?"></textarea>
                        </div>
                        <div class="form-group">
                                <input type="input" class="form-control datepicker" name="timescheduleUpdate" id="timescheduleUpdate" placeholder="Post Time" readonly="" >
                        </div>
                        <ul style="margin-bottom:5px; padding-left:2px;" class="list-inline post-actions" id="list-post">
                          <li><span id="addimageUpdate" class="pointer"><span class="glyphicon glyphicon-camera"></span></span></li>


                          <li class="pull-right">


                            <span class="btn btn-primary btn-xs pointer btn-color" id="addpostUpdate" >Update Post</span>
                          </li>
                        </ul>

                     <div id="posterrUpdate" style="color: #ff0000;"></div>
                     <div class="col-lg-4 col-sm-8 imgPos" style="display: none;"><img src="" id="imageFileUpdate" class="img-responsive img-rounded" ><i class="fa fa-times-circle imgDel"></i></div>
                       <div class="col-lg-4 col-sm-8 imgPos" id="postimage" style="display: none;"></div>
                      <!--
                        <div class="col-md-4 center-block">

                          <button type="button" class="btn btn-default btn-sm">
                                    <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                        </div> -->
                      </form>
                  </div>



            </div>


      </div>
      <div class="modal-footer">

        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>


<!-- model pop up for adding an event -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Post</h4>
      </div>
      <div class="modal-body">



            <div class="row">

                <div class="col-lg-12 col-sm-12 col-xs-12">

                        <form id="socialPost" class="socialPost" method="post" enctype="multipart/form-data" role="form">
                          <ul style=" padding-left:2px;"  class="list-inline list_PostActions" id="list_PostActions">

                            <?php
                               if($all_social_profile!=false)
                                  {
                                      foreach ($all_social_profile as  $profile) {

                                          foreach ($profile['user_profile'] as $key => $user_profile) {

                                            switch ($profile['profile_id']) {
                                              case '1':
                                              $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                              break;

                                              case '2':
                                              $imgUrl=$user_profile['profile']->profile_image_url_https;
                                              break;
                                              case '3':
                                              $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                              break;
                                            }



                                              echo "<li>
                                                  <div class='connectaccount' data-id='".$user_profile['id']."'><img src='".$imgUrl."' title='' alt='' ><span class='".$profile['profile_icon']."' style='background-color:".$profile['profile_default_color'].";'></span></div>
                                              </li>";

                                          }

                                      }
                                  }
                                  else
                                  {
                                    echo "<a href='".base_url('social-profiles')."' style='font-size:12px;'>No Linked accounts ,Please connect your social profile</a>";
                                  }
                            ?>



                        </ul>

                        <input type='file' name='uploadimage' class="uploadimage" id="uploadimage" style="display: none;">
                        <input type="checkbox" name="timeschedulecheck" id="timeschedulecheck" style="display: none;">
                        <div class="form-group">
                                <textarea style="margin-bottom:5px; " class="form-control" name="postmessage" id="postmessage" placeholder="What's on your mind?"></textarea>
                        </div>
                        <div class="form-group">
                                <input type="input" class="form-control datepicker" name="timeschedule" id="timeschedule" placeholder="Post Time" readonly="" style="display: none;">
                        </div>
                        <ul style="margin-bottom:5px; padding-left:2px;" class="list-inline post-actions" id="list-post">
                          <li><span id="addimage" class="pointer icon-color"><span class="glyphicon glyphicon-camera"></span></span></li>
                          <li><span id="addschedule"  class="pointer icon-color"><span class="glyphicon glyphicon-time"></span></span></li>

                          <li class="pull-right">
                              <span class="btn btn-info btn-xs pointer btn-color" id="autoschedule">Auto Schedule</span>
                            <span class="btn btn-primary btn-xs pointer btn-color" id="postnow">Post Now</span>
                            <span class="btn btn-primary btn-xs pointer btn-color" id="addpost" style="display: none;">Add Post</span>
                          </li>
                        </ul>

                     <div id="posterr" style="color: #ff0000;"></div>
                     <div class="col-lg-4 col-sm-8 imgPos" style="display: none;"><img src="" id="imageFile" class="img-responsive img-rounded imageFile" ><i class="fa fa-times-circle imgDel"></i></div>
                   
                      <!--
                      <!--
                        <div class="col-md-4 center-block">

                          <button type="button" class="btn btn-default btn-sm">
                                    <span class="glyphicon glyphicon-plus"></span>
                                  </button>
                        </div> -->
                      </form>
                  </div>



            </div>


      </div>


      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

<!-- end model popup -->

<div class="row">
    <div class="col-md-12">


<!-- Calender add event popup-->


<!-- end popup -->

  <div class="container-fluid">
        <div class="row">

              <div class="col-md-9 col-sm-8">

                  <h1>   Social Calender</h1>

              </div><!-- /.col -->

        <div class="col-md-3 col-sm-4 text-right">

            <a onclick="addevent()" class="btn btn-lg btn-primary btn-embossed btn-wide margin-top-40"><span class="fui-plus"></span> Create New Post</a>

        </div><!-- /.col -->

    </div>
    <div class="row">


          <?php
               if($all_social_profile!=false)
                  {

          ?>
          <div class="col-lg-3 col-sm-12 col-xs-12">

            <div class="dropdown">
              <button class="btn btn-primary dropdown-toggle" id="social_network" type="button" data-toggle="dropdown">Filter By Social Network
              <span class="caret"></span></button>
              <ul class="dropdown-menu social_filter">
                <li><div class='filter_social' data-id=''>All Social Post</div></li>
            <?php
                foreach ($all_social_profile as  $profile) {

                                          foreach ($profile['user_profile'] as $key => $user_profile) {

                                            switch ($profile['profile_id']) {
                                              case '1':
                                              $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                              $name=$user_profile['profile']['name'];
                                              break;

                                              case '2':
                                              $imgUrl=$user_profile['profile']->profile_image_url_https;
                                              $name=$user_profile['profile']->name;
                                              break;
                                              case '3':
                                              $imgUrl="https://graph.facebook.com/".$user_profile['profile']['id']."/picture?type=square";
                                              $name=$user_profile['profile']['name'];
                                              break;
                                            }



                                              echo "<li>
                                                  <div class='filter_social' data-id='".$user_profile['id']."'><img src='".$imgUrl."' title='' alt='' ><span class='".$profile['profile_icon']."' style='background-color:".$profile['profile_default_color'].";'></span> &nbsp;&nbsp;".$name."</div>
                                              </li>";

                                          }

                                      }
                  ?>
              </ul>
            </div>



              </div>

            <?php
                  }

            ?>


    </div>

          <div style="margin-top:30px;" id="calendar">

          </div>


</div>

<script type="text/javascript">
$(document).ready(function() {
  $('#timeschedule').datetimepicker({
    format: 'yyyy-mm-dd hh:ii'
  });
  $('#timescheduleUpdate').datetimepicker({
    format: 'yyyy-mm-dd hh:ii'
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
        var base_url='<?php echo base_url(); ?>';

 var calHeight = 600;
var date_last_clicked = null;
   $('#calendar').fullCalendar({
    header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
      },
      views: {
    month: {
    columnFormat:'ddd'
    }
},

  handleWindowResize: true,
  defaultView: 'agendaWeek', // Only show week view
  minTime: '07:30:00', // Start time for the calendar
  maxTime: '23:59:00',
  allDaySlot: false, // End time for the calendar
  eventStartEditable: true,
  displayEventTime: true, // Display event time
  displayEventEnd: false,
  height:calHeight,
  //eventStartEditable: false,
    contentHeight:calHeight,

       eventSources: [
           {
               events: function(start, end, timezone, callback) {
                var id='';
                $(".filter_social").each(function(){
                   if($(this).parent('li').hasClass('disabled'))
                    {
                      id=$(this).attr("data-id");

                    }
                });

                   $.ajax({
                   url: '<?php echo base_url() ?>social/get_post',
                   dataType: 'json',
                   method:'post',
                   data: {
                   id:id,
                   start: start.unix(),
                   end: end.unix()

                   },
                   success: function(data) {
                       console.log(data.post);
                       if(data.response==true)

                       callback(data.post);
                   }
                   });
               }
           },
       ],
    dayClick: function(date, jsEvent, view) {
        date_last_clicked = $(this);

    },

eventRender: function(event, element) {
  element.find("div.fc-content-col").addClass("events"+event.social_id);
     if(event.icon){
        element.find(".fc-title").prepend("<i class='"+event.icon+" font'></i>  ");
     }
     if (event.imageurl) {
        element.find("div.fc-content").prepend("<img src='" + event.imageurl +"' width='25' height='25'>  ");

    }
    if (event.status==2) {
          element.css("opacity","0.6");
        }
  },
  eventMouseover: function(calEvent, domEvent) {

     if (calEvent.status==2) {
            return false;
        }
  var layer = "<div id='events-layer' class='fc-transparent' style='position:absolute; width:100%; height:100%; top:-1px; text-align:right; z-index:100'> <a> <span class='fa fa-trash-o' style='margin-right:10px;' onClick='return deleteEvent(event,"+calEvent.id+"); return false'></span></a></div>";
  $(this).append(layer);
},
eventMouseout: function(calEvent, domEvent) {
  $("#events-layer").remove();
},
select: function(start, end) {
    if(start.isBefore(moment())) {
        $('#calendar').fullCalendar('unselect');
        return false;
    }
},
 eventConstraint: {
            start: moment().format('YYYY-MM-DD HH:mm'),
            end: '2100-01-01' // hard coded goodness unfortunately
        },
eventDrop: function(event,dayDelta) { // called when an event (already on the calendar) is moved

     if (event.status==2) {
            return false;
        }
        updatePost(event.id,event.title,dayDelta._milliseconds,dayDelta._days);
      },

    eventClick: function(event, jsEvent, view) {
      if (event.status==2) {
            return false;
        }
        else
        {
          removeselected();
          $('#list_PostActionsUpdate li').each(function(){
            if($(this).find("div").attr("data-id")==event.social_id)
            {

              $(this).addClass('active');
              $(this).css('display','inline');
            }
            else
            {

              $(this).css('display','none');

            }
          });
          //$('#list_PostActionsUpdate div[data-id="'+event.social_id+'"]').parent().addClass('active');
          $("#socialPostUpdate").append("<input type='hidden' name='postinto[]' data-id='postinto"+event.social_id+"' value='"+event.social_id+"'>");
          $("#postmessageUpdate").val(event.title);
          $("#timescheduleUpdate").val(event.postime);
          $("#updatepostid").val(event.id);
          $("#postimage").html("<img src='"+base_url+"social_image/"+event.id+"."+event.imagetype+"' class='img-responsive img-rounded'>");
          if(event.imagetype!=''){$("#postimage").css('display','inline');}
          $('#editModal').modal();
        }
},

   });


});



function showCalender(id)
{

//$(".fc-content-col").css("display","none");
//$(".events"+id).css("display","block");
$("#calendar").fullCalendar('removeEvents');
 //$('#calendar').fullCalendar('removeEventSource', curSource[1]);

 $.ajax({
                   url: '<?php echo base_url() ?>social/get_post',
                   data:{
                    id:id
                   },
                   dataType: 'json',
                   method:'post',
                   success: function(data) {
                       console.log(data.post);
                       if(data.response==true)
                      $('#calendar').fullCalendar('addEventSource', data.post);

                   }
                   });


}


function updatePost(id,title,changemilisec,changeday)
{
   $.ajax({
          method: 'post',
           url: '<?php echo base_url() ?>social/updatePostByTime',
           dataType: 'json',
           data: {
           changemilisec: changemilisec,
           changeday:changeday,
           title:title,
           id: id
           },
           success: function(data) {
               console.log(data);
           }
         });


}



function addevent() {
$(this).css('background-color', '#bed7f3');
removeselected();
$('#addModal').modal();
}


function removeselected()
{
  $("input[name='postinto[]']").remove();
  $(".list_PostActions li").removeClass("active");
   $('.imageFileUpdate').attr('src','');
        $('.uploadimageUpdate').val('');
        $('.imgPos').css("display","none");
}


$(document).ready(function(){
  $(".list_PostActions .connectaccount").on("click",function(){

    var postIn=$(this).attr("data-id");
    if($(this).parent().hasClass("active"))
    {
      $(this).parent().removeClass("active");
      $("input[data-id='postinto"+postIn+"']").remove();
    }
    else
    {
      $(".socialPost").append("<input type='hidden' name='postinto[]' data-id='postinto"+postIn+"' value='"+postIn+"'>");
      $(this).parent().addClass("active");
    }

    //$("#list_PostActions").find("li").removeClass("active");

  });



  $("#addschedule").click(function(){

      if($("#timeschedulecheck").prop('checked')==true)
      {
        $("#timeschedulecheck").prop('checked',false);
        $("#timeschedule").css('display', 'none');
        $("#addpost").css('display', 'none');
        $("#postnow").css('display', 'inline');
        $("#autoschedule").removeAttr('disabled');
      }
      else
      {
      $("#timeschedulecheck").prop('checked', true);
      $("#timeschedule").css('display', 'inline');
      $("#addpost").css('display', 'inline');
      $("#postnow").css('display', 'none');
      $("#autoschedule").attr('disabled', '');
      }
  });
});





$(document).ready(function(){
    $("#addimage").click(function(e){

      $('#uploadimage').click();


    });


    $("#addimageUpdate").click(function(e){

      $('#uploadimageUpdate').click();


    });



  $(".social_filter li").click(function(){

      $("#social_network").html($(this).html());
      $(".social_filter li").removeClass("disabled");
      $(this).addClass("disabled");
      showCalender($(this).find('div').attr('data-id'));
  });
});


function validateSocial()
{
    var file =  $(":file").files[0];
    var fileType = file["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
    if ($.inArray(fileType, ValidImageTypes) < 0) {
         return false;
    }
    return true;
}


$(document).ready(function(){
    $(":file").on("change", function(e) {
      $('#imageFile').attr('src','');
      $('#imageFileUpdate').attr('src','');
      var input=this;

      var file = this.files[0];
      var fileType = file["type"];
      var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
      if ($.inArray(fileType, ValidImageTypes) < 0) {
          alert("Upload file is not a image");
          this.value=="";
      }
      else
      {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageFile').attr('src', e.target.result);
                $('#imageFileUpdate').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
            $('.imgPos').css("display","inline");
        }
      }

  });

    $(".imgDel").click(function(){
        $('#imageFile').attr('src','');
        $('#uploadimage').val('');
        $('#imageFileUpdate').attr('src','');
        $('#uploadimageUpdate').val('');
        $('.imgPos').css("display","none");
    });
});


$(document).ready(function(){
    $("#postnow").click(function(){

      var postmessage=$("#postmessage").val();
      var postinto=new Array();

      $("input[name='postinto[]']").each(function(){

        postinto.push($(this).val());

      });
   $("#postmessage").css('border-color','#d5dbdb');
   if(postmessage.trim()=="")
   {
    $("#posterr").html("Enter the message !");
    $("#postmessage").css('border-color','#ff0000');
   }
   else if(postinto.length===0)
   {
    $("#posterr").html("Select atleast one social profile !");

   }
   else
   {


        $.ajax({
          method: 'post',
          url: '<?php echo base_url(); ?>social/postnow',
          data:new FormData($('#socialPost')[0]),
          dataType:'json',
          contentType: false,
            processData:false,
          success: function(data){
            console.log(data);

            for(var i=0;i<data.response.length;i++)
            {
              if(data.response[i]==false)
              {

                notify(data.message[i],"danger");
              }
              else
              {

                notify(data.message[i],"success");
              }
            }


            //$("#posterr").html(data.message);

            setTimeout(function(){location.reload();},4000);
          }
        });

      }

    });



    $("#autoschedule").on('click',function(e){
      e.preventDefault();

      var postmessage=$("#postmessage").val();
      var postinto=new Array();

      $("input[name='postinto[]']").each(function(){

        postinto.push($(this).val());

      });
   $("#postmessage").css('border-color','#d5dbdb');
   if(postmessage.trim()=="")
   {
    $("#posterr").html("Enter the message !");
    $("#postmessage").css('border-color','#ff0000');
   }
   else if(postinto.length===0)
   {
    $("#posterr").html("Select atleast one social profile !");

   }
   else
   {


        $.ajax({
          method: 'post',
          url: '<?php echo base_url(); ?>social/autoschedule',
          data: new FormData($('#socialPost')[0]),
          dataType:'json',
          contentType: false,
            processData:false,
          success: function(data){
            console.log(data.response);

            if(data.response==false)
            {

              notify(data.message,"danger");
            }
            else
            {

              notify(data.message,"success");
            }

            //$("#posterr").html(data.message);

            setTimeout(function(){location.reload();},4000);
          }
        });

      }

    });




    $("#addpost").click(function(){

      var postmessage=$("#postmessage").val();
      var postime=$("#timeschedule").val();
      var postinto=new Array();

      $("input[name='postinto[]']").each(function(){

        postinto.push($(this).val());

      });

$("#postmessage").css('border-color','#d5dbdb');
$("#timeschedule").css('border-color','#d5dbdb');

if(postmessage.trim()=="")
   {
    $("#posterr").html("Enter the message !");
    $("#postmessage").css('border-color','#ff0000');
   }
   else if(postinto.length===0)
   {
    $("#posterr").html("Select atleast one social profile !");

   }
   else if(postime.trim()=="")
   {
    $("#posterr").html("Select atleast one social profile !");
    $("#timeschedule").css('border-color','#ff0000');
   }
   else
   {

        $.ajax({
          method: 'post',
          url: '<?php echo base_url(); ?>social/addpost',
          data: new FormData($('#socialPost')[0]),
          dataType:'json',
          contentType: false,
            processData:false,
          success: function(data){

           if(data.response==false)
            {

              notify(data.message,"danger");
            }
            else
            {

              notify(data.message,"success");
            }

            //$("#posterr").html(data.message);

            setTimeout(function(){location.reload();},4000);
          }
        });

    }

    });




        $("#addpostUpdate").click(function(){

      var postmessage=$("#postmessageUpdate").val();
      var postime=$("#timescheduleUpdate").val();
      var postinto=new Array();

      $("input[name='postinto[]']").each(function(){

        postinto.push($(this).val());

      });

$("#postmessageUpdate").css('border-color','#d5dbdb');
$("#timescheduleUpdate").css('border-color','#d5dbdb');

if(postmessage.trim()=="")
   {
    $("#posterrUpdate").html("Enter the message !");
    $("#postmessageUpdate").css('border-color','#ff0000');
   }
   else if(postinto.length===0)
   {
    $("#posterrUpdate").html("Select atleast one social profile !");

   }
   else if(postime.trim()=="")
   {
    $("#posterrUpdate").html("Select atleast one social profile !");
    $("#timescheduleUpdate").css('border-color','#ff0000');
   }
   else
   {

        $.ajax({
          method: 'post',
          url: '<?php echo base_url(); ?>social/updatepost',
          data: new FormData($('#socialPostUpdate')[0]),
          dataType:'json',
          contentType: false,
            processData:false,
          success: function(data){

           if(data.response==false)
            {

              notify(data.message,"danger");
            }
            else
            {

              notify(data.message,"success");
            }

            //$("#posterr").html(data.message);

            

            setTimeout(function(){$('#editModal').modal('hide');location.reload();},2000);
            
          }
        });

    }

    });


});


function deleteEvent(e,eventId) {

            $.ajax({
                  method: 'post',
                   url: '<?php echo base_url() ?>social/deletePost',
                   dataType: 'json',
                   data: {
                   eventid :eventId

                   },

                   success: function(data) {
                     $("#editModal").modal('hide');

                       if(data.response==false)
                        {
                          notify(data.message,"danger");
                        }
                        else
                        {
                          notify(data.message,"success",'body');
                        }

                         $("#calendar").fullCalendar('removeEvents',eventId);

                       //  setTimeout(function(){location.reload();},2000);
                   }
                   });

return false;
}




function notify(message,type,element='.modal')
{

$.notify({
  // options
  icon: 'glyphicon',

  message: message,

},{
  // settings
  element: element,
  position: null,
  type: type,
  allow_dismiss: false,
  newest_on_top: false,
  showProgressbar: false,
  placement: {
    from: "top",
    align: "right"
  },
  offset: 20,
  spacing: 10,
  z_index: 1031,
  delay: 1000,
  timer: 1000,

  mouse_over: null,
  animate: {
    enter: 'animated fadeInDown',
    exit: 'animated fadeOutUp'
  }

});
}

</script>
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/materialFullCalendar.css" />

</html>
