<?php $this->load->view("shared/header.php");?>


    <body>
      <?php $this->load->view("shared/social_nav.php");

?>
  <?php
          include( 'assets/social/social-stream.php' ); // Path to PHP Social Stream main file
          ?>
  <div class="container-fluid">

    <div style="padding-bottom:60px;">


                <div style="float:left;"><h1>Find Content To Share</h1></div>
                <div style="float:right;"><h4><i style=" color:#000;" data-placement="bottom" data-toggle="tooltip" title="Currently you can search for content using instagram, twitter and youtube. Other social networks have not made it possible to search for content. We are working on developing more content sources for you right now. " class="fa fa-question-circle"></i> </h4>
                </div>
            </div><!-- /.col -->


<br>


  <form action="<?php echo base_url() ?>find-content" method="get" class="form-wrapper">

<div class="col-md-4 col-md-offset-4">

    <div style="max-width:400px;" class="input-group">
      <input type="text" name="search" id="search" class="form-control" placeholder="Search for...">
      <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Search!</button>
      </span>

    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
    </form>

  </br>
    <!-- social media search and display -->
    <?php
    //check for search
    $search = (isset($_GET['search']))?htmlspecialchars($_GET["search"]):'';
    // default search options
    $search = $search ?: 'inboundmarketing';
    // Remove spaces
    $search = str_replace(' ', '', $search);
    // adapt for instagram
    $searchhashtag = '#' . $search;

    //echo social stream on screen
    echo social_stream(
        array(
            'id' => '1',
            'type' => 'wall',
            'network' => array(
                'twitter' => array(
                    'twitter_id_3' => array(
                        $searchhashtag // Twitter hashtag 1
                         // Twitter hashtag 2
                    ),
                    'twitter_images' => 'small',
                    'twitter_feeds' => 'retweets,replies'
                ),
                'instagram' => array(
                    'instagram_id_2' => array(
                        $search,
                        $searchhashtag // Instagram hashtag 1
                         // Instagram hashtag 2
                    )
                ),
                'youtube' => array(
                    'youtube_id_3' => array(
                        $search // Instagram hashtag 1
                         // Instagram hashtag 2
                    )
                )
            ),
            'theme' => 'sb-modern-light',
            'itemwidth' => 250,
            'results' => 30,
            'debuglog' => 0,
            'add_files' => true,
            'https' => true,
            'filter_search' => false,
        )
    );

?>


    </div>

   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script>
            // Add tooltip initiate
            $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            });

            </script>

</html>
