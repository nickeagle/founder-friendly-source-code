<?php $this->load->view("shared/header.php");?>
    <body>
      <?php $this->load->view("shared/social_nav.php");

?>

  <div class="container-fluid">






    <div class="row">

        <div class="col-md-9 col-sm-8">
            <h1>   Your Social Schedule</h1>
        </div>
    </div>


    <div class="row">
    <div style="margin-top:30px; margin-bottom:60px;" class="alert alert-primary" role="alert"><p>Start by building a schedule of the times you would like your social media messages to be posted. Then use the Auto Schedule button when creating a message to use your scheduled slots.</p></div>
    </div>

<!-- external events drag and drop -->
    <div id='wrap'>

       <div id='external-events'>
         <h4>Your Social Accounts</h4>

         <?php

            if($all_social_profile!=false)
                    {
                        foreach ($all_social_profile as  $profile) {

                            foreach ($profile['user_profile'] as $key => $user_profile) {

                              switch ($profile['profile_id']) {
                                case '1':
                                  $profile_name=$user_profile['profile']['name'];
                                  break;

                                  case '2':
                                  $profile_name=$user_profile['profile']->name;
                                  break;
                                  case '3':
                                 $profile_name=$user_profile['profile']['name'];
                                  break;

                              }

                                echo '<div style="background-color: transparent; border: none;" class="fc-event" data-id="'.$user_profile['id'].'"><a class="btn btn-block btn-social '.$profile['btn_type'].'" >
            <span class="'.$profile['profile_icon'].'"></span> '.$profile_name.'</a></div>';

                            }

                        }
                    }
                    else
                                  {
                                    echo "<a href='".base_url('social-profiles')."' style='font-size:12px;'>No Linked accounts ,Please connect your social profile</a>";
                                  }
         ?>


<!--
         <div style="background-color: transparent; border: none;" class='fc-event'><a class="btn btn-block btn-social btn-facebook">
            <span class="fa fa-facebook"></span> Add Facebook Slots</a></div>

            <div style="background-color: transparent; border: none;" class='fc-event'><a class="btn btn-block btn-social btn-twitter">
               <span class="fa fa-twitter"></span> Add Twitter Slots</a></div>

               <div style="background-color: transparent; border: none;" class='fc-event'><a class="btn btn-block btn-social btn-instagram">
                  <span class="fa fa-instagram"></span> Add Instagram Slots</a></div>
-->
                  <p> Drag accounts on to the calender to create a posting schedule</p>


                  <!-- Save button should take the current schedule and store in database for the user -->



       </div>


       <!-- social calender full calender -->
    <div id="calendar" style="width: 900px;">
    </div>


    </div>
    </div>
    </div>


<script type="text/javascript">
$(document).ready(function() {

var date_last_clicked = null;

/* initialize the external events
  -----------------------------------------------------------------*/
  $('#external-events .fc-event').each(function() {
    // store data so the calendar knows to render an event upon drop
    $(this).data('event', {
      title: $.trim($(this).text()), // use the element's text as the event title
      stick: true // maintain when user navigates (see docs on the renderEvent method)
    });
    // make the event draggable using jQuery UI
    $(this).draggable({
      zIndex: 999,
      revert: true,      // will cause the event to go back to its
      revertDuration: 0  //  original position after the drag
    });
  });

  $('#calendar').fullCalendar({
    header: {
          left: '',
          center: '',
          right: ''
      },
      views: {
    week: {
    columnFormat:'ddd'
    }
},

  handleWindowResize: true,
  defaultView: 'agendaWeek', // Only show week view
  minTime: '07:30:00', // Start time for the calendar
  maxTime: '22:30:00',
  defaultDate:'2018-02-22',
  editable: false,
  selectable: false,
  allDaySlot: false, // End time for the calendar
  eventStartEditable: true,
  displayEventTime: true, // Display event time
  displayEventEnd: false,
  droppable: true, // this allows things to be dropped onto the calendar
       eventSources: [
           {
               events: function(start, end, timezone, callback) {
                   $.ajax({
                   url: '<?php echo base_url() ?>social/getSchedule',
                   dataType: 'json',
                   data: {
                   // our hypothetical feed requires UNIX timestamps
                   start: start.unix(),
                   end: end.unix()

                   },
                   success: function(data) {

                       callback(data.schedule);
                   }
                   });
               }
           },
       ],
      drop: function (date, jsEvent, ui, resourceId) {

      console.log(ui);
      var title = $(this).data('event').title;
      var profile_id=$(this).attr("data-id");
      console.log(profile_id);
      var date=date.format();
      addSchedule(title,date,profile_id);

  },

    eventClick: function(event, jsEvent, view) {

},
eventRender: function(event, element) {
     if(event.icon){
        element.find(".fc-title").prepend("<i class='"+event.icon+" font'></i>  ");
     }
     if (event.imageurl) {
        element.find("div.fc-content").prepend("<img src='" + event.imageurl +"' width='25' height='25'>  ");
    }
    if (event.status==2) {
          element.css("opacity","0.6");
        }
  },
eventMouseover: function(calEvent, domEvent) {
	var layer =	"<div id='events-layer' class='fc-transparent' style='position:absolute; width:100%; height:100%; top:-1px; text-align:right; z-index:100'> <a> <span class='fa fa-trash-o' style='margin-right:10px;'' onClick='deleteEvent("+calEvent.id+");'></span></a></div>";
	$(this).append(layer);
},
eventMouseout: function(calEvent, domEvent) {
	$("#events-layer").remove();
},


eventReceive: function(event) { // called when a proper external event is dropped
        //console.log('eventReceive', event);

      },
      eventDrop: function(event,dayDelta) { // called when an event (already on the calendar) is moved
        console.log(event);
        console.log(dayDelta);
        updateSchedule(event.id,event.title,dayDelta._milliseconds,dayDelta._days);
      }

   });


});




function deleteEvent(eventId) {

            $.ajax({
                  method: 'post',
                   url: '<?php echo base_url() ?>social/deleteSchedule',
                   dataType: 'json',
                   data: {
                   eventid :eventId

                   },
                   success: function(data) {

                       if(data.response==false)
                        {

                          notify(data.message,"danger");
                        }
                        else
                        {

                          notify(data.message,"success");
                        }

                         setTimeout(function(){location.reload();},2000);
                   }
                   });

}




function addevent() {
$(this).css('background-color', '#bed7f3');
$('#addModal').modal();
}


function addSchedule(title,date,profile_id)
{

  $.ajax({
          method: 'post',
           url: '<?php echo base_url() ?>social/addSchedule',
           dataType: 'json',
           data: {
           start: date,
           title:title,
           profile_id:profile_id
           },
           success: function(data) {
               console.log(data);
           }
         });
}


function updateSchedule(id,title,changemilisec,changeday)
{
   $.ajax({
          method: 'post',
           url: '<?php echo base_url() ?>social/updateSchedule',
           dataType: 'json',
           data: {
           changemilisec: changemilisec,
           changeday:changeday,
           title:title,
           id: id
           },
           success: function(data) {
               console.log(data);
           }
         });


}

function notify(message,type)
{

$.notify({
  // options
  icon: 'glyphicon',

  message: message,

},{
  // settings
  element: 'body',
  position: null,
  type: type,
  allow_dismiss: false,
  newest_on_top: false,
  showProgressbar: false,
  placement: {
    from: "top",
    align: "right"
  },
  offset: 20,
  spacing: 10,
  z_index: 1031,
  delay: 4000,
  timer: 1000,

  mouse_over: null,
  animate: {
    enter: 'animated fadeInDown',
    exit: 'animated fadeOutUp'
  }

});
}

</script>
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/materialFullCalendar.css" />

</html>
