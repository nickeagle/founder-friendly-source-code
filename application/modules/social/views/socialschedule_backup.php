<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Social Media Calender</title>
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link href="<?php echo base_url() ?>/build/sites.css" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon.png">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/css/flat-ui-pro-custom.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- local js files and css -->
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/calender/fullcalendar.min.css" />

               <script src="<?php echo base_url() ?>/assets/calender/lib/moment.min.js"></script>
               <script src="<?php echo base_url() ?>/assets/calender/fullcalendar.min.js"></script>
               <script src="<?php echo base_url() ?>/assets/calender/gcal.js"></script>
               <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
               <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
               <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
                <link rel="stylesheet" href="<?php echo base_url() ?>assets/social/bootstrap-social.css" />


        <style type="text/css">
          #drop-zone {
              width: 573px;
              height: 164px;
              position: absolute;
              left: 28%;
              top: 5px;
              margin-left: -161px;
              border: 2px dashed rgba(0,0,0,.3);
              border-radius: 20px;
              font-family: Arial;
              text-align: center;
              position: relative;
              line-height: 114px;
              font-size: 21px;
              color: rgba(0,0,0,.3);
          }


            #drop-zone input {
                position: absolute;
                cursor: pointer;
                left: 0px;
                top: 0px;
                /*Important This is only comment out for demonstration purposes.
                opacity:0; */
            }

            /*Important*/
            #drop-zone.mouse-over {
                border: 2px dashed rgba(0,0,0,.5);
                color: rgba(0,0,0,.5);
            }


        /*If you dont want the button*/
        #clickHere {
            position: absolute;
            cursor: pointer;
            left: 50%;
            top: 50%;
            margin-left: -50px;
            margin-top: 20px;
            line-height: 26px;
            color: white;
            font-size: 12px;
            width: 100px;
            height: 26px;
            border-radius: 4px;
            background-color: #3b85c3;

        }

            #clickHere:hover {
                background-color: #4499DD;

            }
            #external-events {
    float: left;
    width: 250px;
    padding: 0 10px;
    border: 1px solid #ddd;
    border-radius: 3px;
    text-align: left;
  }

  #external-events h4 {
    font-size: 16px;
    margin-top: 0;
    padding-top: 1em;
  }

  #external-events .fc-event {
    margin: 10px 0;
    cursor: pointer;
  }

  #external-events p {
    margin: 1.5em 0;
    font-size: 11px;
    color: #666;
  }

  #external-events p input {
    margin: 0;
    vertical-align: middle;
  }
  #calendar {
    float: right;
    width: 900px;
    margin-bottom: 30px;
  }
  .fc {
    padding-top: 0 !important;
}
  .fc-toolbar.fc-header-toolbar {
    padding-bottom: 30px;
    display: none;
}
        </style>
    </head>
    <body>
      <?php $this->load->view("shared/social_nav.php");

?>

  <div class="container-fluid">

    <!-- edit modal pop up -->

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Set Up Schedule</h4>
      </div>
      <div class="modal-body">
      <?php echo form_open(site_url("social/edit_event"), array("class" => "form-horizontal")) ?>
      <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Event Name</label>
                <div class="col-md-8 ui-front">
                    <input type="text" class="form-control" name="name" value="" id="name">
                </div>
        </div>
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Description</label>
                <div class="col-md-8 ui-front">
                    <input type="text" class="form-control" name="description" id="description">
                </div>
        </div>
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">Start Date</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="start_date" id="start_date">
                </div>
        </div>
        <div class="form-group">
                <label for="p-in" class="col-md-4 label-heading">End Date</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="end_date" id="end_date">
                </div>
        </div>
        <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading">Delete Event</label>
                    <div class="col-md-8">
                        <input type="checkbox" name="delete" value="1">
                    </div>
            </div>
            <input type="hidden" name="eventid" id="event_id" value="0" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Update Event">
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>


<!-- model pop up for adding an event -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Post</h4>
      </div>
      <div class="modal-body">
      <?php echo form_open_multipart(site_url("social/addSocialPost"), array("class" => "form-horizontal")) ?>


      <div class="row">

        <div class="col-lg-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="social_message">Message</label>
            <textarea class="form-control" name="social_message" id="social_message"></textarea>
          </div>
        </div>

        <div class="col-lg-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="social_link">Link</label>
            <input type="text" class="form-control" name="social_link" id="social_link">
          </div>
        </div>

        <div class="col-lg-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="social_link">Post Date</label>
            <input type="text" class="form-control datepicker" name="social_date" id="social_date" data-date-format="yyyy/mm/dd" >
          </div>
        </div>

        <div class="col-lg-12 col-sm-12 col-xs-12">
          <div class="form-group">
            <label for="social_message">Image</label>
            <div id="drop-zone" ondragover="return false">
                Drop file here...
                <div id="clickHere">
                    or Click here..

                </div>
                <input type="file"  name="social_image" id="social_image" style="display: none;">
            </div>

          </div>
        </div>


      </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Add Post" onclick="return validateSocial();">
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>

<!-- end model popup -->
    <div class="row">
    <div class="col-md-12">


<!-- end popup -->


<div class="row">

        <div class="col-md-9 col-sm-8">

            <h1>   Your Social Schedule</h1>

        </div><!-- /.col -->


    </div>
    <div style="margin-top:30px; margin-bottom:60px;" class="alert alert-info" role="alert"><p>Start by building a schedule of the times you would like your social media messages to be posted. Then use the queue post button when creating a message to use your scheduled slots.</p></div>

<!-- external events drag and drop -->
    <div id='wrap'>

       <div id='external-events'>
         <h4>Your Social Accounts</h4>
         <div style="background-color: transparent; border: none;" class='fc-event'><a class="btn btn-block btn-social btn-facebook">
            <span class="fa fa-facebook"></span> Add Facebook Slots</a></div>

            <div style="background-color: transparent; border: none;" class='fc-event'><a class="btn btn-block btn-social btn-twitter">
               <span class="fa fa-twitter"></span> Add Twitter Slots</a></div>

               <div style="background-color: transparent; border: none;" class='fc-event'><a class="btn btn-block btn-social btn-instagram">
                  <span class="fa fa-instagram"></span> Add Instagram Slots</a></div>

                  <p> Drag accounts on to calender to create a posting schedule</p>


                  <!-- Save button should take the current schedule and store in database for the user -->

                <button class="btn btn-primary" name="save"> Save Schedule </button>
                <br></br>
       </div>


       <!-- social calender full calender -->
    <div id="calendar">
    </div>


    </div>
    </div>
    </div>


<script type="text/javascript">
$(document).ready(function() {
  $('.datepicker').datepicker();
});
</script>
<script type="text/javascript">
$(document).ready(function() {

var date_last_clicked = null;

/* initialize the external events
  -----------------------------------------------------------------*/
  $('#external-events .fc-event').each(function() {
    // store data so the calendar knows to render an event upon drop
    $(this).data('event', {
      title: $.trim($(this).text()), // use the element's text as the event title
      stick: true // maintain when user navigates (see docs on the renderEvent method)
    });
    // make the event draggable using jQuery UI
    $(this).draggable({
      zIndex: 999,
      revert: true,      // will cause the event to go back to its
      revertDuration: 0  //  original position after the drag
    });
  });

  $('#calendar').fullCalendar({
    header: {
          left: '',
          center: '',
          right: ''
      },
      views: {
    week: {
    columnFormat:'ddd'
    }
},
  handleWindowResize: true,
  defaultView: 'agendaWeek', // Only show week view
  minTime: '07:30:00', // Start time for the calendar
  maxTime: '22:00:00',
  allDaySlot: false, // End time for the calendar
  eventStartEditable: true,
  displayEventTime: true, // Display event time
  displayEventEnd: false,
  droppable: true, // this allows things to be dropped onto the calendar
       eventSources: [
           {
               events: function(start, end, timezone, callback) {
                   $.ajax({
                   url: '<?php echo base_url() ?>social/get_events',
                   dataType: 'json',
                   data: {
                   // our hypothetical feed requires UNIX timestamps
                   start: start.unix(),
                   end: end.unix()

                   },
                   success: function(msg) {
                       var events = msg.events;
                       callback(events);
                   }
                   });
               }
           },
       ],
    dayClick: function(date, jsEvent, view) {
        date_last_clicked = $(this);
        $(this).css('background-color', '#bed7f3');
        $('#addModal').modal();
    },
    eventClick: function(event, jsEvent, view) {
          $('#name').val(event.title);
          $('#description').val(event.description);
          $('#start_date').val(moment(event.start).format('YYYY/MM/DD HH:mm'));
          if(event.end) {
            $('#end_date').val(moment(event.end).format('YYYY/MM/DD HH:mm'));
          } else {
            $('#end_date').val(moment(event.start).format('YYYY/MM/DD HH:mm'));
          }
          $('#event_id').val(event.id);
          $('#editModal').modal();
},
eventReceive: function(event) { // called when a proper external event is dropped
        console.log('eventReceive', event);
      },
      eventDrop: function(event) { // called when an event (already on the calendar) is moved
        console.log('eventDrop', event);
      }

   });


});
          function addevent() {
          $(this).css('background-color', '#bed7f3');
          $('#addModal').modal();
        }


$(document).ready(function() {
  console.log(5);
    var dropZoneId = "drop-zone";
    var buttonId = "clickHere";
    var mouseOverClass = "mouse-over";

    var dropZone = $("#" + dropZoneId);
    var ooleft = dropZone.offset().left;
    var ooright = dropZone.outerWidth() + ooleft;
    var ootop = dropZone.offset().top;
    var oobottom = dropZone.outerHeight() + ootop;
    var inputFile = dropZone.find("input");
    document.getElementById(dropZoneId).addEventListener("dragover", function (e) {
      console.log(4);
        e.preventDefault();
        e.stopPropagation();
        dropZone.addClass(mouseOverClass);


    }, true);

jQuery.event.props.push('dataTransfer');
    $(dropZone).bind('drop', function(e) {
      var files = e.dataTransfer.files;
      console.log(files);
    });


    if (buttonId != "") {
        var clickZone = $("#" + buttonId);

        var oleft = clickZone.offset().left;
        var oright = clickZone.outerWidth() + oleft;
        var otop = clickZone.offset().top;
        var obottom = clickZone.outerHeight() + otop;

        $("#" + buttonId).mousemove(function (e) {
            var x = e.pageX;
            var y = e.pageY;
            if (!(x < oleft || x > oright || y < otop || y > obottom)) {
                inputFile.offset({ top: y - 15, left: x - 160 });
            } else {
                inputFile.offset({ top: -400, left: -400 });
            }
        });
    }

    document.getElementById(dropZoneId).addEventListener("drop", function (e) {
        $("#" + dropZoneId).removeClass(mouseOverClass);
    }, true);

});


$(document).ready(function(){
    $("#clickHere").click(function(e){

      $('#social_image').click();


    });

});


function validateSocial()
{
    var file =  $(":file").files[0];
    var fileType = file["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
    if ($.inArray(fileType, ValidImageTypes) < 0) {
         return false;
    }
}


$(document).ready(function(){
    $(":file").on("change", function(e) {

      var file = this.files[0];
      var fileType = file["type"];
      var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
      if ($.inArray(fileType, ValidImageTypes) < 0) {
          alert("Upload file is not a image");
      }

  });
});
</script>
<link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/materialFullCalendar.css" />

</html>
