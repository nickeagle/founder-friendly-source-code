<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Social extends MY_Controller {
    /**
      * Class constructor
      *
      * @return void
      */





    public function __construct()
    {
        parent::__construct();
        if ( ! $this->session->has_userdata('user_id') && $this->uri->segment(1) != 'loadsinglepage' && $this->uri->segment(1) != 'loadsingleframe')
        {
            redirect('auth', 'refresh');
        }

        $this->load->module('package');

        if($this->package->check_subscription()==FALSE)
        {
            redirect($this->config->item('mail_url').'account/subscription');
        }

        $this->load->model("social/Social_model","Msocial");
        $this->load->model("user/Users_model","Musers");
    }
    /**
      * Index
      *
      * @return void
      */
    public function index()
    {
      $data['title'] = $this->lang->line('Social_index_title');
       $data['page'] = "social";
       $data['all_social_profile']=$this->getallSocialProfile();
        $this->load->view('social',$data);

    }



     public function findContent()
     {
       $data['title'] = $this->lang->line('Social_connect');
       $data['page'] = "socialFindcontent";

      $this->load->view("findcontent",$data);
     }

     public function socialProfiles()
     {
      $data['title'] = $this->lang->line('Social_connect');
      $data['page'] = "socialProfile";
      $data['facebook_profile']=$this->get_all_facebook_profile();
      $data['instragram_profile']=$this->get_all_instragram_profile();
      $data['twitter_profile']=$this->get_all_twitter_profile();
      $data['social_profile']=$this->Msocial->getSocialProfile();
      $data['all_social_profile']=$this->getallSocialProfile();

      $this->load->view("social/socialprofiles",$data);
     }

     public function socialQueue()
     {
      $data['title'] = $this->lang->line('Social_connect');

      $data['page'] = "socialQueue";
      $data["all_post"]=$this->Msocial->get_post('');
       $data['all_social_profile']=$this->getallSocialProfile();

      $this->load->view("social/socialqueue1",$data);
     }

     public function socialSchedule()
     {
      $data['title'] = $this->lang->line('Social_index_title');
      $data['page'] = "socialSchedule";
      $data['all_social_profile']=$this->getallSocialProfile();

      $this->load->view("social/socialschedule",$data);
     }


     public function addSocialPost()
     {
       $postdata=array();
       if(is_uploaded_file($_FILES['social_image']['tmp_name'])) {
          $imageFileType = strtolower(pathinfo($target_dir . basename($_FILES["social_image"]["name"]),PATHINFO_EXTENSION));
          if($imageFileType=="jpg" || $imageFileType=="jpeg" || $imageFileType=="png")
          {


        $postdata["message"]=$this->input->post("social_message" , true);
        $postdata["link"]=$this->input->post("social_link" , true);
        $postdata["post_date"]=$this->input->post("social_date" , true);
        $postdata["imagetype"]=$imageFileType;


        $post_id=$this->Msocial->insert_social($postdata);

         $target_dir = APPPATH."../social_image/";

                  if(!is_dir($target_dir))
                    {
                     mkdir($target_dir, 0777, true);
                     chmod($target_dir, 0777);
                    }


                    move_uploaded_file($_FILES["social_image"]["tmp_name"],$target_dir.$post_id.".".$imageFileType);


        }
      }

        redirect('social');

     }


     public function get_profile_by_name($profile_name)
     {
      $profile=$this->Msocial->get_profile_by_name($profile_name);
      return $profile;
     }


     private function _facebooksdk()
     {
      require_once 'vendor/autoload.php';

       $fb = new Facebook\Facebook([
      'app_id' => $this->config->item('facebook_app_id'),
      'app_secret' => $this->config->item('facebook_app_secret'),
      'default_graph_version' => 'v2.10',
      ]);
       return $fb;
     }


     public function facebookAuth()
     {
      $fb=$this->_facebooksdk();
      $helper = $fb->getRedirectLoginHelper();
      $permissions = ['email', 'user_likes','publish_actions', 'manage_pages']; // optional
      $loginUrl = $helper->getLoginUrl(base_url().'social/validateAuthFacebook', $permissions);

      echo "<script>window.location='".$loginUrl."';</script>";
     }


     public function validateAuthFacebook()
     {
      $fb=$this->_facebooksdk();

      $helper = $fb->getRedirectLoginHelper();
      $_SESSION['FBRLH_state']=$_GET['state'];
      try {
        $accessToken = $helper->getAccessToken();
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }

      if (isset($accessToken)) {
        // Logged in!
        $facebook_access_token= (string) $accessToken;

        $profile=$this->get_profile_by_name('facebook');

        $user_data=$this->getfacebookProfile($facebook_access_token);



        $checkauthtoken=$this->Msocial->auth_user_id($user_data['id'],$profile[0]->id,$this->session->userdata("user_id"));

        if($checkauthtoken==FALSE)
        {
        $this->Msocial->update_authorization_token(array("auth_token"=>$facebook_access_token,"auth_user_id"=>$user_data['id'],"profile_id"=>$profile[0]->id,"user_id"=>$this->session->userdata("user_id")));
        }




        $this->popup('');
        // Now you can redirect to another page and use the
        // access token from $_SESSION['facebook_access_token']
      }
     }


     public function getfacebookProfile($auth_token)
     {
      $fb=$this->_facebooksdk();
      $fb->setDefaultAccessToken($auth_token);

        try {
          $response = $fb->get('/me');
          $userNode = $response->getGraphUser();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          //echo 'Graph returned an error: ' . $e->getMessage();
          //exit;
          return FALSE;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          //echo 'Facebook SDK returned an error: ' . $e->getMessage();
          //exit;
          return FALSE;
        }

        return $userNode;
     }

     public function get_all_facebook_profile()
     {
      $profile=$this->get_profile_by_name('facebook');

      $user_profile=$this->Msocial->get_user_profile($this->session->userdata('user_id'),$profile[0]->id);

      if($user_profile!=FALSE)
      {
        $data=array();


        foreach ($user_profile as $key => $value) {

            $data[]=$this->getfacebookProfile($value->auth_token);

        }

        return $data;

      }
      else
        return FALSE;

     }

     public function _checkFacebookAuth()
     {
      $checkAuth=$this->Musers->get_value("facebook_auth_token");
      if($checkAuth!=FALSE){$_SESSION['facebook_access_token']=$checkAuth[0]->facebook_auth_token; return true;}
      else {return FALSE;}
     }


     public function popup($name)
     {
      $data['name']=$name;
      $this->load->view('social/popup',$data);
     }


     public function instragramAuth()
     {
      $fb=$this->_facebooksdk();
      $helper = $fb->getRedirectLoginHelper();
      $permissions = ['email', 'instagram_basic']; // optional
      $loginUrl = $helper->getLoginUrl(base_url().'social/validateAuthInstragram', $permissions);

      echo "<script>window.location='".$loginUrl."';</script>";
     }


     public function validateAuthInstragram()
     {
      $fb=$this->_facebooksdk();

      $helper = $fb->getRedirectLoginHelper();
      $_SESSION['FBRLH_state']=$_GET['state'];
      try {
        $accessToken = $helper->getAccessToken();
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }

      if (isset($accessToken)) {
        // Logged in!
        $instragram_access_token = (string) $accessToken;

        $profile=$this->get_profile_by_name('instragram');

         $user_data=$this->getfacebookProfile($instragram_access_token);



        $checkauthtoken=$this->Msocial->auth_user_id($user_data['id'],$profile[0]->id,$this->session->userdata("user_id"));

        if($checkauthtoken==FALSE)
        {
        $this->Msocial->update_authorization_token(array("auth_token"=>$instragram_access_token,"auth_user_id"=>$user_data['id'],"profile_id"=>$profile[0]->id,"user_id"=>$this->session->userdata("user_id")));
        }

        $this->popup('');
        // Now you can redirect to another page and use the
        // access token from $_SESSION['facebook_access_token']
      }
     }


     public function getInstragramProfile($auth_token)
     {


      $fb=$this->_facebooksdk();
      $fb->setDefaultAccessToken($auth_token);

        try {
          $response = $fb->get('/me');
          $userNode = $response->getGraphUser();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          //echo 'Graph returned an error: ' . $e->getMessage();
          //exit;
          return FALSE;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          //echo 'Facebook SDK returned an error: ' . $e->getMessage();
          //exit;
          return FALSE;
        }

        return $userNode;
     }



     public function get_all_instragram_profile()
     {
      $profile=$this->get_profile_by_name('instragram');

      $user_profile=$this->Msocial->get_user_profile($this->session->userdata('user_id'),$profile[0]->id);

      if($user_profile!=FALSE)
      {
        $data=array();


        foreach ($user_profile as $key => $value) {

            $data[]=$this->getInstragramProfile($value->auth_token);

        }

        return $data;

      }
      else
        return FALSE;

     }


     public function _checkInstragramAuth()
     {
      $checkAuth=$this->Musers->get_value("instragram_auth_token");
      if($checkAuth!=FALSE){$_SESSION['instragram_access_token']=$checkAuth[0]->instragram_auth_token; return true;}
      else {return FALSE;}
     }

     public function _twittersdk()
     {
      require_once ('phpsdk/twitter/src/codebird.php');
      \Codebird\Codebird::setConsumerKey($this->config->item('twitter_app_id'),$this->config->item('twitter_app_secret')); // static, see README

        $cb = \Codebird\Codebird::getInstance();

        return $cb;
     }


     public function twitterAuth()
     {
      unset($_SESSION['oauth_token']);
      redirect('social/twitterAuthToken');
     }



     public function twitterAuthToken()
     {
      $cb=$this->_twittersdk();
        if (! isset($_SESSION['oauth_token'])) {
          // get the request token
          $reply = $cb->oauth_requestToken([
            'oauth_callback' => 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
          ]);


          // store the token
          $cb->setToken($reply->oauth_token, $reply->oauth_token_secret);
          $_SESSION['oauth_token'] = $reply->oauth_token;
          $_SESSION['oauth_token_secret'] = $reply->oauth_token_secret;
          $_SESSION['oauth_verify'] = true;

          // redirect to auth website
          $auth_url = $cb->oauth_authorize();
          header('Location: ' . $auth_url);
          die();

        } elseif (isset($_GET['oauth_verifier']) && isset($_SESSION['oauth_verify'])) {
          // verify the token
          $cb->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
          unset($_SESSION['oauth_verify']);

          // get the access token
          $reply = $cb->oauth_accessToken([
            'oauth_verifier' => $_GET['oauth_verifier']
          ]);

          // store the token (which is different from the request token!)
          $_SESSION['oauth_token'] = $reply->oauth_token;
          $_SESSION['oauth_token_secret'] = $reply->oauth_token_secret;

          // send to same URL, without oauth GET parameters


        }



        $profile=$this->get_profile_by_name('twitter');

        $checkauthtoken=$this->Msocial->checkauthtoken($_SESSION['oauth_token'],$profile[0]->id,$this->session->userdata("user_id"));

        if($checkauthtoken==FALSE)
        {
          $user=$this->getTwitterProfile($_SESSION['oauth_token'],$_SESSION['oauth_token_secret']);
        $this->Msocial->update_authorization_token(array("auth_token"=>$_SESSION['oauth_token'],"auth_token_secrect"=>$_SESSION['oauth_token_secret'],"auth_user_id"=>$user->screen_name,"profile_id"=>$profile[0]->id,"user_id"=>$this->session->userdata("user_id")));
        }

        unset($_SESSION['oauth_token']);
        unset($_SESSION['auth_token_secrect']);


        $this->popup('');

     }

     public function getTwitterProfile($auth_token,$auth_token_secrect)
     {

      $cb=$this->_twittersdk();
      $_SESSION['oauth_token']=$auth_token;
      $_SESSION['oauth_token_secret']=$auth_token_secrect;

      $cb->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
      $reply = $cb->account_verifyCredentials();


      if(isset($reply->error) || $reply->httpstatus==401)
      {
        return FALSE;
      }
      else
      {
        return $reply;
      }


     }


     public function get_all_twitter_profile()
     {
      $profile=$this->get_profile_by_name('twitter');

      $user_profile=$this->Msocial->get_user_profile($this->session->userdata('user_id'),$profile[0]->id);

      if($user_profile!=FALSE)
      {
        $data=array();


        foreach ($user_profile as $key => $value) {

            $data[]=$this->getTwitterProfile($value->auth_token,$value->auth_token_secrect);

        }

     //print_r($data);

        return $data;

      }
      else
        return FALSE;

     }



     public function getallSocialProfile()
     {

        $profile=$this->Msocial->getallSocialProfile();

        if($profile!=FALSE)
        {
          $data=array();
            foreach ($profile as $key => $value) {

              $profile_data=$this->Msocial->get_user_profile($this->session->userdata('user_id'),$value->id);

                    if($profile_data!=FALSE)
                    {

                      $profile_d=array();
                      $profile_d['profile_id']=$value->id;
                      $profile_d['profile_name']=$value->profile_name;
                      $profile_d['profile_icon']=$value->profile_icon;
                      $profile_d['btn_type']=$value->btn_type;
                      $profile_d['profile_default_color']=$value->profile_default_color;

                      $prof_dd=array();
                      foreach ($profile_data as $key1 => $value1) {
                        $prof_d=array();
                        $prof_d["id"]=$value1->id;
                        //$prof_d["auth_token"]=$value1->auth_token;

                        switch ($value->id) {
                            case "1":
                                $prof_d['profile']=$this->getFacebookProfile($value1->auth_token);
                                break;
                            case "2":
                                $prof_d['profile']=$this->getTwitterProfile($value1->auth_token,$value1->auth_token_secrect);
                                break;
                            case "3":
                                $prof_d['profile']=$this->getInstragramProfile($value1->auth_token);
                                break;
                        }

                        $prof_dd[]=$prof_d;

                      }
                      $profile_d['user_profile']=$prof_dd;
                      $data[]=$profile_d;

                    }

              }

              //echo "<pre>";
              //print_r($data);
         return $data;
        }
        else
        {
          return FALSE;
        }

     }



     public function _checkTwitterAuth($auth_token,$auth_token_secrect)
     {


      if($checkAuth!=FALSE && trim($checkAuth[0]->twitter_auth_token)!=""){

      $_SESSION['oauth_token']=$checkAuth[0]->twitter_auth_token;$_SESSION['oauth_toen_secret']=$checkAuth_secrect[0]->twitter_oauth_token_secret; return true;

      }
      else
        {
          if(isset($_SESSION['oauth_token']) || isset($_SESSION['oauth_toen_secret']))
          {
            unset($_SESSION['oauth_token']);
            unset($_SESSION['oauth_toen_secret']);
          }
          return FALSE;
        }
     }




     public function getSchedule()
     {
        $post=$this->Msocial->getSchedule();

         $data=array();




      if($post!=FALSE)
      {
        $data['response']=true;
        foreach ($post as $key => $value) {
                                              switch ($value->profile_name) {
                                              case 'facebook':
                                              $imgUrl="https://graph.facebook.com/".$value->auth_user_id."/picture?type=square";
                                              break;

                                              case 'twitter':
                                              $imgUrl="https://twitter.com/".$value->auth_user_id."/profile_image?size=normal";
                                              break;
                                              case 'instragram':
                                              $imgUrl="https://graph.facebook.com/".$value->auth_user_id."/picture?type=square";
                                              break;
                                            }
          $data['schedule'][]=array("id"=>$value->id,"start"=>$value->start,"title"=>$value->title,"color"=>$value->profile_default_color,"icon"=>$value->profile_icon,"imageurl"=>$imgUrl,"status"=>$value->status,"social_id"=>$value->user_profile_id);
        }

      }
      else
        {
          $data['response']=FALSE;
        }

        echo json_encode($data);

     }

     public function addSchedule()
     {
      $data['start']=$this->input->post("start");
      $data['title']= $this->input->post("title");
      $data['user_profile_id']=$this->input->post("profile_id");

      $date=date_create($data['start']);
      $data['start']= date_format($date,"Y-m-d H:i:s");
      $data['time']= date_format($date,"H:i:s");
      $data['weekday']= date("w",strtotime($data['start']));
      $data['user_id']=$this->session->userdata('user_id');

      $this->Msocial->addSchedule($data);
     }

     public function updateSchedule()
     {
      $time=$this->input->post("changemilisec");
      $day=$this->input->post("changeday");
      $data['title']= $this->input->post("title");
      $id= $this->input->post("id");

      $schedule=$this->Msocial->getSchedule_by_id($id);


      if($schedule!=false)
      {
       $start=(strtotime($schedule[0]->start.$day.' day')*1000)+$time;

        $data['start']=date("Y-m-d H:i:s",($start/1000));
        $data['time']=date("H:i:s",strtotime($data['start']));
        $data['weekday']= date("w",strtotime($data['start']));
        $this->Msocial->updateSchedule($data,$id);
      }
      else
      {
        echo "Some error";
      }


     }

     public function deleteSchedule()
     {
      $data = array();
      $schedule_id=$this->input->post('eventid');
       $schedule=$this->Msocial->getSchedule_by_id($schedule_id);

       if($schedule[0]->user_id==$this->session->userdata('user_id'))
       {
        $this->Msocial->updateSchedule(array('status'=>0),$schedule_id);
        $data['response']=true;
        $data['message']='Schedule deleted';
       }

       echo json_encode($data);
     }




     public function deletePost()
     {
      $data = array();
      $post_id=$this->input->post('eventid');
       $post=$this->Msocial->getSocial_by_id($post_id);

       if($post[0]->user_id==$this->session->userdata('user_id'))
       {
        $this->Msocial->update_social(array('status'=>0),$post_id);
        $data['response']=true;
        $data['message']='Post deleted';
       }

       echo json_encode($data);
     }



     public function updatePostByTime()
     {

      $time=$this->input->post("changemilisec");
      $day=$this->input->post("changeday");
      $data['message']= $this->input->post("title");
     $id= $this->input->post("id");

      $post=$this->Msocial->getSocial_by_id($id);


      if($post!=false)
      {
       $start=(strtotime($post[0]->postdate.$day.' day')*1000)+$time;

        $data['postdate']=date("Y-m-d H:i:s",($start/1000));
       // $data['time']=date("H:i:s",strtotime($data['start']));
        //$data['weekday']= date("w",strtotime($data['start']));
        $this->Msocial->update_social($data,$id);
        echo "success";
      }
      else
      {
        echo "Some error";
      }


     }



     public function postnow()
     {
        $data=array();
        //print_r($_POST);

        $postmessage=$this->input->post("postmessage");
        $user_profile_id=$this->input->post("postinto");

        $i=0;
        foreach ($user_profile_id as $value) {
          $insert_post['user_social_id']=$value;
          $insert_post['message']=$postmessage;
          $insert_post['postdate']=date('Y-m-d H:i:s');
          $insert_post['user_id']=$this->session->userdata('user_id');
          $insert_post['status']=2;
          $ipost=$this->Msocial->insert_social($insert_post);
          $post=$this->Msocial->get_user_profil_by_id($value);
           $postinto= $post[0]->profile_name;
           $file_upload="";
           if(is_array($_FILES) && $i==0) {
            if(is_uploaded_file($_FILES['uploadimage']['tmp_name'])) {

                 $target_dir = APPPATH."../social_image/";
                  $imageFileType = strtolower(pathinfo($target_dir . basename($_FILES["uploadimage"]["name"]),PATHINFO_EXTENSION));

                  if($imageFileType=="png" || $imageFileType=="jpg" || $imageFileType=="jpeg" || $imageFileType=="gif")
                  {
                    if(!is_dir($target_dir))
                    {
                     mkdir($target_dir, 0777, true);
                     chmod($target_dir, 0777);
                    }


                    move_uploaded_file($_FILES["uploadimage"]["tmp_name"],$target_dir.$ipost.".".$imageFileType);
                    $upost=$this->Msocial->update_social(array("imagetype"=>$imageFileType),$ipost);

                    $file_upload=file_get_contents(base_url().'social_image/'.$ipost.".".$imageFileType);

                    $i++;
                  }

              }
            }
            else
            {
              file_put_contents($target_dir.$ipost.".".$imageFileType, $file_upload);
              $upost=$this->Msocial->update_social(array("imagetype"=>$imageFileType),$ipost);
            }




          if($postinto=='facebook')
          {
            $afterpost=$this->postFacebook($postmessage);
             $data['response'][]=$afterpost['response'];
            $data['message'][]=$afterpost['message'];
          }
          elseif($postinto=='twitter')
          {

            $afterpost=$this->tweetonTwitter($postmessage,$file_upload,$post[0]->auth_token,$post[0]->auth_token_secrect);


            $data['response'][]=$afterpost['response'];
            $data['message'][]=$afterpost['message'];
          }
          elseif($postinto=='instragram')
          {
            $afterpost=$this->postInstragram($postmessage);
             $data['response'][]=$afterpost['response'];
            $data['message'][]=$afterpost['message'];
          }
          else
          {
            $data['response']=FALSE;
            $data['message']="Select where to post";
          }
        }



          echo json_encode($data);
     }


     public function postFacebook($postmessage)
     {
          $data['response']=False;
          $data['message']='Message not posted in facebook';

        return $data;
     }


      public function postInstragram($postmessage)
     {
          $data['response']=False;
          $data['message']='Message not posted in instragram';

        return $data;
     }




     public function tweetonTwitter($postmessage,$file,$twitter_auth_token,$twitter_oauth_token_secret)
     {
      $data=array();

      $_SESSION['oauth_token']=$twitter_auth_token;
      $_SESSION['oauth_token_secret']=$twitter_oauth_token_secret;



        $cb=$this->_twittersdk();
        $cb->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

        if($file!="")
        {
          $reply_file = $cb->media_upload([
            'media' => $file
          ]);
          echo "<pre>";
          print_r($reply_file);

          $media_ids = $reply_file->media_id_string;

          $reply = $cb->statuses_update([
          'status=' . urlencode($postmessage),
          'media_ids' => (string)$media_ids
        ]);

          print_r($reply);
        }
        else
        $reply = $cb->statuses_update('status=' . urlencode($postmessage));




        if($reply->httpstatus==200)
        {
          $data['response']=TRUE;
          $data['message']='Message twetted sucessfully';
        }
        else
        {
          $data['response']=False;
          $data['message']='Message not twitted';
        }

      unset($_SESSION['oauth_token']);
      unset($_SESSION['oauth_token_secret']);

      return $data;
     }



     public function addpost()
     {
       $data=array();

        $postmessage=$this->input->post("postmessage");
        $postime=$this->input->post("timeschedule");
        $user_profile_id=$this->input->post("postinto");

        $postime=date("Y-m-d H:i:s",strtotime($postime));
        $i=0;
        foreach ($user_profile_id as $value) {
          $insert_post['user_social_id']=$value;
          $insert_post['message']=$postmessage;
          $insert_post['postdate']=$postime;
          $insert_post['user_id']=$this->session->userdata('user_id');

          $ipost=$this->Msocial->insert_social($insert_post);

          if(is_array($_FILES) && $i==0) {
            if(is_uploaded_file($_FILES['uploadimage']['tmp_name'])) {

                 $target_dir = APPPATH."../social_image/";
                  $imageFileType = strtolower(pathinfo($target_dir . basename($_FILES["uploadimage"]["name"]),PATHINFO_EXTENSION));

                  if($imageFileType=="png" || $imageFileType=="jpg" || $imageFileType=="jpeg" || $imageFileType=="gif")
                  {
                    if(!is_dir($target_dir))
                    {
                     mkdir($target_dir, 0777, true);
                     chmod($target_dir, 0777);
                    }


                    move_uploaded_file($_FILES["uploadimage"]["tmp_name"],$target_dir.$ipost.".".$imageFileType);
                    $upost=$this->Msocial->update_social(array("imagetype"=>$imageFileType),$ipost);

                    $file_upload=file_get_contents(base_url().'social_image/'.$ipost.".".$imageFileType);

                    $i++;
                  }

              }
            }
            else
            {
              file_put_contents($target_dir.$ipost.".".$imageFileType, $file_upload);
              $upost=$this->Msocial->update_social(array("imagetype"=>$imageFileType),$ipost);
            }
        }
        $data["message"]="Post added";
        $data["response"]=True;

        echo json_encode($data);
     }


     public function autoschedule()
     {
        $data=array();


        $postmessage=$this->input->post("postmessage");
        $user_profile_id=$this->input->post("postinto");

        $i=0;

        foreach ($user_profile_id as $value) {

            $schedule=$this->Msocial->get_free_schedule($value);
            if($schedule==FALSE)
            {
              $data["message"]="No schedule for this account";
              $data["response"]=FALSE;
            }
            else
            {
              $date=date("Y-m-d");
              $dayofweek = date('w', strtotime($date));
              $result    = date('Y-m-d', strtotime(($schedule['weekday'] - $dayofweek).' day', strtotime($date)));

              $postdate=date("Y-m-d H:i:s",strtotime($result.' '.$schedule['time']));
              if(strtotime($postdate)<time())
              {

                $postdate=date("Y-m-d H:i:s",strtotime("+1 week",strtotime($postdate)));
              }

              $insert_post['user_social_id']=$value;
              $insert_post['schedule_id']=$schedule['schedule_id'];
              $insert_post['message']=$postmessage;
              $insert_post['postdate']=$postdate;
              $insert_post['user_id']=$this->session->userdata('user_id');
              $ipost=$this->Msocial->insert_social($insert_post);

                      if(is_array($_FILES) && $i==0) {
                        if(is_uploaded_file($_FILES['uploadimage']['tmp_name'])) {

                             $target_dir = APPPATH."../social_image/";
                              $imageFileType = strtolower(pathinfo($target_dir . basename($_FILES["uploadimage"]["name"]),PATHINFO_EXTENSION));

                              if($imageFileType=="png" || $imageFileType=="jpg" || $imageFileType=="jpeg" || $imageFileType=="gif")
                              {
                                if(!is_dir($target_dir))
                                {
                                 mkdir($target_dir, 0777, true);
                                 chmod($target_dir, 0777);
                                }


                                move_uploaded_file($_FILES["uploadimage"]["tmp_name"],$target_dir.$ipost.".".$imageFileType);
                                $upost=$this->Msocial->update_social(array("imagetype"=>$imageFileType),$ipost);

                                $file_upload=file_get_contents(base_url().'social_image/'.$ipost.".".$imageFileType);

                                $i++;
                              }

                          }
                        }
                        else
                        {
                          file_put_contents($target_dir.$ipost.".".$imageFileType, $file_upload);
                          $upost=$this->Msocial->update_social(array("imagetype"=>$imageFileType),$ipost);
                        }

              $data["message"]="Inserted";
              $data["response"]=True;
            }
        }

        echo json_encode($data);
     }



     public function updatepost()
     {
       $data=array();

        $postmessage=$this->input->post("postmessageUpdate");
        $postime=$this->input->post("timescheduleUpdate");
        $user_profile_id=$this->input->post("postinto");
        $post_id=$this->input->post("updatepostid");

        $postime=date("Y-m-d H:i:s",strtotime($postime));
        $i=0;
        foreach ($user_profile_id as $value) {
          $insert_post['user_social_id']=$value;
          $insert_post['message']=$postmessage;
          $insert_post['postdate']=$postime;
          $insert_post['user_id']=$this->session->userdata('user_id');


          $ipost=$this->Msocial->update_social($insert_post,$post_id);

          if(is_array($_FILES) && $i==0) {
           
            if(is_uploaded_file($_FILES['uploadimageUpdate']['tmp_name'])) {
             
                 $target_dir = APPPATH."../social_image/";
                  $imageFileType = strtolower(pathinfo($target_dir . basename($_FILES["uploadimageUpdate"]["name"]),PATHINFO_EXTENSION));

                  if($imageFileType=="png" || $imageFileType=="jpg" || $imageFileType=="jpeg" || $imageFileType=="gif")
                  {
                    if(!is_dir($target_dir))
                    {
                     mkdir($target_dir, 0777, true);
                     chmod($target_dir, 0777);
                    }


                    move_uploaded_file($_FILES["uploadimageUpdate"]["tmp_name"],$target_dir.$post_id.".".$imageFileType);
                    $upost=$this->Msocial->update_social(array("imagetype"=>$imageFileType),$post_id);

                    $file_upload=file_get_contents(base_url().'social_image/'.$post_id.".".$imageFileType);

                    $i++;
                  }

              }
            }
            else
            {
              file_put_contents($target_dir.$post_id.".".$imageFileType, $file_upload);
              $upost=$this->Msocial->update_social(array("imagetype"=>$imageFileType),$ipost);
            }
        }
        $data["message"]="Post Updated";
        $data["response"]=True;

        echo json_encode($data);
     }




     public function deleteProfile()
     {
      $data=array();
      $id=$this->input->post('id');
      if($id=="")
      {
        $data["message"]="Invalid Data";
        $data["response"]=FALSE;
      }
      else
      {
        $check=$this->Msocial->deleteProfile($id);
        if($check>0){
          $data["message"]="Profile deleted";
          $data["response"]=TRUE;
        }
        else
        {
          $data["message"]="Something went wrong";
            $data["response"]=FALSE;
        }
      }

      echo json_encode($data);
     }



     public function get_post()
     {

      $user_profile_id=$this->input->post('id');

      $post = $this->Msocial->get_post($user_profile_id);


      $data=array();


      if($post!=FALSE)
      {
        $data['response']=true;
        foreach ($post as $key => $value) {
                                              switch ($value->profile_name) {
                                              case 'facebook':
                                              $imgUrl="https://graph.facebook.com/".$value->auth_user_id."/picture?type=square";
                                              break;

                                              case 'twitter':
                                              $imgUrl="https://twitter.com/".$value->auth_user_id."/profile_image?size=normal";
                                              break;
                                              case 'instragram':
                                              $imgUrl="https://graph.facebook.com/".$value->auth_user_id."/picture?type=square";
                                              break;
                                            }
          $data['post'][]=array("id"=>$value->post_id,"start"=>$value->postdate,"title"=>$value->message,"color"=>$value->profile_default_color,"icon"=>$value->profile_icon,"imageurl"=>$imgUrl,"imagetype"=>$value->imagetype,"status"=>$value->status,"social_id"=>$value->user_social_id,"postime"=>$value->postdate);
        }

      }
      else
        {
          $data['response']=FALSE;
        }


     echo json_encode($data);
     exit();
     }


      public function check()
     {
      $aa=$this->getTwitterProfile("978922681394442240-148PJJNZI1rkJ377qbJy9QdiszGb944","Izu2kiMdx5EaDgMClfpoHgGEQROOPew06OlONFThWLbop");
      print_r($aa);
     }







}
