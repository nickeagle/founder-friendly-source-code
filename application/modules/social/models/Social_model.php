<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Social_model extends CI_Model {

    /**
     * Class constructor
     *
     * @return  void
     */
        public function __construct()
        {
            parent::__construct();
        }


        public function get_events($start,$end)
        {
            //$this->db->select('*');
            //$result = $this->db->get_where('calendar_events',array('start'=>$start,'end'=>$end));
            $result = $this->db->get('calendar_events');
            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }

        public function add_event($data)
        {

            $this->db->insert('calendar_events', $data);
        }

        public function get_event($id)
        {
            return $this->db->where("ID", $id)->get("calendar_events");
        }

        public function update_event($id, $data)
        {
            $this->db->where("ID", $id)->update("calendar_events", $data);
        }

        public function delete_event($id)
        {
            $this->db->where("ID", $id)->delete("calendar_events");
        }

        public function insert_social($data)
        {

            $this->db->insert('social_post', $data);

            return $this->db->insert_id();
        }

        public function update_social($data,$id)
        {
            $this->db->where("post_id", $id)->update("social_post", $data);
        }

        public function getSocial_by_id($id)
        {
            $result = $this->db->get_where('social_post',array("post_id"=>$id));

            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }


        public function getSchedule()
        {
            $result=$this->db->select("t1.*,t2.profile_id,t2.auth_user_id,t3.profile_name,t3.profile_icon,t3.profile_default_color")->from("`social_schedule` t1,user_social_profile t2,social_profile t3")->where("t1.status=1")->where("t1.user_id=".$this->session->userdata('user_id')."")->where("t2.id=t1.user_profile_id")->where("t3.id=t2.profile_id")->get();
           // $result = $this->db->get_where('social_schedule',array("status"=>1,"user_id"=>$this->session->userdata('user_id')));
            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }


        public function addSchedule($data)
        {
            $this->db->insert('social_schedule', $data);
        }

        public function getSchedule_by_id($id)
        {
            $result = $this->db->get_where('social_schedule',array("id"=>$id));

            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }




        public function updateSchedule($data,$id)
        {
            $this->db->where("id", $id)->update("social_schedule", $data);
        }


        public function getSocialProfile()
        {

            $result = $this->db->get_where('social_profile',array("status"=>1));
            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }


        public function update_authorization_token($data)
        {
            $this->db->insert('user_social_profile', $data);
        }

        public function get_profile_by_name($profile_name)
        {
            $result = $this->db->get_where('social_profile',array("profile_name"=>$profile_name,"status"=>1));

            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }


        public function get_user_profile($user_id,$profile_id)
        {
             $result = $this->db->get_where('user_social_profile',array("user_id"=>$user_id,"profile_id"=>$profile_id,"status"=>1));

            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }


        public function getallSocialProfile()
        {
            $result = $this->db->get_where('social_profile',array("status"=>1));

            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }


        public function checkauthtoken($auth_token,$profile_id,$user_id)
        {


            $result = $this->db->get_where('user_social_profile',array("auth_token"=>$auth_token,"user_id"=>$user_id,"profile_id"=>$profile_id,"status"=>1));

            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }

        public function auth_user_id($auth_user_id,$profile_id,$user_id)
        {


            $result = $this->db->get_where('user_social_profile',array("auth_user_id"=>$auth_user_id,"user_id"=>$user_id,"profile_id"=>$profile_id,"status"=>1));

            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }


        public function deleteProfile($id)
        {
            $this->db->where("id", $id)->update("user_social_profile", array("status"=>0));
            return $this->db->affected_rows();
        }


        public function get_user_profil_by_id($user_profile_id)
        {
            $result=$this->db->query(" SELECT t1.*,(SELECT profile_name from social_profile where id=t1.profile_id) as profile_name FROM user_social_profile t1 WHERE t1.id=".$user_profile_id);

            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }

        public function get_free_schedule($user_profile_id)
        {
            $check=0;

            $query_schedule=$this->db->get_where("social_schedule",array("user_profile_id"=>$user_profile_id,"status"=>1));

            if($query_schedule->num_rows()!==0)
            {
                 $result_schedule=$query_schedule->result();

                 foreach ($result_schedule as $schedule) {

                     $query_profile=$this->db->get_where("social_post",array("schedule_id"=>$schedule->id,"status"=>1));

                     if($query_profile->num_rows()==0)
                     {
                        $check++;
                        $data['schedule_id']=$schedule->id;
                        $data['weekday']=$schedule->weekday;
                        $data['time']=$schedule->time;
                        break;
                     }
                 }

                 if($check!=0)
                 {
                    return $data;
                 }
                 else
                 {
                    return FALSE;
                 }
            }
            else
                return FALSE;
        }


        public function get_post($user_profile_id)
        {
            $query="SELECT t1.*,t2.*,t3.profile_name,t3.profile_icon,t3.profile_default_color FROM social_post t1,user_social_profile t2,social_profile t3 WHERE t1.status>=1 and t1.user_id=".$this->session->userdata('user_id')." and t2.id=t1.user_social_id and t3.id=t2.profile_id order by t1.postdate desc;";
            if($user_profile_id!="")
            {
                $query.=" and t1.user_social_id=".$user_profile_id;
            }
            $result=$this->db->query($query);

             //$result=$this->db->select("t1.*,t2.profile_id,t2.auth_user_id,t3.profile_name,t3.profile_icon,t3.profile_default_color")->from("`social_post` t1,user_social_profile t2,social_profile t3")->where("t1.status>=1")->where("t1.user_id=".$this->session->userdata('user_id')."")->where("t2.id=t1.user_social_id")->where("t3.id=t2.profile_id")->order_by('t1.postdate', 'DESC')->get();

            if($result->num_rows()!==0)
                return $result->result();
            else
                return FALSE;
        }


        public function checkPost($datetime)
        {
            //echo $query1="SELECT t1.*,t2.auth_token,t2.auth_token_secrect,t2.profile_id,t3.profile_name FROM `social_post` t1,user_social_profile t2,social_profile t3 where t1.postdate='".$datetime."' and t1.status=1 and t2.id=t1.`user_social_id`and t2.profile_id=t3.id";
            $query=$this->db->select("t1.*,t2.auth_token,t2.auth_token_secrect,t2.profile_id,t3.profile_name")->from("social_post t1,user_social_profile t2,social_profile t3")->where("t1.postdate='".$datetime."'")->where("t1.status=1")->where("t2.id=t1.user_social_id")->where("t2.profile_id=t3.id")->get();

            //$query=$this->db->query($query1);

            if($query->num_rows()!=0)
            {
                return $query->result();

            }
            else
            {

                return FALSE;
            }
        }


}
