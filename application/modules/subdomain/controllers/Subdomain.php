<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subdomain extends MY_Controller {

	/**
     * Class constructor
     *
     * Loads required models, loads the hook class and add a hook point
     *
     * @return  void
     */
	public function __construct()
	{
		parent::__construct();
		$model_list = [
			'sites/Sites_model' => 'MSites',
			'sites/Pages_model' => 'MPages',
			'shared/Revision_model' => 'MRevisions',
			'user/Users_model' => 'MUsers',
		];
		$this->load->model($model_list);

		$this->hooks = load_class('Hooks', 'core');
        $this->data = [];

		/** Hook point */
		$this->hooks->call_hook('subdomain_construct');
	}

	/**
	 * Generate the site
	 *
	 * @param  string 	$page
	 * @return void
	 */
	public function index($page = NULL)
	{
		/** Hook point */
		$this->hooks->call_hook('subdomain_index_pre');

		$server_scheme = server_scheme();
		$req_host = $_SERVER['HTTP_HOST'];
		$base_url = parse_url(base_url());
		$app_host = $base_url['host'];
		$sub = explode('.', $req_host);

		$site_content = $this->MSites->get_by_field_value('sub_domain', $sub[0]);
		if (count($site_content) > 0)
		{
			/** If there is no page value then its home page */
			if ( ! $page)
			{
				$page = 'index';
			}
			else
			{
				$page_arr = explode(".", $page);
				$page = $page_arr[0];
			}

			$page = $this->MPages->getSinglePage($site_content[0]['sites_id'], $page);
			$page_content = $this->MPages->load_page($page->pages_id);
			/** Fix relative path */
			$base_url = '<base href="' . base_url() . 'elements/">';
			$render_page = str_replace('<!--baseURL-->', $base_url, $page_content);
			/** Add meta info */
			$meta = '';
			$meta .= '<title>' . $page->pages_title . '</title>' . "\n";
			$meta .= '<meta name="keywords" content="' . $page->pages_meta_keywords . '">' . "\n";
			$meta .= '<meta name="description" content="' . $page->pages_meta_description . '">' . "\n";
			$render_page = str_replace('<!--pageMeta-->', $meta, $render_page);
			/** Add other header */
			$header = '';
			$header .= $page->pages_header_includes . "\n";

			if ($site_content[0]['global_css'] != '')
			{
				$header .= "\n<style>" . $site_content[0]['global_css'] . "</style>\n";
			}
			if ($page->pages_css != '')
			{
				$header .= "\n<style>" . $page->pages_css . "</style>\n";
			}

			if($site_content[0]['sites_tracking']!="")
                {
                $header .="<script async src='https://www.googletagmanager.com/gtag/js?id=".$site_content[0]['sites_tracking']."'></script>\n";
                $header .="\n\t<script>
                                    window.dataLayer = window.dataLayer || [];
                                    function gtag(){dataLayer.push(arguments);}
                                    gtag('js', new Date());

                                    gtag('config', '".$site_content[0]['sites_tracking']."');
                                    </script>\n";
                
                }

                if($site_content[0]['facebook_pixel']!="")
                {
                $header .="<script>
                                    !function(f,b,e,v,n,t,s)
                                    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                                    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                                    n.queue=[];t=b.createElement(e);t.async=!0;
                                    t.src=v;s=b.getElementsByTagName(e)[0];
                                    s.parentNode.insertBefore(t,s)}(window, document,'script',
                                    'https://connect.facebook.net/en_US/fbevents.js');
                                    fbq('init', '".$site_content[0]['facebook_pixel']."');
                                    fbq('track', 'PageView');
                                    </script>\n";
                $header .="\n\t<noscript><img height='1' width='1' style='display:none'
                                src='https://www.facebook.com/tr?id=".$site_content[0]['facebook_pixel']."&ev=PageView&noscript=1'
                                /></noscript>
                                <!-- End Facebook Pixel Code -->\n";
                
                }

			$render_page = str_replace('<!--headerIncludes-->', $header, $render_page);
			$render_page = str_replace('<link rel="icon" type="image/png" href="https://example.com/favicon.png" />', '<link rel="icon" type="image/png" href="'.$site_content[0]['favicon_url'].'">', $render_page);
			/** Load html with Simple HTML DOM */
			$this->load->library('Simple_html_dom');
			$raw = str_get_html($render_page, true, true, DEFAULT_TARGET_CHARSET, false);
			if (empty($raw))
			{
				show_404();
			}

			/** Fix the menu link */
			foreach($raw->find('a') as $element)
			{
				if ( substr($element->href, 0, 1) !== '#' && !$element->hasAttribute('data-toggle') && !strpos($element->href, '//') )
				{
					$element->href = $server_scheme . '://' . $req_host . '/' . $element->href;
				}
			}

			/** Strip out video overlays */
			foreach ($raw->find('.frameCover') as $element)
			{
				$element->outertext = "";
			}

			/** Custom header to deal with XSS protection */
			header("X-XSS-Protection: 0");

			/** Hook point */
			$this->hooks->call_hook('subdomain_index_post');

			echo $raw;
		}
		else
		{
			/** Hook point */
			$this->hooks->call_hook('subdomain_index_error');

			show_404();
		}
	}

	/**
     * Controller desctruct method for custom hook point
     *
     * @return void
     */
	public function __destruct()
	{
		/** Hook point */
		$this->hooks->call_hook('subdomain_destruct');
	}

}