<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Analytics extends MY_Controller {
    /**
      * Class constructor
      *
      * @return void
      */
    public function __construct()
    {
        parent::__construct();
        if ( ! $this->session->has_userdata('user_id') && $this->uri->segment(1) != 'loadsinglepage' && $this->uri->segment(1) != 'loadsingleframe')
        {
            redirect('auth', 'refresh');
        }

        $this->load->module('package');

        if($this->package->check_subscription()==FALSE)
        {
            redirect($this->config->item('mail_url').'account/subscription');
        }
        $this->data = [];
          $this->hooks->call_hook('sites_construct');
    }
    /**
      * Index
      *
      * @return void
      */
    public function index()
    {
      /** Hook point */
      $this->hooks->call_hook('sites_index_pre');

      $this->data['title'] = $this->lang->line('Analytic_index_title');
       $this->data['page'] = "analytics";
      
        $this->load->view('analytics',$this->data);
       
    }

    public function new()
    {
      $this->load->view('analyticNew');
    }





}
