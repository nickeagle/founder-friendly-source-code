<?php $this->load->view("shared/header.php");?>

<body class="analytics  theme">
<div class="container">
<?php $this->load->view("shared/nav.php"); ?>
</div>
<div class="container-fluid">
    <div class="row">
    <div class="col-lg-9 col-xs-12 col-sm-12"><h1>Analytics</h1></div>
    <div class="col-lg-3 col-xs-12 col-sm-12 text-right"><img style="max-width:200px; padding-top:15px" src="/images/uploads/1/5aa12012786d0_powered-by-Google-Analytics-(1).png"></div>
  </div>
</div>


<div class="container-fluid">

    <div class="row padding" style="margin-left:0px;">
      <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="pull-left" id="auth-button"></div>
        <div class="pull-right" id="viewid"></div>
      </div>

    <div class="col-lg-12 col-sm-12 col-xs-12"><br></div>

        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div id="view-selector-container"></div><div id="days" style="float:right; display:none;"> Last 30 Days</div>
        </div>
    </div>

</div>
<!-- no analytics connected -->
<div class="row" id="notloadedanalytics" style="display: block;">


                <div class="col-md-6 col-md-offset-3">

                    <div class="alert alert-info" style="margin-top: 30px; background-color: #f7f9fa;
    border-color: #e9ecef; padding: 18px;">
                        <h2>You are without analytics!</h2>
                        <p>
                            It appears you havn't configured analytics yet, All you need to do, is click the orange button above an sign in with your google analytics account.<br><br>
                                If you don't have an analytics account yet, It's free to create one, and you will need to copy the "view id" to the websites you would like to track.</p>
                        <br>
                        <a href="https://founder-friendly.com/install-tracking-code/" class="btn btn-primary btn-lg btn-wide">Watch tutorial</a>
                    </div>

                </div><!-- ./col -->


        </div>
<!-- connected analytics -->

<div id="loadedanalytics" style="display: none;">
   <!-- Start of top widget row -->
<div class="container-fluid">
    <div class="container-fluid">
                  <div class="col-lg-3">
                      <div class="panel panel-primary">
                          <div class="panel-heading">
                              <div class="row">
                                  <div class="col-xs-3">
                                      <i class="fa fa-line-chart fa-5x"></i>
                                  </div>
                                  <div class="col-xs-9 text-right">
                                      <div class="huge"><span id="totalvisitscount">0</span>  </div>
                                      <div>Total Sessions</div>
                                  </div>
                              </div>
                          </div>

                              <div class="panel-footer">
                                  <i style="float:right; color:#fff;" data-placement="bottom" data-toggle="tooltip" title="This is the total number of sessions on your site, 1 user visiting your website twice equals 2 sessions." class="fa fa-question-circle"></i>

                                  <div class="clearfix"></div>
                              </div>

                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="panel panel-primary">
                          <div class="panel-heading">
                              <div class="row">
                                  <div class="col-xs-3">
                                      <i class="fa fa-users fa-5x"></i>
                                  </div>
                                  <div class="col-xs-9 text-right">
                                      <div class="huge" id="totalusers">0</div>
                                      <div>Total Users</div>
                                  </div>
                              </div>
                          </div>

                              <div class="panel-footer">
                                  <i style="float:right; color:#fff;" data-placement="bottom" data-toggle="tooltip" title="The total amount of unique users visiting your website" class="fa fa-question-circle"></i>

                                  <div class="clearfix"></div>
                              </div>

                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="panel panel-primary">
                          <div class="panel-heading">
                              <div class="row">
                                  <div class="col-xs-3">
                                      <i class="fa fa-exclamation-triangle fa-5x"></i>
                                  </div>
                                  <div class="col-xs-9 text-right">
                                      <div class="huge" id="bouncerate">0</div>
                                      <div>Bounce Rate</div>
                                  </div>
                              </div>
                          </div>
                              <div class="panel-footer">
                                    <i style="float:right; color:#fff;" data-placement="bottom" data-toggle="tooltip" title="The percentage of visitors to your website who navigate away from the site after viewing only one page." class="fa fa-question-circle"></i>

                                  <div class="clearfix"></div>
                              </div>
                      </div>
                  </div>
                  <div class="col-lg-3">
                      <div class="panel panel-primary">
                          <div class="panel-heading">
                              <div class="row">
                                  <div class="col-xs-3">
                                      <i class="fa fa-clock-o fa-5x"></i>
                                  </div>
                                  <div class="col-xs-9 text-right">
                                      <div class="huge" id="averageduration">0</div>
                                      <div>Average Duration</div>
                                  </div>
                              </div>
                          </div>
                              <div class="panel-footer">
                                <i style="float:right; color:#fff;" data-placement="bottom" data-toggle="tooltip" title="The average time people spend on your website in a single visit formated in Hours.Minutes.Seconds" class="fa fa-question-circle"></i>

                                  <div class="clearfix"></div>
                              </div>
                      </div>
                  </div>
              </div>
          </div>
              <!-- End of Widget Row -->



<div class="container-fluid">
  <div class="row">
    <div class="col-lg-8 col-xs-12 col-sm-12">

      <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 ">

          <div class="box padding">
            <div><h4> <i style=" color:#000;" data-placement="bottom" data-toggle="tooltip" title="Daily Visits is the number of unduplicated (counted only once) visitors to your website over the course of the last 30 day. " class="fa fa-question-circle"></i> Daily Visits</h4>
</div>
            <div class="linecanvas" id="divchart1"><canvas id="myChart1"></canvas></div>
          </div>
        </div>
      </div>

      <div class="row"><br></div>

      <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 ">

          <div class="box padding">
             <div><h4> <i style=" color:#000;" data-placement="bottom" data-toggle="tooltip" title="Bounce rate represents the percentage of visitors who enter the site and then leave (bounce) rather than conitnuing to view other pages within the same site." class="fa fa-question-circle"></i> Bounce Rate</h4></div>
            <div class="linecanvas" id="divchart2"><canvas id="myChart2"></canvas></div>
          </div>
        </div>
      </div>

      <div class="row"><br></div>

      <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 ">

          <div class="box padding">
             <div><h4> <i style=" color:#000;" data-placement="bottom" data-toggle="tooltip" title="Most popular pages is a list of your top 10 most visited pages." class="fa fa-question-circle"></i> Most Popular Pages</h4></div>
            <div class="linecanvas" >
              <div id="chart-container-fifth" style="height: 100%;overflow: auto;">



              </div></div>
          </div>
        </div>
      </div>

    </div>

    <div class="col-lg-4 col-xs-12 col-sm-12">

      <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 box padding ">
          <div><h4> <i style=" color:#000;" data-placement="bottom" data-toggle="tooltip" title="New visitors are people who are coming to your site for the first time on a device. Returning visitors have come back to your site before." class="fa fa-question-circle"></i> New Visitor vs Returning Visitor</h4></div>
           <div class="canvas" align="center" id="divchart3"><canvas id="myChart3"></canvas></div>
        </div>
      </div>
      <div class="row"><br></div>

      <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 box padding ">
           <div><h4> <i style=" color:#000;" data-placement="bottom" data-toggle="tooltip" title="A comparison of which devices your website vistitors are using" class="fa fa-question-circle"></i> Devices</h4></div>
           <div class="canvas" id="divchart4"><canvas id="myChart4" ></canvas></div>
        </div>
      </div>
      <div class="row"><br></div>

      <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12 box padding ">
           <div><h4> <i style=" color:#000;" data-placement="bottom" data-toggle="tooltip" title="Traffic sources is a report that provides an overview of the different kinds of sources that send traffic to your Web site, for example direct traffic (clicks from bookmarks or visitors who know your URL)." class="fa fa-question-circle"></i> Traffic Sources</h4></div>
           <div class="canvas" id="divchart5"><canvas id="myChart5"></canvas></div>
        </div>
      </div>

    </div>


  </div>
</div>

  <div class="row">
        <div class="container-fluid"> <p>
  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    How to set up analytics
  </a>
</p>
</div>
<div style="padding-bottom:30px;" class="collapse" id="collapseExample">
  <div class="container-fluid">
    <section class="article-container" data-stats-ve="35"><h1>Set up Google Analytics</h1><div class="cc">

    <ol>
      <li>If you have not already done so, <a href="https://www.google.com/analytics/">create a property in Google Analytics</a>. Create one Analytics property for each website you want to track. Creating a property generates a tracking ID, which you'll use in your site settings.</li>
      <li><a href="#trackingID">Find your tracking ID</a>, as described below. You'll use this tracking ID in the tracking code snippet.</li>
      <li>Copy your tracking and paste it in your site settings in the website builder when creating your website.</li>
      <li>Come back to this page to view your website analytics.</li>
      <li>*If you see a Google Analytics orange button above, just click to authenticate Founder Friendly to view data from your Google Analytics account.</li>


    </ol>

    <h4 id="trackingID">Find your tracking ID</h4>

    <p>To find the tracking ID:</p>

    <ol>
      <li>Sign in to <a href="https://www.google.com/analytics/web/#home/" target="_blank">your Analytics account</a>.</li>
      <li>Click <a class="glossary-term" data-answer="6132368" href="/analytics/answer/6132368" target="_blank" data-stats-ve="40">Admin</a>.</li>
      <li>Select an account from the menu in the <em>ACCOUNT</em> column.</li>
      <li>Select a property from the menu in the <em>PROPERTY</em> column.</li>
      <li>Under <em>PROPERTY</em>, click <strong>Tracking Info &gt; Tracking Code</strong>. Your <a class="glossary-term" data-answer="7372977" href="/analytics/answer/7372977" data-stats-ve="40">tracking ID</a> is displayed at the top of the page.</li>
    </ol>



    </div></section>  </div>
</div>
  </div>
</div>



<script>
(function(w,d,s,g,js,fjs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));
</script>


<script type="text/javascript">
gapi.analytics.ready(function() {

  var CLIENT_ID = '967332182733-dpl7ev8nbjieagro0drnk9077f4qg36a.apps.googleusercontent.com';
    var SCOPES_VERSION1 = ['https://www.googleapis.com/auth/analytics.readonly'];

  gapi.analytics.auth.authorize({
    container: 'auth-button',
    userInfoLabel:'Analytic Sign In: ',
    clientid: CLIENT_ID,

  });



  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector-container'
  });

   viewSelector.execute();

  gapi.analytics.auth.on('success', function(response) {

    document.getElementById("loadedanalytics").style.display="block";
    document.getElementById("days").style.display="block";
    document.getElementById("notloadedanalytics").style.display="none";

  });






  var result1 = new gapi.analytics.report.Data({
    query: {
     metrics: 'ga:sessions',
      dimensions: 'ga:date',
      'start-date': '30daysAgo',
      'end-date': 'today'
    }
  });

  var result2 = new gapi.analytics.report.Data({

    query: {
      metrics: 'ga:bounceRate',
    dimensions: 'ga:yearWeek',
    'start-date': '30daysAgo',
    'end-date': 'today'
    }
  });

  var result3 = new gapi.analytics.report.Data({

    query: {
      metrics: 'ga:sessions',
      dimensions: 'ga:userType',
      'start-date': '30daysAgo',
      'end-date': 'today',
      'max-results': '5'
    }
  });

  var result4 = new gapi.analytics.report.Data({

    query: {
      metrics: 'ga:sessions',
    dimensions: 'ga:deviceCategory',
    'start-date': '30daysAgo',
    'end-date': 'today',
    'max-results': '5'
    }
  });

    var result5 = new gapi.analytics.report.Data({

    query: {
   metrics: 'ga:sessions',
     dimensions: 'ga:channelGrouping',
     'start-date': '30daysAgo',
     'end-date': 'today',
     'max-results': '5'
    }
  });

  // Step 6: Hook up the components to work together.
var dataChartFifth = new gapi.analytics.googleCharts.DataChart({
 query: {
   metrics: 'ga:sessions',
   dimensions: 'ga:pageTitle',
   'sort': '-ga:sessions',
   'start-date': '30daysAgo',
   'end-date': 'today',
   'max-results': '10'

 },
 chart: {
   container: 'chart-container-fifth',
   type: 'TABLE',
   options: {
     width: '100%'
   }
 }
});


  result1.on('success', function(response) {
  makeChart1(response);
});
    result2.on('success', function(response) {
  makeChart2(response);
});

  result3.on('success', function(response) {
  makeChart3(response);
});

    result4.on('success', function(response) {
  makeChart4(response);
});
        result5.on('success', function(response) {
  makeChart5(response);
});

  viewSelector.on('change', function(ids) {
    var newIds = {
      query: {
        ids: ids
      }
    }
    result1.set(newIds).execute();
    result2.set(newIds).execute();
    result3.set(newIds).execute();
    result4.set(newIds).execute();
    result5.set(newIds).execute();
    dataChartFifth.set({query: {ids: ids}}).execute();
    queryCoreReportingApiWithIds(ids);
    //ids['account_id'] = viewSelector.B.fb;
    //ids['property_id'] = viewSelector.HC.fb;
    //ids['profile_id'] = viewSelector.RF.fb;
    console.log(viewSelector.HC.fb);
    document.getElementById("viewid").innerHTML="<b>View Id:</b>"+newIds;
  });
});
</script>
<script type="text/javascript">
  function makedate(a)
  {
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";

      var b=new Array();
      for(var i=0;i<a.length;i++)
      {
      b.push(a.charAt(i));
      }

      var da=b[0]+b[1]+b[2]+b[3]+"-"+b[4]+b[5]+"-"+b[6]+b[7];
      var d = new Date(da);
      var n = d.getDate()+"-"+month[d.getMonth()];
      return n;
  }
   function makeday(a)
  {

      var b=new Array();
      for(var i=0;i<a.length;i++)
      {
      b.push(a.charAt(i));
      }

      var da=b[4]+b[5];
      return da;
  }
  function makeChart1(response)
  {
    document.getElementById("divchart1").innerHTML = '&nbsp;';
    document.getElementById("divchart1").innerHTML = '<canvas id="myChart1"></canvas>';
    var ctx1 = document.getElementById("myChart1");
    var data=new Array();
    var labels=new Array();
    for(var i=0;i<response.rows.length;i++)
    {
      var dumb={x:response.rows[i][0],y:response.rows[i][1]};
      data.push(dumb);
      labels.push(makedate(response.rows[i][0]));
    }


    var myLineChart  = new Chart(ctx1,{
      type: 'line',
    data: {
        labels: labels,
        datasets: [{
          borderColor:'rgba(74, 144, 226,0.8)',
          backgroundColor:'rgba(74, 144, 226, 0.2)',
            label: 'Daily Visits',
            data: data
        }]
    },
    options: {
      responsive: true,
      layout: {
            padding: {
                left: 50,
                right: 0,
                top: 0,
                bottom: 40
            }
        },
            scales: {
              xAxes: [{
                gridLines: {
                  color: 'rgba(15, 115, 115,0.4)',
                  zeroLineColor: 'rgba(15, 115, 115,0.4)'
                }
              }],
              yAxes: [{
                gridLines: {
                  color: 'rgba(15, 115, 115,0.4)'
                },
                ticks: {
                  suggestedMax: 5,
                  stepSize: 20
                }
              }]
            }
          }

    });

  }

    function makeChart2(response)
  {
    console.log(response);
    document.getElementById("divchart2").innerHTML = '&nbsp;';
    document.getElementById("divchart2").innerHTML = '<canvas id="myChart2"></canvas>';
    var ctx2 = document.getElementById("myChart2");
    var data=new Array();
    var labels=new Array();
    for(var i=0;i<response.rows.length;i++)
    {
      var dumb={x:response.rows[i][0],y:response.rows[i][1]};
      data.push(dumb);
      labels.push(makeday(response.rows[i][0]));
    }


    var myLineChart2  = new Chart(ctx2,{
      type: 'line',
    data: {
        labels: labels,
        datasets: [{
          borderColor:'rgba(26, 188, 156,0.8)',
          backgroundColor:'rgba(26, 188, 156, 0.2)',
            label: 'Bounce Rate',
            data: data
        }]
    },
    options: {
      responsive: true,
      layout: {
            padding: {
                left: 50,
                right: 0,
                top: 0,
                bottom: 40
            }
        },
            scales: {
              yAxes: [{
                ticks: {
                  callback: function(value){return value+ "%"},
                  suggestedMax: 5,
                  scaleLabel: {
                   display: true,
                   labelString: "Percentage"
                  }
                }
              }]
            }
          }

    });
  }

  function makeChart3(response)
  {
    document.getElementById("divchart3").innerHTML = '&nbsp;';
    document.getElementById("divchart3").innerHTML = '<canvas id="myChart3"></canvas>';
    var data=new Array();
    var labels=new Array();
    for(var i=0;i<response.rows.length;i++)
    {
      data.push(response.rows[i][1]);
      labels.push(response.rows[i][0]);
    }


    var ctx3 = document.getElementById("myChart3");
    var myPieChart3 = new Chart(ctx3,{
    type: 'pie',
    data: {
        labels: labels,
        datasets: [{
            data:data,
            backgroundColor: [
                'rgba(255, 99, 132, 2)',
                'rgba(54, 162, 235, 2)'
            ],
        }]
    },
    options: {
      responsive: true,
      layout: {
            padding: {
                left: 0,
                right: 80,
                top: 0,
                bottom: 0
            }
        }
      }
    });
  }

    function makeChart4(response)
  {
    document.getElementById("divchart4").innerHTML = '&nbsp;';
    document.getElementById("divchart4").innerHTML = '<canvas id="myChart4"></canvas>';
    var data=new Array();
    var labels=new Array();
    for(var i=0;i<response.rows.length;i++)
    {
      data.push(response.rows[i][1]);
      labels.push(response.rows[i][0]);
    }


    var ctx4 = document.getElementById("myChart4");
    var myPieChart4 = new Chart(ctx4,{
    type: 'pie',
    data: {
        labels: labels,
        datasets: [{
            data:data,
            backgroundColor: [
                'rgba(142, 68, 173, 0.7)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(211, 84, 0,0.5)',
                'rgba(243, 156, 18,0.4)',
                'rgba(231, 76, 60,0.4)'
            ],
        }]
    },
    options: {
      responsive: true,
      layout: {
            padding: {
                left: 0,
                right: 80,
                top: 0,
                bottom: 0
            }
        }
      }
    });
  }

      function makeChart5(response)
  {
    document.getElementById("divchart5").innerHTML = '&nbsp;';
    document.getElementById("divchart5").innerHTML = '<canvas id="myChart5"></canvas>';
    var data=new Array();
    var labels=new Array();
    for(var i=0;i<response.rows.length;i++)
    {
      data.push(response.rows[i][1]);
      labels.push(response.rows[i][0]);
    }


    var ctx5 = document.getElementById("myChart5");
    var myPieChart5 = new Chart(ctx5,{
    type: 'pie',
    data: {
        labels: labels,
        datasets: [{
            data:data,
            backgroundColor: [

                'rgba(26, 188, 156,0.8)',
                'rgba(241, 196, 15,2)',
                'rgba(211, 84, 0,2)',
                'rgba(243, 156, 18,0.4)',
                'rgba(231, 76, 60,0.4)'
            ],
        }]
    },
    options: {
      responsive: true,
      layout: {
            padding: {
                left: 0,
                right: 100,
                top: 0,
                bottom: 0
            }
        }
      }
    });
  }
</script>
</body>

</script>



<script src="https://apis.google.com/js/client.js?onload=authorize"></script>
<script>



function queryCoreReportingApiWithIds(ids) {

  // Query the Core Reporting API for the number sessions for
  // the past seven days.
  gapi.client.analytics.data.ga.get({
    'ids': ids,
    'start-date': '28daysAgo',
    'end-date': 'yesterday',
    'metrics': 'ga:sessions,ga:users,ga:bounceRate,ga:avgSessionDuration'
  })
  .then(function(response) {
      var formattedJson = JSON.stringify(response.result, null, 2);
  var theResult = JSON.parse(formattedJson);
  var visits = theResult.totalsForAllResults['ga:sessions'];
  var totalusers = theResult.totalsForAllResults['ga:users'];
  var bouncerate = theResult.totalsForAllResults['ga:bounceRate'];
  var averageduration = theResult.totalsForAllResults['ga:avgSessionDuration'];

  var averageDurationTruncated = parseFloat(Math.round(averageduration * 100) / 100).toFixed(0);
  var hours = Math.floor(averageDurationTruncated/3600);
  var minutes = Math.floor(averageDurationTruncated/60);
  var seconds = averageDurationTruncated % 60;

  bouncerateRounded = parseFloat(Math.round(bouncerate * 100) / 100).toFixed(2);

  document.getElementById('totalvisitscount').innerHTML  = visits;
  document.getElementById('totalusers').innerHTML  = totalusers;
  document.getElementById('bouncerate').innerHTML  = bouncerateRounded+'%';
  document.getElementById('averageduration').innerHTML  = hours + ':' + minutes + ':' + seconds ;
  })
  .then(null, function(err) {
      // Log any errors.
      console.log(err);
  });


  // Handles the authorization flow.
    // `immediate` should be false when invoked from the button click.
    var useImmdiate = true;
    var authData = {
      client_id: CLIENT_ID_VERSION1,
      scope: SCOPES_VERSION1,
      immediate: useImmdiate
    };

    gapi.auth.authorize(authData, function(response) {
       // authButton.hidden = true;
        queryAccounts();

    });

}

  // Add an event listener to the 'auth-button'.
  //document.getElementById('auth-button').addEventListener('click', authorize);



</script>


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>

 <script src="<?php echo base_url(); ?>assets/analytic/Chart.bundle.js"></script>

 <script type="text/javascript">
  $(function () {

$('[data-toggle="tooltip"]').tooltip();

});

</script>
</html>
