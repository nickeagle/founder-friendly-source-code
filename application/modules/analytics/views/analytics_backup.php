<!DOCTYPE html>
<html>
<head>
<title>Analytic</title>
<link href="https://sites.founder-friendly.com/build/sites.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://sites.founder-friendly.com/assets/css/flat-ui-pro-custom.css">

</head>
<body class="analytics">

    <?php $this->load->view("shared/nav.php"); ?>
<div class="container-fluid">
    <div class="row">

                <div class="col-md-9 col-sm-8">

                    <h1>Analytics</h1>

                </div><!-- /.col -->

                <div class="col-md-3 col-sm-4 text-right">
                  <img style="max-width:200px; padding-top:15px" src="/images/uploads/1/5aa12012786d0_powered-by-Google-Analytics-(1).png">

                </div><!-- /.col -->

            </div>
    <!-- Analytics views -->
      <div id="view-selector-container"></div>
      <br/>
    <div></div>
    <div id="date-range-selector-1-container"></div>
    <br/>
  <div></div>
    <div id="embed-api-auth-container"></div>
    <div id="chart-container"></div>

    <div id="chart-container2"></div>


	<!-- GA Chart Browsers by Sessionst -->
	<div id="chart-container-second"></div>
	<!-- Testing another chard ends here -->
  <div id="chart-container-third"></div>
  <!-- Testing another chard ends here -->
  <div id="chart-container-fourth"></div>
  <!-- Testing another chard ends here -->
  <div id="chart-container-fifth"></div>
  <!-- Testing another chard ends here -->
  <div class="col-lg-6 col-sm-12 col-xs-12">
    <h3> Title 1</h3>
    <div id="chart-container-sixth"></div>
  </div>
  <!-- Testing another chard ends here -->
  <div class="col-lg-6 col-sm-12 col-xs-12">
    <h3> Title 1</h3>
    <div id="chart-container-seven"></div>
  </div>

    <!-- Extra Analytics views -->
    <div class="row"><div class="col-lg-12">
      <div id="auth-button"></div>
    </div>

</div>


    <!-- The API response will be printed here. -->
  <p cols="80" rows="20" id="query-output"> 23</p>

    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <div id="view-selector"></div>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <div  id="view-selector1"></div>
      </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-12 col-xs-12">
          <div id="timeline"></div>
        </div>
        <div class="col-lg-6 col-sm-12 col-xs-12">
          <div id="timeline1"></div>
        </div>


    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-12 col-xs-12">
          <div id="timeline2"></div>
        </div>


    </div>











    <div style="padding-top:30px;"> <p>
  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    How to set up analytics
  </a>
</p>

</div>
<div style="padding-bottom:30px;" class="collapse" id="collapseExample">
  <div class="card card-block">
    <section class="article-container" data-stats-ve="35"><h1>Set up Google Analytics</h1><div class="cc">

    <ol>
      <li>If you have not already done so, <a href="https://www.google.com/analytics/">create a property in Google Analytics</a>. Create one Analytics property for each website you want to track. Creating a property generates a tracking ID, which you'll use in your site settings.</li>
      <li><a href="#trackingID">Find your tracking ID</a>, as described below. You'll use this tracking ID in the tracking code snippet.</li>
      <li>Copy your tracking and paste it in your site settings in the website builder when creating your website.</li>
      <li>Come back to this page to view your website analytics.</li>
      <li>*If you see a Google Analytics orange button above, just click to authenticate Founder Friendly to view data from your Google Analytics account.</li>


    </ol>

    <h4 id="trackingID">Find your tracking ID</h4>

    <p>To find the tracking ID:</p>

    <ol>
      <li>Sign in to <a href="https://www.google.com/analytics/web/#home/" target="_blank">your Analytics account</a>.</li>
      <li>Click <a class="glossary-term" data-answer="6132368" href="/analytics/answer/6132368" target="_blank" data-stats-ve="40">Admin</a>.</li>
      <li>Select an account from the menu in the <em>ACCOUNT</em> column.</li>
      <li>Select a property from the menu in the <em>PROPERTY</em> column.</li>
      <li>Under <em>PROPERTY</em>, click <strong>Tracking Info &gt; Tracking Code</strong>. Your <a class="glossary-term" data-answer="7372977" href="/analytics/answer/7372977" data-stats-ve="40">tracking ID</a> is displayed at the top of the page.</li>
    </ol>



    </div></section>  </div>
</div>

    </div><!-- /.container -->


    <!-- modals -->

    <?php $this->load->view("shared/modal_sitesettings.php"); ?>

    <?php $this->load->view("shared/modal_account.php"); ?>

    <?php $this->load->view("shared/modal_deletesite.php"); ?>

    <!-- /modals -->


    <!-- Load JS here for greater good =============================-->
    <?php if (ENVIRONMENT == 'production') : ?>
    <script src="<?php echo base_url('build/sites.bundle.js'); ?>"></script>
    <?php elseif (ENVIRONMENT == 'development') : ?>
    <script src="<?php echo $this->config->item('webpack_dev_url'); ?>build/sites.bundle.js"></script>
    <?php endif; ?>


<script>
(function(w,d,s,g,js,fjs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));
</script>

<script>
gapi.analytics.ready(function() {

  // Step 3: Authorize the user.

  var CLIENT_ID = '967332182733-dpl7ev8nbjieagro0drnk9077f4qg36a.apps.googleusercontent.com';

  gapi.analytics.auth.authorize({
    container: 'auth-button',
    clientid: CLIENT_ID,
    userInfoLabel:'You are : ',
  });

  gapi.analytics.auth.on('signIn', function() {
  console.log(gapi.analytics.auth.getUserProfile());
});


  /**
   * Create a new ViewSelector instance to be rendered inside of an
   * element with the id "view-selector-container".
   */
  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector-container'
  });

  // Render the view selector to the page.
  viewSelector.execute();


  /**
   * Create a new DataChart instance with the given query parameters
   * and Google chart options. It will be rendered inside an element
   * with the id "chart-container".
   */
  var dataChart = new gapi.analytics.googleCharts.DataChart({
    query: {
      metrics: 'ga:sessions',
      dimensions: 'ga:date',
      'start-date': '30daysAgo',
      'end-date': 'today'
    },
    chart: {
      container: 'chart-container',
      type: 'LINE',
      options: {
        width: '100%'
      }
    }
  });

// bounce rate by week of year
var dataChart2 = new gapi.analytics.googleCharts.DataChart({
  query: {
    metrics: 'ga:bounceRate',
    dimensions: 'ga:yearWeek',
    'start-date': '365daysAgo',
    'end-date': 'today'
  },
  chart: {
    container: 'chart-container2',
    type: 'LINE',
    options: {
      width: '100%'
    }
  }
});


  /**
   * Render the dataChart on the page whenever a new view is selected.
   */
  viewSelector.on('change', function(ids) {
    dataChart.set({query: {ids: ids}}).execute();
      dataChart2.set({query: {ids: ids}}).execute();
        dataChartSecond.set({query: {ids: ids}}).execute();
          dataChartThird.set({query: {ids: ids}}).execute();
            dataChartFourth.set({query: {ids: ids}}).execute();
              dataChartFifth.set({query: {ids: ids}}).execute();
                dataChartSixth.set({query: {ids: ids}}).execute();
                  dataChartSeven.set({query: {ids: ids}}).execute();
  });

 //Second view selector
//  var viewSelectorSecond = new gapi.analytics.ViewSelector({
//    container: 'view-selector-second'
//  viewSelectorSecond.execute();
   var dataChartSecond = new gapi.analytics.googleCharts.DataChart({
    query: {
      metrics: 'ga:sessions',
      dimensions: 'ga:userType',
      'start-date': '30daysAgo',
      'end-date': 'today',
      'max-results': '5'
    },
    chart: {
      container: 'chart-container-second',
      type: 'COLUMN',
      options: {
        width: '100%'
      }
    }
  });

  //Third view selector


  var dataChartThird = new gapi.analytics.googleCharts.DataChart({
   query: {
     metrics: 'ga:sessions',
     dimensions: 'ga:channelGrouping',
     'start-date': '30daysAgo',
     'end-date': 'today',
     'max-results': '5'
   },
   chart: {
     container: 'chart-container-third',
     type: 'PIE',
     options: {
       width: '100%'
     }
   }
 });

 // Fourth view
 var dataChartFourth = new gapi.analytics.googleCharts.DataChart({
  query: {
    metrics: 'ga:sessions',
    dimensions: 'ga:deviceCategory',
    'start-date': '30daysAgo',
    'end-date': 'today',
    'max-results': '5'
  },
  chart: {
    container: 'chart-container-fourth',
    type: 'PIE',
    options: {
      width: '100%'
    }
  }
});

// Fifth view
var dataChartFifth = new gapi.analytics.googleCharts.DataChart({
 query: {
   metrics: 'ga:sessions',
   dimensions: 'ga:pageTitle',
   'sort': '-ga:sessions',
   'start-date': '30daysAgo',
   'end-date': 'today',
   'max-results': '10'

 },
 chart: {
   container: 'chart-container-fifth',
   type: 'TABLE',
   options: {
     width: '100%'
   }
 }
});

// Sixth views
var dataChartSixth = new gapi.analytics.googleCharts.DataChart({
 query: {
   metrics: 'ga:goalConversionRateAll',
   dimensions: 'ga:ChannelGrouping',
   'sort': '-ga:goalConversionRateAll',
   'start-date': '30daysAgo',
   'end-date': 'today',
   'max-results': '5'

 },
 chart: {
   container: 'chart-container-sixth',
   type: 'BAR',
   options: {
     width: '100%'
   }
 }
});
// Sixth views
var dataChartSeven = new gapi.analytics.googleCharts.DataChart({
 query: {
   metrics: 'ga:goalConversionRateAll',
   dimensions: 'ga:pageTitle',
   'sort': '-ga:goalConversionRateAll',
   'start-date': '30daysAgo',
   'end-date': 'today',
   'max-results': '5'

 },
 chart: {
   container: 'chart-container-seven',
   type: 'BAR',
   options: {
     width: '100%'
   }
 }
});



// Test 4th single metric
// Query the API and print the results to the page.
  function queryReports() {
    gapi.client.request({
      path: '/v4/reports:batchGet',
      root: 'https://analyticsreporting.googleapis.com/',
      method: 'POST',
      body: {
        reportRequests: [
          {
            viewId: CLIENT_ID,
            dateRanges: [
              {
                startDate: '7daysAgo',
                endDate: 'today'
              }
            ],
            metrics: [
              {
                expression: 'ga:sessions'
              }
            ]
          }
        ]
      }
    }).then(displayResults, console.error.bind(console));
  }

  function displayResults(response) {
      var formattedJson = JSON.stringify(response.result, null, 2);
      document.getElementById('query-output').value = formattedJson;
    }
});
</script>

</body>
</html>
