<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
<style type="text/css">
#pagination span{
  cursor:pointer;
  color:#666666;
  padding:5px;
  font-weight: bold;
}
#pagination span:hover{
  text-decoration: underline;
}
#pagination .current{
  color: #16a085;
}
ul.enlarge{
list-style-type:none; /*remove the bullet point*/
margin-left:0;
}

ul.enlarge .slim-loader1
{
  display: inline;
      position: absolute;
    top: 7px;
    left: 166px;
}

ul.enlarge .uploadBtn
{
  display: inline;
  position: absolute;
  top: 94px;
  left: 143px;
}
ul.enlarge li{
display:inline-block; /*places the images in a line*/
position: relative;
z-index: 0; /*resets the stack order of the list items - later we'll increase this*/
margin:7px 5px 4px 5px;
font-family: 'Droid Sans', sans-serif; /*Droid Sans is available from Google fonts*/
font-size:.9em;
text-align: center;
color: #495a62;
}
ul.enlarge img{
background-color:#ffffff;
padding: 6px;
-webkit-box-shadow: 0 0 6px rgba(132, 132, 132, .75);
-moz-box-shadow: 0 0 6px rgba(132, 132, 132, .75);
box-shadow: 0 0 6px rgba(132, 132, 132, .75);
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
width:200px;
height: 140px;
}
ul.enlarge span{
position:absolute;
left: -9999px;
background-color:#ffffff;
padding: 3px;

text-align: center;
color: #495a62;
-webkit-box-shadow: 0 0 20px rgba(0,0,0, .75));
-moz-box-shadow: 0 0 20px rgba(0,0,0, .75);
box-shadow: 0 0 20px rgba(0,0,0, .75);
-webkit-border-radius: 8px;
-moz-border-radius: 8px;
border-radius:8px;
}
ul.enlarge li:hover{
z-index: 50;
cursor:pointer;
}
ul.enlarge span img{
padding:2px;
background:#ccc;
width:350px;
height:auto;
z-index: 9;
}
ul.enlarge li:hover span{
top: -210px; /*the distance from the bottom of the thumbnail to the top of the popup image*/
left: 0px; /*distance from the left of the thumbnail to the left of the popup image*/
}
ul.enlarge li:hover:nth-child(2) span{
left: -100px;
}
ul.enlarge li:hover:nth-child(3) span{
left: -200px;
}
/**IE Hacks - see https://css3pie.com/ for more info on how to use CS3Pie and to download the latest version**/
ul.enlarge img, ul.enlarge span{
behavior: url(pie/PIE.htc);
}
.nav-tabs>li.active {
    z-index: 0;
}
.loader3 {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
      margin-bottom: 50px;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    margin-right: 0;
    margin-bottom: 0;
    padding: 7px 21px 8px;
    border-radius: 0 0 0 0;
    border-color: transparent;
    border-bottom-color: transparent;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

.slim-btn-upload {
    background-image: url('<?php echo base_url('img/upload.png'); ?>');
    color: #ffffff;
}
.slim-btn-edit {
    background-image: url('<?php echo base_url('img/edit.png'); ?>' );
}
.slim-btn {
  background-color: rgba(160, 156, 156, 0.7);
}
.slim-btn1 {
    position: relative;
    padding: 0;
    margin: 0 7.2px;
    font-size: 15px;
    outline: none;
    width: 36px;
    height: 36px;
    border: none;
    color: #fff;
    background-color: rgba(0,0,0,.7);
    background-repeat: no-repeat;
    background-size: 50% 50%;
    background-position: 50%;
    border-radius: 50%;
}
}

</style>

<ul class="nav nav-tabs nav-append-content">
    <li class="active"><a href="#myImagesTab" id="anchorMyImagesTab">
        <?php
            $size_bytes = get_dir_size('./images/uploads/'. $this->session->userdata('user_id'));
            $size_mb = round(($size_bytes/1000)/1000, 2);
            $package = $this->MPackages->get_by_id($this->session->userdata('package_id'));
            if ( $this->session->userdata('user_type') != 'Admin' )
            {
                $disk_space = $package['disk_space'];
            }
            else
            {
                $disk_space = '&infin;';
            }
        ?>
        <?php echo sprintf($this->lang->line('modal_imagelibrary_tab_myimages'), $size_mb, $disk_space);?></a>
    </li>
    <li class="None"><a href="#stockImages" id="anchorstockImages">
      Stock Images</a>
    </li>

    <?php if ( (isset($adminImages) && $page == 'site_builder') || $this->session->userdata('user_type') == 'Admin' ) : ?><li><a href="#adminImagesTab"><?php echo $this->lang->line('modal_imagelibrary_tab_otherimages'); ?></a></li><?php endif; ?>
</ul> <!-- /tabs -->

<div class="tab-content forImageLib" id="divImageLibrary">

    <div class="tab-pane active" id="myImagesTab">

        <?php if (isset($userImages)) : ?>

         

            <?php $this->load->view("shared/myimages.php", array('userImages' => $userImages)); ?>

        <?php else: ?>

            <!-- Alert Info -->
            <div class="alert alert-info">
                <button type="button" class="close fui-cross" data-dismiss="alert"></button>
                <?php echo $this->lang->line('modal_imagelibrary_message_noimages'); ?>
            </div>

        <?php endif; ?>

    </div><!-- /.tab-pane -->
<!-- /start stock pane tab-pane -->

        <div class="tab-pane" id="stockImages">



                <div id="app">
                  <div>
        <input type="text" class="form-control" id="searchWord" style="height: 35px; width:200px; float:left; margin-right:5px;" value="spring" v-model="query">
        <button type="button" class="btn btn-primary btn-embossed btn-wide" onclick="getResult(1);">Search</button>
        <span id="uploaderr" style="color: #ff0000;padding: 4px;"></span>
        <p style="float:right;">Royalty Free Stock Images </p>
      </div>
      <br/>

        <!--  <img class="imageWrap" style="   margin: 5px;
            border-radius: 10px;
            height:200px; width:200px;  border-radius:10px;
            border-color: black;
            border-width: : 5px;" v-for="image in images" src="{{ image.urls.small }}" data-url="{{ image.urls.full }}" alt="" >-->


            <div id="placeimage"></div>

            <div align="center" id="pagination"></div>


        <br/>
      </div>

        </div><!-- /.stock tab-pane -->

    <div class="tab-pane" id="adminImagesTab">

        <div class="images clearfix" id="adminImages">

            <?php if (isset($adminImages)) : ?>

                <?php foreach ($adminImages as $img) : ?>
                    <div class="image">

                        <?php
                            $imageUrl_ = "./" . $this->config->item('images_dir') . "/" . $img;
                        ?>
                        <div class="imageWrap" data-org-src="<?php echo $this->config->item('images_dir') . "/" . $img?>" data-admin="true" data-thumb="<?php echo thumb($imageUrl_, 250, 140);?>" style="background-image: url('<?php echo thumb($imageUrl_, 250, 140);?>')">

                            <div class="ribbon-wrapper-red"><div class="ribbon-red"><?php echo $this->lang->line('modal_imagelibrary_ribbon_admin'); ?></div></div>
                        </div>

                    </div><!-- /.image -->
                <?php endforeach; ?>

            <?php endif; ?>

        </div><!-- /.adminImages -->

    </div><!-- /.tab-pane -->

</div> <!-- /tab-content -->

<!-- Nicks script for getting images from unsplash and displaying them -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">//<![CDATA[
  var CLIENT_ID = "ac006ccd56b7438ddaa16b9c6b7d22efa456527612f54ee17ed95591fa1684cd";

  /*function makeQuery(query) {

    return "https://api.unsplash.com/photos/search/?query=" + query +  "&client_id=" + CLIENT_ID+"";
  }

  function doQuery(self) {

    window.fetch(makeQuery(self.query))
    .then(function(res){
    console.log(res);

      return res.json();
    })
    .then(function(res){

     placeImage(res);
      //self.images = res;
    });
  }

  var application = new Vue({
    el: "#app",
    data: {
    	query: 'spring',
    	images: [],
    },
    ready: function() {
      console.log(8);
  	  doQuery(this);
    },
    methods: {
      updateQuery: function() {
      	console.log(this.query);
  		  doQuery(this);
      }
    }
  });
  //]]>*/

  function getResult(pagenNo) {
    var searchWord=$("#searchWord").val();
    var query="https://api.unsplash.com/photos/search/?query=" + searchWord +  "&client_id=" + CLIENT_ID+"&page="+pagenNo;
    $("#placeimage").html("<div align='center' ><div class='loader3' style=''></div></div>");
    $("#pagination").html("");
    $.ajax({
            method :"get",
            cache :false,
            url:query,
            dataType:"json",

            success:function(data, textStatus, request){

            console.log(request.getResponseHeader("X-Total"));
            placeImage(data,request.getResponseHeader("X-Total"),request.getResponseHeader("X-Per-Page"),pagenNo);
            }
          });


  }

function placeImage(data,totalNo,perPage,pagenNo) {


    var image="<ul class='enlarge'>";
    for(var i=0;i<data.length;i++)
    {
      image+="<li><img  src='"+data[i].urls.thumb+"' >";
      //image+="<div id='loader"+i+"' class='slim-loader1'><div class='c100 p1 small'><span>1%</span><div class='slice'><div class='bar'></div> <div class='fill'></div> </div></div></div>";
      image+="<button class='slim-btn1 uploadBtn' title='Upload' type='button' data-id='"+i+"' data-url='"+data[i].links.download_location+"' onclick='uploadImage(this);' style='opacity: 1;''><i class='fas fa-cloud-download-alt'></i></button>";
      image+="<span><img src='"+data[i].urls.regular+"'></span>";
      image+="<br>Photo by <a target='_blank' href='"+data[i].user.links.html+"?utm_source=Founder_friendly&utm_medium=referral'>"+data[i].user.name+"</a> <br> on <a href='https://unsplash.com/?utm_source=Founder_friendly&utm_medium=referral' target='_blank'>Unsplash</a>";
      image+="</li>";
    }

    image+='</ul>';

    $("#placeimage").html(image);

    var totalPage=totalNo/perPage;
     var prev="";
    for(var j=3;j>0;j--)
    {
      if((pagenNo-j)>0){
       prev+="<span onclick=getResult("+(pagenNo-j)+")>"+(pagenNo-j)+"</span>";
      }
    }

    var pagination="<span class='prevPage' onclick=getResult("+(pagenNo-1)+")>Prev</span>"+prev+"<span class='current'>"+pagenNo+"</span><span  onclick=getResult("+(pagenNo+1)+")>"+(pagenNo+1)+"</span><span  onclick=getResult("+(pagenNo+2)+")>"+(pagenNo+2)+"</span><span class='nextPage' onclick=getResult("+(pagenNo+1)+")>Next</span><!--<span class='lastPage' onclick=getResult("+parseInt(totalPage)+")>Last</span>-->";

    $("#pagination").html(pagination);

}

$(document).ready(function(){
  getResult(1);
});


  function uploadImage(d)
  {
    var baseUrl="<?php echo base_url(); ?>";
    var max_file_size='<?php echo $this->config->item('upload_max_size')/1000;?>';
    var status_upload_success="<?php echo $this->lang->line('asset_slim_upload_success');?>";
    var button_cancel_label="<?php echo $this->lang->line('asset_slim_button_cancel');?>";
    var button_confirm_label="<?php echo $this->lang->line('asset_slim_button_confirm');?>";
    var label="<?php echo $this->lang->line('myimages_newimage_label');?>";
    var $id=d.dataset.id;
    var $loader=$("#loader"+$id+" div").first();


    var conf=confirm("Are you sure you want to transfer this image to your library !");


    if(conf==true)
    {
      var $slimloader1=$(".slim-loader1");
      $.ajax({
            method :"post",
            cache :false,
            url:baseUrl+"asset/imageUploadUrl",
            data:{"imagePath":d.dataset.url},
            dataType:"json",

            success:function(data){
              $("#uploaderr").html(data.message);
                
                if(data.response==true)
                {
                  var img='<div class="image">';
                        img+='<div class="imageWrap" style="background-image: url('+data.thumb+')"';
                          img+='data-ratio="free"';
                          img+='data-service="'+baseUrl+'asset/imageUploadAjax/"';
                          img+='data-fetcher="fetch.php"';
                          img+='data-max-file-size="'+max_file_size+'"';
                          img+='data-status-file-type=""';
                          img+='data-status-image-too-small=""';
                          img+='data-status-unknown-response=""';
                          img+='data-status-upload-success="'+max_file_size+'"';
                          img+='data-button-cancel-label="'+status_upload_success+'"';
                          img+='data-button-confirm-label="'+button_confirm_label+'"';
                          img+='data-label="'+label+'"';
                          img+='data-org-src="'+data.file+'"';
                          img+='data-thumb="'+data.thumb+'">';
                      
                        img+='</div>';
                      img+='</div>';

                      $("#myImages").append(img);


                      setTimeout(function(){$("#uploaderr").html('');$('.nav-tabs a[href="#myImagesTab"]').tab('show');},2000); 
                }
            }
        });
    }
  }

</script>
