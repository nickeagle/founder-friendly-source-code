

<nav class="mainnav navbar navbar-inverse navbar-fixed-top" role="navigation" id="mainNav">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
			<span class="sr-only"><?php echo $this->lang->line('nav_toggle_navigation'); ?></span>
		</button>
		<div class="navbar-header">
					<a class="navbar-brand" href="<?php echo $this->config->item("mail_url"); ?>" >
						                    <img src="<?php echo base_url() ?>img/site_logo_small.png" alt="">
		                			</a>

					<ul class="nav navbar-nav pull-right visible-xs-block">
						<li><a class="mobile-menu-button" data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-menu7"></i></a></li>
					</ul>
				</div>
	</div>
	<div class="collapse navbar-collapse" id="navbar-collapse-01">
		<ul class="nav navbar-nav">
			<li class="dropdown">



										<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<span class="fui-list-thumbnailed"></span> Social Manager
											<span class="caret"></span>
										</a>
										<ul class="dropdown-menu">
											<!-- only show if user hasn't completed all onboarding -->
					<li><a href="<?php echo base_url() ?>getting-started">Getting Started</a></li>
										<!-- end if -->
					<li><a href="<?php echo base_url() ?>">Site Builder</a></li>
					<li><a href="<?php echo $this->config->item("mail_url"); ?>">Email Campaigns</a></li>
					<li><a href="<?php echo base_url() ?>social">Social Manager</a></li>
				</ul> <!-- /Sub menu -->
									</li>
			<?php if (isset($siteData) || (isset($page) && $page == 'newPage')) : ?>

				<?php if (isset($siteData)) : ?>
					<li class="active">
						<a><span class="fui-home"></span> <span id="siteTitle"><?php echo $siteData['site']->sites_name; ?></span></a>
					</li>
				<?php endif; ?>

				<?php if (isset($page) && $page == 'newPage') : ?>
					<li class="active">
						<a><span class="fui-home"></span> <span id="siteTitle"><?php echo $this->lang->line('newsite_default_title'); ?></span> </a>
					</li>
				<?php endif; ?>

				<?php if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != '') : ?>

					<?php
					// Find out where we came from :)
					$temp = explode("/", $_SERVER['HTTP_REFERER']);
					if (array_pop($temp) == 'users')
					{
						$t = 'nav_goback_users';
						$to = site_url('users');
					}
					else
					{
						$t = 'nav_goback_sites';
						$to = site_url('sites');
					}
					?>

					<li><a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" id="backButton"><span class="fui-arrow-left"></span> <?php echo $this->lang->line($t); ?></a></li>

				<?php else: ?>

					<li><a href="sites" id="backButton"><span class="fui-arrow-left"></span> <?php echo $this->lang->line('nav_goback_users'); ?></a></li>

				<?php endif; ?>

			<?php else: ?>

				<li <?php if (isset($page) && $page == "social") : ?>class="active"<?php endif; ?>><a href="social"><span class="fui-calendar"></span> Social Calender</a></li>
				<li <?php if (isset($page) && $page == "socialQueue") : ?>class="active"<?php endif; ?>><a href="social-queue"><span class="fui-list-numbered"></span> Social Queue</a></li>
				<li <?php if (isset($page) && $page == "socialProfile") : ?>class="active"<?php endif; ?>><a href="social-profiles"><span class="fui-list-numbered"></span> Linked Profiles</a></li>
					<li <?php if (isset($page) && $page == "socialSchedule") : ?>class="active"<?php endif; ?>><a href="social-schedule"><span class="fui-gear"></span> Schedule</a></li>
					<li <?php if (isset($page) && $page == "socialFindcontent") : ?>class="active"<?php endif; ?>><a href="find-content"><span class="fui-eye"></span> Find Content</a></li>
				</ul>

			<?php endif; ?>
		<ul class="nav navbar-nav navbar-right" style="margin-right: 20px;">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->lang->line('nav_greeting'); ?> <?php echo $this->session->userdata('user_fname') . ' ' . $this->session->userdata('user_lname'); ?> <b class="caret"></b></a>
				<span class="dropdown-arrow"></span>
				<ul class="dropdown-menu">

						<li rel0="AccountController\subscription">
								<a href="<?php echo $this->config->item("mail_url"); ?>account/subscription">
									<i class="icon-quill4"></i> Subscriptions
								</a>
							</li>

									<li rel0="AccountController\subscription">
											<a href="https://founder-friendly.com/category/wiki/">
												<i class="icon-quill4"></i> Wiki
											</a>
								</li>
								<li rel0="AccountController\subscription">
										<a href="https://founder-friendly.com/suggest-a-feature/">
											<i class="icon-quill4"></i> Request A Feature
										</a>
							</li>
							<li><a href="<?php echo $this->config->item("mail_url"); ?>/account/profile"><i class="icon-profile"></i> Account</a></li>
							<li class="divider"></li>

							<li><a href="auth/logout"><span class="fui-exit"></span> <?php echo $this->lang->line('nav_logout'); ?></a></li>
				</ul>
			</li>
		</ul>
	</div><!-- /.navbar-collapse -->
</nav><!-- /navbar -->
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  page_id="2105333753030002"
  theme_color="#4A90E2"
  minimized="true">
</div>
