<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<base href="<?php echo base_url(); ?>">

	<title><?php if (isset($title)) { echo $title; } else { echo $this->lang->line('application_name_text'); } ?></title>

	<?php if ($page == 'site_builder') : ?>

		<?php if (ENVIRONMENT == 'production') : ?>
		<link href="<?php echo base_url(); ?>build/builder.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/builder.css" rel="stylesheet">
		<?php endif; ?>

	<?php elseif ($page == 'site') : ?>

		<?php if (ENVIRONMENT == 'production') : ?>
		<link href="<?php echo base_url(); ?>build/sites.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/sites.css" rel="stylesheet">
		<?php endif; ?>

	<?php elseif ($page == 'asset') : ?>

		<?php if (ENVIRONMENT == 'production') : ?>
		<link href="<?php echo base_url(); ?>build/images.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/images.css" rel="stylesheet">
		<?php endif; ?>

	<?php elseif ($page == 'user') : ?>

		<?php if (ENVIRONMENT == 'production') : ?>
		<link href="<?php echo base_url(); ?>build/users.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/users.css" rel="stylesheet">
		<?php endif; ?>

	<?php elseif ($page == 'packages') : ?>

		<?php if (ENVIRONMENT == 'production') : ?>
		<link href="<?php echo base_url(); ?>build/packages.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/packages.css" rel="stylesheet">
		<?php endif; ?>

	<?php elseif ($page == 'settings') : ?>

		<?php if (ENVIRONMENT == 'production') : ?>
		<link href="<?php echo base_url(); ?>build/settings.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/settings.css" rel="stylesheet">
		<?php endif; ?>

	<?php elseif ($page == 'elements_blocks') : ?>

		<?php if (ENVIRONMENT == 'production') : ?>
		<link href="<?php echo base_url(); ?>build/elements_blocks.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/elements_blocks.css" rel="stylesheet">
		<?php endif; ?>

	<?php elseif ($page == 'elements_components') : ?>

		<?php if (ENVIRONMENT == 'production') : ?>
		<link href="<?php echo base_url(); ?>build/elements_components.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/elements_components.css" rel="stylesheet">
		<?php endif; ?>

	<?php elseif ($page == 'analytics') : ?>

		<?php if (ENVIRONMENT == 'production') : ?>

		<link href="<?php echo base_url(); ?>build/analytics.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>build/sites.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/analytics.css" rel="stylesheet">
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/sites.css" rel="stylesheet">
		<?php endif; ?>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">

	<?php elseif ($page == 'social' || $page == 'socialFindcontent' || $page == 'socialProfile' || $page == 'socialQueue' || $page == 'socialSchedule')  : ?>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link href="<?php echo base_url(); ?>build/sites.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/flat-ui-pro-custom.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">

		<link href="<?php echo base_url(); ?>assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
		<link  href="<?php echo base_url() ?>assets/calender/fullcalendar.min.css" rel="stylesheet"  media="screen" />

		    <link rel="stylesheet" href="<?php echo base_url() ?>assets/social/bootstrap-social.css" />


		<link href="<?php echo base_url(); ?>build/social.css" rel="stylesheet">


	<?php else : ?>

		<?php if (ENVIRONMENT == 'production') : ?>
		<link href="<?php echo base_url(); ?>build/login.css" rel="stylesheet">
		<?php elseif (ENVIRONMENT == 'development') : ?>
		<link href="<?php echo $this->config->item('webpack_dev_url'); ?>build/login.css" rel="stylesheet">
		<?php endif; ?>

	<?php endif; ?>

	<link rel="shortcut icon" href="<?php echo base_url('img/favicon.png'); ?>">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
	<script src="<?php echo base_url('assets/js/html5shiv.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
	<![endif]-->
	<!--[if lt IE 10]>
	<link href="<?php echo base_url('assets/css/ie-masonry.css'); ?>" rel="stylesheet">
	<script src="<?php echo base_url('assets/js/masonry.pkgd.min.js'); ?>"></script>
	<![endif]-->

	<script>
		var baseUrl = '<?php echo base_url('/'); ?>';
		var siteUrl = '<?php echo site_url('/'); ?>';
	</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '97115dbe2010690de58d639740bbefe87954edb9');
</script>



<?php if ($page == 'social' || $page == 'socialFindcontent' || $page == 'socialProfile' || $page == 'socialQueue' || $page == 'socialSchedule')  : ?>
<script src="<?php echo base_url() ?>assets/calender/lib/moment.min.js"></script>
        <script src="<?php echo base_url() ?>assets/calender/fullcalendar.min.js"></script>
               <script src="<?php echo base_url() ?>assets/calender/gcal.js"></script>
             
             <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/bootstrap-notify/bootstrap-notify.js"></script>


<?php endif; ?>

</head>
