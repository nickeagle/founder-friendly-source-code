<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onboarding extends MY_Controller {

    /**
     * Class constructor
     *
     * Loads required models, check if user has right to access this class, load the hook class and add a hook point
     *
     * @return  void
     */
     public function __construct()
     {
       parent::__construct();
       if ( ! $this->session->has_userdata('user_id') && $this->uri->segment(1) != 'loadsinglepage' && $this->uri->segment(1) != 'loadsingleframe')
       {
           redirect('auth', 'refresh');
       }

       $this->load->module('package');

        if($this->package->check_subscription()==FALSE)
        {
            redirect($this->config->item('mail_url').'account/subscription');
        }
       $this->load->model("onboarding/Onboarding_model","model");

     }


     public function index()
     {
        $data['onboarding'] = $this->model->get('');
         $this->load->view('onboarding/onboarding', $data);

     }



}
