<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Getting Started</title>
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link href="<?php echo base_url() ?>/build/sites.css" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon.png">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/css/flat-ui-pro-custom.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- local js files and css -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
      <?php $this->load->view("shared/nav.php"); ?>

      <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align: center;" >Welcome to Founder Friendly</h4>
      </div>
      <div class="modal-body">

        <div style="text-align: center;">

<iframe width="560" height="315" src="https://www.youtube.com/embed/gJDsL9_TkUY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>

      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
    </div>

  </div>
</div>


  <div class="container-fluid">

          <!-- jubotron welcome -->
              <div class="alert alert-info" role="alert">
                <strong>Getting Started...</strong>
                <br>
                </br>
                <p>Here are the first few easy steps to help you make the most of Founder Friendly.  </p>
              </div>
          <div class="container-fluid">
          <div class="col-md-6 col-md-offset-4">


              <h4> Maximise your marketing now... </h4>


          </div>
        </div>



        <!-- Site builder onboarding Table -->
        <table class="table">
                  <thead>
                    <tr>
                      <td scope="col"></td>
                      <th scope="col" style="width:80%;color:#4A90E2;">Site Builder Tasks</th>
                      <th scope="col">Completed</th>
                    </tr>
                  </thead>
                  <tbody>
                <tr>
                  <td scope="row">1</td>
                  <td><strong>Create Your First Website</strong><br>Set up the first page of your website using our drag and drop website builder. It's super simple. <a href="https://founder-friendly.com/create-your-first-website/" target="_blank"> Watch video </a>
                  <br>
                  <a href="<?php echo base_url() ?>sites/create">Start Now > </a></td>
                  <td>
                      <!-- if website exists with user id then display -->
                      <?php
                      echo $onboarding[0];
                  ?>

                  </td>
                </tr>

                <tr>
                  <td scope="row">2</td>
                  <td><strong>Install tracking code</strong><br>Set up a google analytics account and copy the view id to your website.<a href="https://founder-friendly.com/install-tracking-code/" target="_blank"> Watch video </a>
                  <br>
                  <a href="<?php echo base_url() ?>analytics">Start Now > </a>
                </td>
                  <td>
                      <!-- if tracking code exists with user id then display -->
                    <?php
                    echo $onboarding[1];
                    ?>
                    </tr>
                <tr>
                  <td scope="row">3</td>
                  <td><strong>Publish your first website</strong><br>Set up custom domain publishing and publish your first website.<a href="https://founder-friendly.com/publish-your-first-website/" target="_blank"> Watch video </a>
                </td>
                  <td>
                    <!-- if site published on custom domain with user id then display -->
                  <?php
                  echo $onboarding[2];
                  ?>
                </tr>
  </tbody>
</table>


                <!-- Social Manager onboarding Table -->
  <table class="table">
        <thead>
                <tr>
                  <td scope="col"></td>
                  <th scope="col" style="width:80%;color:#4A90E2;">Social Manager Tasks</th>
                  <th scope="col">Completed</th>
                </tr>
        </thead>
          <tbody>
                <tr>
                  <td scope="row">1</td>
                  <td><strong>Connect your social media accounts</strong><br>Add the accounts you would like to schedule messages for and publish to from Founder Friendy.<a href="https://founder-friendly.com/connect-your-social-media-accounts/" target="_blank"> Watch video </a>
                  <br>
                  <a href="<?php echo base_url() ?>social-profiles">Start Now > </a>
                </td>
                  <td>
                    <?php
                    echo $onboarding[3];
                    ?>
                    </td>
                </tr>
                <tr>
                  <td scope="row">2</td>
                  <td><strong>Set up a schedule</strong><br>Create a simple posting schedule to evenly distribute your social media posts at suitable times.<a href="https://founder-friendly.com/set-up-a-schedule/" target="_blank"> Watch video </a>
                  <br>
                  <a href="<?php echo base_url() ?>social-schedule">Start Now > </a>
                </td>
                  <td>
                    <?php
                    echo $onboarding[4];
                    ?>
                    </tr>
                <tr>
                  <td scope="row">3</td>
                  <td><strong>Schedule your first social media post</strong><br>Create a message to post to one of your social media channels. Use the autoschedule feature, schedule or post now!<a href="https://founder-friendly.com/schedule-your-first-social-media-post/" target="_blank"> Watch video </a>
                  <br>
                  <a href="<?php echo base_url() ?>social">Start Now > </a>
                </td>
                  <td>
                    <?php
                    echo $onboarding[5];
                    ?>
                    </tr>
                </tbody>
                </table>



                <!-- Email Campaigns onboarding Table -->
<table class="table">
        <thead>
                <tr>
                  <td scope="col"></td>
                  <th scope="col" style="width:80%;color:#4A90E2;">Email Campaigns Tasks</th>
                  <th scope="col">Completed</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td scope="row">1</td>
                  <td><strong>Create your first list</strong><br>List are where you keep all of your leads, you can have multiple lists and use these for automation and email campaigns.<a href="https://founder-friendly.com/create-your-first-list-2/" target="_blank"> Watch video </a>
                  <br>
                  <a href="<?php echo $this->config->item("mail_url"); ?>lists">Start Now > </a>
                </td>
                  <td>
                    <?php
                    echo $onboarding[6];
                    ?>
                    </tr>
                  </td>
                </tr>
                <tr>
                  <td scope="row">2</td>
                  <td><strong>Create your first campaign</strong><br>Build you first email campaign using our drag and drop builder.<a href="https://founder-friendly.com/create-your-first-list/" target="_blank"> Watch video </a>
                  <br>
                  <a href="<?php echo $this->config->item("mail_url"); ?>campaigns/select-type">Start Now > </a>
                </td>
                  <td>
                    <?php
                    echo $onboarding[7];
                    ?>

                    </tr>
                <tr>
                  <td scope="row">3</td>
                  <td><strong>Send your first campaign</strong><br>After creating your first email, send you campaign to a list using the schedule feature or send now. check back after to find out how the campaign performed.<a target="_blank" href="https://founder-friendly.com/send-your-first-campaign/"> Watch video </a>
                </td>
                  <td>
                    <?php
                    echo $onboarding[8];
                    ?>
                    </tr>
                </tbody>
                </table>

<br></br>

</div>
<script>
// A $( document ).ready() block.
$( document ).ready(function() {
  if (document.cookie.indexOf('visited=false') == -1){
    // load the overlay
    $('#myModal').modal({show:false});

    var year = 1000*60*60*24*365;
    var expires = new Date((new Date()).valueOf() + year);
    document.cookie = "visited=true;expires=" + expires.toUTCString();

  }
});


</script>

</html>
