<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onboarding_model extends CI_Model {

    /**
     * Class constructor
     *
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }


        public function get($sites)
	    {
	        //write your query and get result

          //current user id
            $id = $_SESSION['user_id'];
            $query=$this->db->query("SELECT * FROM `sites` WHERE `users_id` = $id ");
            $result=$query->result_array();

            if($result == TRUE ) {
              $sites = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-check-circle fa-3x"></i> </span>';
     // row not found, do stuff...
            } else {
    // do other stuff...
           $sites = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-circle-thin fa-3x"></i> </span>';
            }


            // check user has created tracking code
            $query=$this->db->query("SELECT * FROM `sites` WHERE `users_id` = $id AND `sites_tracking` IS NOT NULL");
            $result=$query->result_array();

            if($result == TRUE ) {
            // display ticked box
            $tracking = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-check-circle fa-3x"></i> </span>';
            } else {
            // display empty tick box
            $tracking = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-circle-thin fa-3x"></i> </span>';
            }


            // check user has published a site on custom domain
            $query=$this->db->query("SELECT * FROM `sites` WHERE `users_id` = $id AND `custom_domain` IS NOT NULL");
            $result=$query->result_array();

            if($result == TRUE ) {
            // display ticked box
            $publish = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-check-circle fa-3x"></i> </span>';
            } else {
            // display empty tick box
            $publish = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-circle-thin fa-3x"></i> </span>';
            }


            // check user has connected social media account
            $query=$this->db->query("SELECT * FROM `social_profile` WHERE `created_by` = $id");
            $result=$query->result_array();

            if($result == TRUE ) {
            // display ticked box
            $profile = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-check-circle fa-3x"></i> </span>';
            } else {
            // display empty tick box
            $profile = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-circle-thin fa-3x"></i> </span>';
            }

            // check user has social scheduled slot set up
            $query=$this->db->query("SELECT * FROM `social_schedule` WHERE `user_id` = $id");
            $result=$query->result_array();

            if($result == TRUE ) {
            // display ticked box
            $schedule = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-check-circle fa-3x"></i> </span>';
            } else {
            // display empty tick box
            $schedule = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-circle-thin fa-3x"></i> </span>';
            }

            // check the user has created a social post
            $query=$this->db->query("SELECT * FROM `social_post` WHERE `user_id` = $id");
            $result=$query->result_array();

            if($result == TRUE ) {
            // display ticked box
            $post = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-check-circle fa-3x"></i> </span>';
            } else {
            // display empty tick box
            $post = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-circle-thin fa-3x"></i> </span>';
            }

            // mail database connection
            $database2=$this->config->item('database2');
        	 	$database1=$this->config->item('database1');
        	 	$this->db->db_select($database2);


            // check user has social social media post scheduled
            $query=$this->db->query("SELECT * FROM `mail_lists` WHERE `customer_id` = $id");
            $result=$query->result_array();

            if($result == TRUE ) {
            // display ticked box
            $list = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-check-circle fa-3x"></i> </span>';
            } else {
            // display empty tick box
            $list = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-circle-thin fa-3x"></i> </span>';
            }

            // check user has created a campaign
            $query=$this->db->query("SELECT * FROM `campaigns` WHERE `customer_id` = $id");
            $result=$query->result_array();

            if($result == TRUE ) {
            // display ticked box
            $campaign = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-check-circle fa-3x"></i> </span>';
            } else {
            // display empty tick box
            $campaign = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-circle-thin fa-3x"></i> </span>';
            }



              // check user has created a campaign
              $query=$this->db->query("SELECT * FROM `campaigns` WHERE `customer_id` = $id AND `delivery_at` IS NOT NULL");
              $result=$query->result_array();

              if($result == TRUE ) {
              // display ticked box
              $sent = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-check-circle fa-3x"></i> </span>';
              } else {
              // display empty tick box
              $sent = '<span style="text-align:center;"><i style=" color:#4A90E2; vertical-align: middle;" data-placement="bottom" data-toggle="" title="" class="fa fa-circle-thin fa-3x"></i> </span>';
}

            $sites = array($sites, $tracking , $publish, $profile, $schedule, $post, $list, $campaign, $sent );
            $this->db->db_select($database1);
            return $sites;

	    }


}
