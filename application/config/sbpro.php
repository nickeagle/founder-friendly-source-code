<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// From address is used for all emails send by the script
$config['email_from_address'] = "info@founder-friendly.com";

// From name is used for all emails send by the script
$config['email_from_name'] = "Founder Friendly";

// Subject for the email send to user when payment received
$config['email_confirmation_subject'] = "Founder Friendly :  Confirmation email!";

// Subject for the email send to user when admin create an user from admin panel with paid plan
$config['email_activation_subject'] = "Founder Friendly :  Activation email!";

// Subject for the email send to user when admin create an user from admin panel with free plan
$config['email_login_subject'] = "Founder Friendly :  Account created!";

// Subject for the email send to user when password forgot
$config['email_forgot_password_subject'] = "Founder Friendly : Forgot Password!";

// Subject for the email send to user when admin send reset password email
$config['email_reset_password_subject'] = "Founder Friendly :  Reset Password!";

// SentAPI email from address is used for all emails send by the script
$config['sent_email_from_address'] = "info@founder-friendly.com";

// SentAPI email from name is used for all emails send by the script
$config['sent_email_from_name'] = "Founder Friendly";

// SentAPI email subject for the email send to user by the script
$config['sent_email_default_subject'] = "Founder Friendly :  Mail from your site!";

$config['email_sub_cancel_subject'] = "Founder Friendly :  Profile Cancelled!";

$config['sub_cancel_failed_subject'] = "Founder Friendly :  Profile Cancellation Failed!";

// CoreUpdate URI
$config['autoupdate_uri'] = 'https://update.pagestead.com/updates.json';
//$config['autoupdate_uri'] = 'https://sbprolicense.lhost/updates.json';

// License Server API Endpoint
$config['license_api'] = 'https://license.pagestead.com/api/';
//$config['license_api'] = 'https://sbprolicense.lhost/api/';

// Depricated
// License URI
$config['license_uri'] = 'https://license.pagestead.com/api/verify_key/';
//$config['license_uri'] = 'https://sbprolicense.lhost/api/verify_key/';
// Depricated
// JSON URI
$config['json_uri'] = 'https://license.pagestead.com/api/get_json/';
//$config['json_uri'] = 'https://sbprolicense.lhost/api/get_json/';

// Screenshot API Key
$config['screenshot_api_key'] = "2e94ee";
$config['screenshot_secret'] = "lksejhfefghug75765";

// Screenshot folder for site thumbs
$config['screenshot_sitethumbs_folder'] = "tmp/sitethumbs/";
// Screenshot folder for site thumbs
$config['screenshot_blockhumbs_folder'] = "tmp/blockthumbs/";

// Upload path for block thumbnails
$config['block_thumbnail_upload_config']['upload_path'] = "./images/uploads"; // Used to store the uploaded file
$config['block_thumbnail_upload_config']['allowed_types'] = 'gif|jpg|png';
$config['block_thumbnail_upload_config']['max_size'] = 5000;
$config['block_thumbnail_upload_config']['max_width'] = 2000;
$config['block_thumbnail_upload_config']['max_height'] = 1000;

// Upload path for component thumbnails
$config['component_thumbnail_upload_config']['upload_path'] = "./images/uploads"; // Used to store the uploaded file
$config['component_thumbnail_upload_config']['allowed_types'] = 'gif|jpg|png';
$config['component_thumbnail_upload_config']['max_size'] = 5000;
$config['component_thumbnail_upload_config']['max_width'] = 2000;
$config['component_thumbnail_upload_config']['max_height'] = 1000;