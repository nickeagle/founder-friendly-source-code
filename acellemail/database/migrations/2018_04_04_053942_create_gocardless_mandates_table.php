<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGocardlessMandatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gocardless_mandates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mandate',250);
            $table->integer('customer_id',11);
             $table->longText('webhook_response');
             $table->integer('status',4)->->default(2)->comment('-2->expired,-1->failed,0-cancelled,1-active,2-created,3-submitted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gocardless_mandates');
    }
}
