# Doctrine Common

[![Build Status](https://secure.travis-ci.org/doctrine/common.png)](https://travis-ci.org/doctrine/common)

The Doctrine Common project is a library that provides extensions to core PHP functionality.

## More resources:

* [Website](https://www.doctrine-project.org)
* [Documentation](https://docs.doctrine-project.org/projects/doctrine-common/en/latest/)
* [Issue Tracker](https://www.doctrine-project.org/jira/browse/DCOM)
* [Downloads](https://github.com/doctrine/common/downloads)
