@if (is_object($subscription->plan))
    <div class="row">
        <div class="col-md-12 plan-left">
            

            <ul class="dotted-list topborder section mb-0">
                <li class="border-bottom-0">
                    <div class="unit size1of2 text-bold">
                        <strong>{{ trans('messages.monthly').' '.trans('messages.price') }}</strong>
                    </div>
                    <div class="lastUnit size1of2">
                        <p class="mt-0 mb-0 text-semibold">
                            <mc:flag>
                                @if ($subscription->price)
                                    {{ Acelle\Library\Tool::format_price($subscription->price, $subscription->currency_format) }}
                                @else
                                    {{ trans('messages.free') }}
                                @endif
                            </mc:flag>
                        </p>
                    </div>
                </li>
                <li class="border-bottom-0">
                    <div class="unit size1of2 text-bold">
                        <strong>{{ trans('messages.payment_date') }}</strong>
                    </div>
                    <div class="lastUnit size1of2">
                        <p class="mt-0 mb-0 text-semibold">
                           
                               1st of each month
                        
                        </p>
                    </div>
                </li>
                <li class="border-bottom-0">
                    <div class="unit size1of2 text-bold">
                        <strong>{{ trans('messages.today_payment') }}</strong>
                    </div>
                    <div class="lastUnit size1of2">
                        <p class="mt-0 mb-0 text-semibold">
                            <mc:flag>
                                @if ($subscription->price)
                                <?php
                            $remaining_date=(int)date('t', time()) - (int)date('j', time());
                             $day=(int)date('t', time());                             
                            $remaining_amount=round(((int)$subscription->price/$day)*$remaining_date);
                            ?>
                            {{ Acelle\Library\Tool::format_price($remaining_amount, $subscription->currency_format) }}
                            @endif
                            </mc:flag>
                        </p>
                    </div>
                </li>
            </ul>

           @include('subscriptions._details', ['subscription' => $subscription])

            @if ($subscription->isFree())
                <div class="text-left">
                    <button type='submit' class="btn bg-teal">
                    {{ trans('messages.get_started') }}
                    <i class="icon-arrow-right7"></i></button>
                </div>
            @else
                <input type="hidden" name="payment_method_uid" value='' />
                <div class="text-left">
                    @foreach (\Acelle\Model\PaymentMethod::getAllActive() as $payment_method)
                        @include('payments._button_' . $payment_method->type)
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endif
