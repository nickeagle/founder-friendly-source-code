<!DOCTYPE html>
<html>
<head>
	<title>Thank You</title>
</head>
<body>
	<center>

		<p>Your details have been submitted, thank you.</p>

		<p><i>You will be automatically redirected to our website. <a href="<?php echo $location; ?>">Click here</a> for our website.</i></p>
	</center>


	<script type="text/javascript">setTimeout(function(){window.location="<?php echo $location; ?>"; }, 4000);</script>

</body>
</html>
