
<html>
<head>
	<style>
		table, th, td {
   border: 1px solid #9b9b9b;
}
table {
    border-collapse: collapse;
    width: 80%;
}
th,td {
    text-align: left;
    padding: 18px;
}
	</style>
</head>
<body>
	<img src="https://mail.founder-friendly.com/images/site_logo_small.png" style="width: 150px;">
	<hr>

	<h3>Congratulations! You just acquired a new lead.</h3>

	<table>
		<tr><th colspan="2" style="background-color: #9b9b9b;color: #ffffff;">Submitted Form Data</th></tr>

		<?php
		foreach ($mail_message as $key => $value) {
			if($key!='listuid' && $key!='api_token' && substr($key,0,1)!='_'){
		?>
			<tr><th><?php echo ucfirst(strtolower($key)); ?></th><td><?php echo $value; ?></td></tr>
		<?php	
			}
		}
		?>
		
	</table>
</body>
</html>