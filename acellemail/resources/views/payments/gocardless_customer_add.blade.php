<html lang="en-gb">
  <head>
    <meta charset="utf-8">
    <title>Founder Friendly</title>
    <meta name="viewport" content="width=device-width">

    <style type="text/css">
      .success-wrapper {
    background-color: white;
    border-radius: 4px;
    box-shadow: 0 0 1px rgba(0,0,0,0.05), 0 0 3px rgba(0,0,0,0.05), 0 2px 8px rgba(0,0,0,0.05);
}
.success-wrapper {
    display: block;
    margin: 30px auto 0;
    max-width: 300px;
    text-align: center;
    padding: 24px 18px;
    -webkit-animation: success-appear 0.4s;
    animation: success-appear 0.4s;
    box-sizing: border-box;
}
.success-check {
    width: 40px;
    height: 40px;
    margin-bottom: 12px;
}
img {
    max-width: 100%;
    height: auto;
    vertical-align: middle;
    border: 0;
    -ms-interpolation-mode: bicubic;
}
.success-wrapper__heading {
    font-size: 14px;
    font-weight: bold;
    margin-bottom: 6px;
    line-height: 1.5em;
}
.success-wrapper__message {
    font-size: 13px;
    color: #86878A;
    margin-bottom: 0;
}
    </style>
  </head>
  <body>
    <div class="success-wrapper">
      <img class="success-check" src="{{ url('images/success-icon.png') }}" alt="">
      <h1 class="success-wrapper__heading">Direct Debit set up successfully</h1>
      <p class="success-wrapper__message">
        GoCardless Ltd will appear on your bank statement when payments are taken against this Direct Debit.
      </p>
    </div>


      <noscript>
        <div class="success-footer">
          <div class="u-text-meta u-text-h5 u-margin-Bxs">
            <p class="u-margin-Bxs">
              <a
              id="mandate-preview-link"
              href="https://pay-sandbox.gocardless.com/pay/flow/RE0000Y8P6QYMN0Y7JPA5Z5Z0MTVT61P/view_mandate_pdf"
              target="_blank"
              rel="noopener noreferrer"
              class="success-footer__view-mandate"
              >
                Your Direct Debit instruction
              </a>
            </p>
          </div>
        </div>
    </noscript>
  </body>
</html>