<?php

namespace Acelle\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{

    protected $mail_server_url='http://mail.founder-friendly.com/';
    protected $site_server_url='http://sites.founder-friendly.com/';

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
               // return redirect()->guest('login');
                return redirect($this->site_server_url.'auth/logout');

            }
        }

        return $next($request);
    }
}
