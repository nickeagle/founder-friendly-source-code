<?php

/**
 * CampaignLink class.
 *
 * Model class for campaign email's links
 *
 * LICENSE: This product includes software developed at
 * the Acelle Co., Ltd. (https://acellemail.com/).
 *
 * @category   MVC Model
 *
 * @author     N. Pham <n.pham@acellemail.com>
 * @author     L. Pham <l.pham@acellemail.com>
 * @copyright  Acelle Co., Ltd
 * @license    Acelle Co., Ltd
 *
 * @version    1.0
 *
 * @link       https://acellemail.com
 */

namespace Acelle\Model;

use Illuminate\Database\Eloquent\Model;

class CampaignLink extends Model
{
}
