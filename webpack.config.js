const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require("copy-webpack-plugin");
const WriteFilePlugin = require("write-file-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const package = "starter";

if ( package === "starter" ) {
    var elements = "assets/elements_starter";
    var skeletonPath = "assets/elements_starter/skeleton.html";
} else if ( package === "professional" ) {
    var elements = "assets/elements_professional";
    var skeletonPath = "assets/elements_professional/skeleton.html";
} else if ( package === "enterprise" ) {
    var elements = "assets/elements_enterprise";
    var skeletonPath = "assets/elements_enterprise/skeleton.html";
}

module.exports = {
    entry: {
        builder: './assets/js/builder.js',
        sites: './assets/js/sites.js',
        images: './assets/js/images.js',
        settings: './assets/js/settings.js',
        users: './assets/js/users.js',
        login: './assets/js/login.js',
        register: './assets/js/register.js',
        packages: './assets/js/packages.js',
        sent: './assets/js/sent.js',
        autoupdate: './assets/js/autoupdate.js',
        inblock: './assets/js/inblock.js',
        elements_blocks: './assets/js/elements_blocks.js',
        elements_components: './assets/js/elements_components.js'
    },
    output: {
        path: __dirname + '/build/',
        publicPath: '/build/',
        filename: '[name].bundle.js'
    },
    devServer: {
        outputPath: __dirname + '/build',
        inline: true,
        headers: {
            "Access-Control-Allow-Origin": "*",
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader?sourceMap"
                })
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader?sourceMap",
                    "sass-loader?sourceMap"
                ]
            },
            {
                test: /\.js$/,
                exclude: [/node_modules/, /assets\/js\/vendor/],
                use: [
                    {
                        loader: "jshint-loader",
                        options: {
                            esversion: 6,
                            bitwise: true,
                            eqeqeq: true,
                            forin: true,
                            freeze: true,
                            futurehostile: true,
                            newcap: true,
                            latedef: 'nofunc',
                            noarg: true,
                            nocomma: true,
                            nonbsp: true,
                            nonew: true,
                            strict: true,
                            undef: true,
                            node: true,
                            browser: true,
                            loopfunc: true,
                            laxcomma: true,
                            '-W121': false,
                            '-W089': false,
                            '-W055': false,
                            '-W069': false,
                            '-W080': false,
                            '-W038': false,
                            globals: {
                                define: false,
                                alert: false,
                                confirm: false,
                                ace: false,
                                $: false,
                                jQuery: false,
                                Slim: false,
                                slimImageUpdate: true,
                                slimHandleServerError: true,
                                slimImageTransform: true
                            }
                        }
                    },
                    {
                        loader: "babel-loader",
                        options: {
                            plugins: [
                                require('babel-plugin-transform-es2015-template-literals'),
                                require('babel-plugin-transform-es2015-classes'),
                                require('babel-plugin-transform-es2015-parameters'),
                                require('babel-plugin-check-es2015-constants'),
                                require('babel-plugin-transform-es2015-block-scoping'),
                                require('babel-plugin-transform-es2015-for-of'),
                                require('babel-plugin-transform-es2015-arrow-functions'),
                                require('babel-plugin-add-module-exports')
                            ]
                        }
                    }
                ]
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: "url-loader",
                    options: {
                        limit: 10000,
                        mimetype: 'image/svg+xml'
                    }
                }
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            limit: 1000
                        }
                    },
 		    {
                        loader: "url-loader",
                        options: {
                            limit: 1000
                        }
                    },
                    {
                        loader: "img-loader",
                        options: {
                            progressive: true
                        }
                    }

                ]
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.(woff(\?v=\d+\.\d+\.\d+)|woff2(\?v=\d+\.\d+\.\d+)|woff|woff2)$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 5000,
                            mimetype: 'aplication/font-woff'
                        }
                    }
                ]
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 10000,
                            mimetype: 'pplication/octet-stream'
                        }
                    }
                ]
            },
        ]
    },
    devtool: "source-map",
    plugins: [
        new WriteFilePlugin(),
        new ExtractTextPlugin("[name].css"),
        new UglifyJSPlugin({
            sourceMap: true
        }),
        new CleanWebpackPlugin(['build'], {
            root: __dirname,
            verbose: true,
            dry: false
        }),
        new CopyWebpackPlugin([
            {from: 'assets/elements_starter/skeleton.html', to: __dirname + '/elements/skeleton.html'},
            {from: 'assets/elements_starter', to: __dirname + '/elements/elements_starter'},
            {from: 'assets/css/blocks.css', to: __dirname + '/elements/css/blocks.css'},
            {from: 'assets/images/thumbs', to: __dirname + '/elements/thumbs'},
            {from: 'assets/images/component_thumbs', to: __dirname + '/elements/thumbs/components'}
        ])
    ]
};
