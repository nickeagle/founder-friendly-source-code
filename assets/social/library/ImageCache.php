<?php

/**
 * PHP Social Stream 2.5.4
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

class ImageCache {
    public $automationHandler = 'auto';
    public $imageCacheDir = 'cache/'; // set to a writable folder for image cache storage
    public $imageSquare = FALSE; // change a rectangular image into a square with white background
    public $imageCacheTime = 604800; // cache period in seconds, set to zero for proxy only mode
    public $imageCacheResize = FALSE; // set to resize dimension to enable  e.g. $this->imageCacheResize = 250;

    function cacheFetch_auto($src)
    {
        if (function_exists("curl_init") )
        {
            return $this->cacheFetch_curl($src);
        }
        else
        {
            return $this->cacheFetch_php($src);
        }
    }

    function cacheFetch_curl($src)
    {
        $ch = curl_init($src);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $img = curl_exec($ch);
        if ($img === false)
        {
            echo curl_error($ch);
        }
    	curl_close($ch);
        return $img;
    }
    
    function cacheFetch_php($src)
    {
        return file_get_contents($src);
    }

    function cacheFetch($src)
    {
        // make file name
        $filename = $this->imageCacheDir.md5($src).'.cache';
        
        // check if file is cached
        $fetch = (
        (!$this->imageCacheTime) ||
        (!file_exists($filename) ) ||
        (filemtime($filename) < (time() - $this->imageCacheTime) )
        );
        if ($fetch)
        {
            $cacheFetch_fn = "cacheFetch_".$this->automationHandler;
            $img = $this->{$cacheFetch_fn}($src);
        }
        else
        {
            $img = file_get_contents($filename);
        }
        if (!$res = @imagecreatefromstring($img) ) return FALSE;
        if ($this->imageCacheResize)
        {
            $oldX = imagesx($res);
            $oldY = imagesy($res);
            if ($this->imageSquare || $oldX > $this->imageCacheResize)
            {
                if ($this->imageSquare)
                {
                    $new = imagecreatetruecolor($this->imageCacheResize, $this->imageCacheResize);
                    $newBackground = imagecolorallocate($new, 255, 255, 255);
                    imagefill($new, 1, 1, $newBackground);
                    if ($oldX > $oldY)
                    {
                        $newX = $this->imageCacheResize;
                        $xMultiplier = ($newX / $oldX);
                        $newY = intval($oldY * $xMultiplier);
                        $dstX = 0;
                        $dstY = ($this->imageCacheResize / 2) - ($newY / 2);
                    }
                    else
                    {
                        $newY = $this->imageCacheResize;
                        $yMultiplier = ($newY / $oldY);
                        $newX = intval($oldX * $yMultiplier);
                        $dstX = ($this->imageCacheResize / 2)-($newX / 2);
                        $dstY = 0;
                    }
                    imagecopyresized($new, $res, $dstX, $dstY, 0, 0, $newX, $newY, $oldX, $oldY);
                }
                elseif ($oldX > $this->imageCacheResize)
                {
                	// calculate new width
                	$ratio = ($this->imageCacheResize / $oldX);
                	$new_w = $this->imageCacheResize;
                	$new_h = intval($oldY * $ratio);
                	$new = imagecreatetruecolor($new_w, $new_h);
                	imagecopyresampled($new, $res, 0, 0, 0, 0, $new_w, $new_h, $oldX, $oldY);
                }
                imagedestroy($res);
                ob_start();
                imagejpeg($new, NULL, 75);
                imagedestroy($new);
                $img = ob_get_contents();
                ob_end_clean();
            }
        }
        
        if ($fetch && $this->imageCacheTime)
        {
            $fp = fopen($filename, "w");
            fwrite($fp, $img);
            fclose($fp);
        }
        
        return $img;
    }

    // display image
    function cacheImage($imgArr)
    {
        if (@$imgArr["refresh"])
            $this->imageCacheTime = $imgArr["refresh"];
        
        if (@$imgArr["resize"])
            $this->imageCacheResize = $imgArr["resize"];
        
        $src = @$imgArr['src'];
        $token = @$imgArr["token"];
        if ($src && $token)
        {
            $tokenServer = md5(urlencode($src).@$_SERVER['SERVER_ADDR'].@$_SERVER['SERVER_ADMIN'].@$_SERVER['SERVER_NAME'].@$_SERVER['SERVER_PORT'].@$_SERVER['SERVER_PROTOCOL'].@$_SERVER['SERVER_SIGNATURE'].@$_SERVER['SERVER_SOFTWARE'].@$_SERVER['DOCUMENT_ROOT']);
            if ($tokenServer == $token)
            {
                if (!$img = $this->cacheFetch($src) )
                {
                    header("HTTP/1.0 404 Not Found");
                    exit();
                }
                else
                {
                    header("Content-Type: image");
                    print $img;
                    exit();
                }
            } else {
                header("HTTP/1.0 404 Not Found");
                exit();
            }
        }
    }
}
