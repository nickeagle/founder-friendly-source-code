/*
    CSS
*/
require('../css/load-main.css');

/*
    scripts (as conventional globals)
*/
require("script-loader!./vendor/jquery.min.js");
require("script-loader!./vendor/jquery-ui.min.js");
require("script-loader!./vendor/flat-ui-pro.min.js");
require("script-loader!./vendor/jquery.zoomer.js");

(function () {
    "use strict";

    require('./modules/shared/ui');
    require('./modules/packages/packages');
    require('./modules/shared/account');

}());